from ads.campaign_management.advertising import Advertising

import datetime


# advertising = Advertising(['google'])
advertising = Advertising(['facebook'])

advertiser = advertising.set_advertiser('autodesk')
print '1. create advertiser'

product = advertising.set_product(advertiser.id, 'fusion360')
print '2. create product'

advertising.set_account('facebook', '727144207398148', 'Test', product.id)
advertising.set_account('google', '4430529228', 'Google Test', product.id)
print '3. create account'

ucampaign = advertising.set_united_campaign(product.id, 'Autodesk campaign3',
                                            10000, datetime.datetime.utcnow())
print '4. create united campaign'

advertising.set_creative_config(ucampaign.id, 'ads/conf/ads.ini')
print '5. create creative'

lead_ids = advertising.import_leads(ucampaign.id, 'test.csv')
# lead_ids = advertising.import_leads(ucampaign.id, 'test/10a.csv')
print '6. import leads'

# set budget bid
budget_bid_info = dict()
budget_bid_info['budget_type'] = 'STANDARD'
budget_bid_info['budget_amount'] = 1200
budget_bid_info['bid_type'] = 'CPC'
budget_bid_info['bid_amount'] = 600
budget_bid_info['is_share'] = False
budget_bid_info['budget_period'] = 'DAILY'

advertising.set_budget_bid('facebook', lead_ids, budget_bid_info)
advertising.set_budget_bid('google', lead_ids, budget_bid_info)
print '7. set budget and bidding'

advertising.create_campaigns(advertiser.id, ucampaign.id, budget_bid_info)
print '8. create campaigns'

'''
budget_info = {'budget_amount': 1300}
advertising.update_budget(ucampaign.id, budget_info)
print '9. update budget'

bid_info = {'bid_type':'CPC','bid_amount':300}
advertising.update_bidding(ucampaign.id, bid_info)
print '10. update bidding'

advertising.update_campaign_status(ucampaign.id, 0)
print '11. update campaign status'

fb_client = advertising_s.get_facebook_ads_client()
insight_s = InsightService(fb_client)
query_field = {}
query_field['time']='last_7_days'
insight_s.get_insight_cron("act_727144207398148", query_field)
'''
