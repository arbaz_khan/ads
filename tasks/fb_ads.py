from facebookads.objects import (
    AdGroup,
    AdSet
)
from facebookads import FacebookAdsApi

# These settings are from ads/conf/fb_online_api.conf
session = (
    700406300056715,
    'e6aff1d879e57c7180d28fac25e193ea',
    "CAAJ9BAy2nIsBAAIDrk9ZC8ZCgzXLgppS0IStzREUqA609DY1U2kdmSN3SkDB2NUTLmezY0vJU2YinyDgP2SDasAZBLHF3XZAalpZAZCS8A4Txkn0LJBEjw2ZAZBUt7lyMnlox6P3byXcI3hK7bpLDgkC92w4Vmo0lpPkrX1UvjaacXtnbKkRCSDG"  # nopep8
)
FacebookAdsApi.init(*session)


def get_ad(ad_id):
    adgroup = AdGroup(ad_id)
    ads = adgroup.remote_read(fields=[AdGroup.Field.name,
                                      AdGroup.Field.creative,
                                      AdGroup.Field.account_id,
                                      AdGroup.Field.campaign_group_id,
                                      AdGroup.Field.campaign_id,
                                      AdGroup.Field.status])
    return ads


def create_campaign(name, campaign_group_id, bid_type, bid_info,
                    account_id, daily_budget=2000):
    data = {
        AdSet.Field.name: name,
        AdSet.Field.bid_type: bid_type,
        # AdSet.Field.bid_info: bid_info,
        AdSet.Field.is_autobid: True,
        AdSet.Field.status: AdSet.Status.active,
        AdSet.Field.daily_budget: daily_budget,
        AdSet.Field.campaign_group_id: campaign_group_id,
        AdSet.Field.targeting: {
            'geo_locations': {
                'countries': ['US'],
            },
        },
    }

    adset = AdSet(parent_id=account_id)
    return adset.remote_create(params=data)


def create_ad(name, campaign_id, creative_id, account_id):
    adgroup = AdGroup(parent_id=account_id)
    adgroup[AdGroup.Field.name] = name
    adgroup[AdGroup.Field.campaign_id] = campaign_id
    adgroup[AdGroup.Field.status] = AdGroup.Status.paused
    adgroup[AdGroup.Field.creative] = {
        'creative_id': creative_id,
    }
    return adgroup.remote_create()

# this id is copied from manually created one, it's to reuse
# the existing creative.
ad_id = 6034165850482
ad = get_ad(ad_id)
print ad

account_id = "act_%s" % ad["account_id"]
creative_id = ad["creative"]["id"]
campaign_group_id = ad["campaign_group_id"]


campaign = create_campaign("script autobid cpa", campaign_group_id,
                           # Here is the edit point to set different bid strategy
                           "CPA",
                           None, account_id)
print campaign
new_ads = create_ad('test_ads', campaign['id'], creative_id, account_id)
print new_ads
