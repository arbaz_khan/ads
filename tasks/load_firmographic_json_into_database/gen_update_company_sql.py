import csv

if __name__ == '__main__':
    with open('autodesk_ad_campaign_comp_profile_enrichment.csv', 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            tpl = "UPDATE %s\n"
            tpl += "SET address_city = \"%s\", address_state = \"%s\", address_country = \"%s\", revenue = \"%s\"\n"
            tpl += "WHERE domain = \"%s\";"
            compiled = tpl % ("company", row["addressCity"], row["addressState"], row["addressCountry"], row["revenue"], row["domain"])

            print compiled
