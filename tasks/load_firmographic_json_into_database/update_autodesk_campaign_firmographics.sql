UPDATE company
SET address_city = "Wichita Falls", address_state = "Texas", address_country = "United States", revenue = "$22,095,000.00 ", employee_size = "175 "
WHERE domain = "unitedregional.org";
UPDATE company
SET address_city = "Little Rock", address_state = "Arkansas", address_country = "United States", revenue = "$4,915,000.00 ", employee_size = "187 "
WHERE domain = "uams.edu";
UPDATE company
SET address_city = "St. Louis", address_state = "Missouri", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "buschgardens.com";
UPDATE company
SET address_city = "Andover", address_state = "Kansas", address_country = "United States", revenue = "$7,235,000.00 ", employee_size = "95 "
WHERE domain = "ksmedcenter.com";
UPDATE company
SET address_city = "Frisco", address_state = "Colorado", address_country = "United States", revenue = "$10,018,000.00 ", employee_size = "50 "
WHERE domain = "cu-rockies.org";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "hallstar.com";
UPDATE company
SET address_city = "San Francisco", address_state = "California", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "westcoastindustries.com";
UPDATE company
SET address_city = "Lexington", address_state = "Massachusetts", address_country = "United States", revenue = "$54,000,000.00 ", employee_size = "270 "
WHERE domain = "gomez.com";
UPDATE company
SET address_city = "Santa Clara", address_state = "California", address_country = "United States", revenue = "$375,000,000.00 ", employee_size = "981 "
WHERE domain = "foundrynet.com";
UPDATE company
SET address_city = "Redwood City", address_state = "California", address_country = "United States", revenue = "$1,010,000,000.00 ", employee_size = "3,234 "
WHERE domain = "informatica.com";
UPDATE company
SET address_city = "Jacksonville", address_state = "Florida", address_country = "United States", revenue = "$27,198,000.00 ", employee_size = "483 "
WHERE domain = "ufhealthjax.org";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$37,252,000.00 ", employee_size = "175 "
WHERE domain = "feedingamerica.org";
UPDATE company
SET address_city = "Latham", address_state = "New York", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "bsneny.com";
UPDATE company
SET address_city = "Akron", address_state = "Ohio", address_country = "United States", revenue = "$22,877,000.00 ", employee_size = "10,000 "
WHERE domain = "summahealth.org";
UPDATE company
SET address_city = "Sioux Falls", address_state = "South Dakota", address_country = "United States", revenue = "$8,394,000.00 ", employee_size = "52 "
WHERE domain = "sduniversitycenter.org";
UPDATE company
SET address_city = "Morgan Hill", address_state = "California", address_country = "United States", revenue = "$25,400,000.00 ", employee_size = "127 "
WHERE domain = "cidco.com";
UPDATE company
SET address_city = "Duluth", address_state = "Georgia", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "worldfinancialgroup.com";
UPDATE company
SET address_city = "Baltimore", address_state = "Maryland", address_country = "United States", revenue = "$60,000,000.00 ", employee_size = "300 "
WHERE domain = "metastorm.com";
UPDATE company
SET address_city = "Fairfax", address_state = "Virginia", address_country = "United States", revenue = "$175,000,000.00 ", employee_size = "750 "
WHERE domain = "datatel.com";
UPDATE company
SET address_city = "4Th Floor Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "sbra.com";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$11,433,000.00 ", employee_size = "61 "
WHERE domain = "highlandsolutions.com";
UPDATE company
SET address_city = "San Jose", address_state = "California", address_country = "United States", revenue = "$152,000,000.00 ", employee_size = "759 "
WHERE domain = "netcom.com";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$29,109,000.00 ", employee_size = "74 "
WHERE domain = "iic.org";
UPDATE company
SET address_city = "Abbott Park", address_state = "Illinois", address_country = "United States", revenue = "$21,800,000,000.00 ", employee_size = "69,000 "
WHERE domain = "abbott.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "edynamic.net";
UPDATE company
SET address_city = "Kansas City", address_state = "Missouri", address_country = "United States", revenue = "$2,680,000,000.00 ", employee_size = "12,930 "
WHERE domain = "dstsystems.com";
UPDATE company
SET address_city = "Pontiac", address_state = "Michigan", address_country = "United States", revenue = "$4,755,000.00 ", employee_size = "196 "
WHERE domain = "stjoesoakland.org";
UPDATE company
SET address_city = "Vancouver", address_state = "Washington", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "175 "
WHERE domain = "mackaysposito.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "75 "
WHERE domain = "ere.net";
UPDATE company
SET address_city = "Nashua", address_state = "New Hampshire", address_country = "United States", revenue = "$50,000,000.00 ", employee_size = "250 "
WHERE domain = "ecopy.com";
UPDATE company
SET address_city = "Erie", address_state = "Pennsylvania", address_country = "United States", revenue = "$26,373,000.00 ", employee_size = "244 "
WHERE domain = "upmchamot.org";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$2,180,000,000.00 ", employee_size = "66,000 "
WHERE domain = "genpact.com";
UPDATE company
SET address_city = "Los Angeles", address_state = "California", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "375 "
WHERE domain = "dodgers.com";
UPDATE company
SET address_city = "Columbia", address_state = "Maryland", address_country = "United States", revenue = "$37,742,000.00 ", employee_size = "375 "
WHERE domain = "enterprisecommunity.org";
UPDATE company
SET address_city = "Cambridge", address_state = "Massachusetts", address_country = "United States", revenue = "$12,770,000.00 ", employee_size = "133 "
WHERE domain = "acornpd.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "wimba.com";
UPDATE company
SET address_city = "Urbana", address_state = "Illinois", address_country = "United States", revenue = "$37,926,000.00 ", employee_size = "375 "
WHERE domain = "healthalliance.org";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$19,796,000.00 ", employee_size = "175 "
WHERE domain = "cescenter.com";
UPDATE company
SET address_city = "Ann Arbor", address_state = "Michigan", address_country = "United States", revenue = "$16,618,000.00 ", employee_size = "175 "
WHERE domain = "giftoflifemichigan.org";
UPDATE company
SET address_city = "Dallas", address_state = "Texas", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "tradesmasters.net";
UPDATE company
SET address_city = "Milwaukee", address_state = "Wisconsin", address_country = "United States", revenue = "$6,260,000,000.00 ", employee_size = "6,400 "
WHERE domain = "harley-davidson.com";
UPDATE company
SET address_city = "Ashburn", address_state = "Virginia", address_country = "United States", revenue = "$5,800,000,000.00 ", employee_size = "13,582 "
WHERE domain = "dimensiondata.com";
UPDATE company
SET address_city = "Santa Clara", address_state = "California", address_country = "United States", revenue = "$3,000,000,000.00 ", employee_size = "3,000 "
WHERE domain = "hds.com";
UPDATE company
SET address_city = "Sewickley", address_state = "Pennsylvania", address_country = "United States", revenue = "$13,497,000.00 ", employee_size = "63 "
WHERE domain = "ccgf.org";
UPDATE company
SET address_city = "Tulsa", address_state = "Oklahoma", address_country = "United States", revenue = "$15,594,000.00 ", employee_size = "175 "
WHERE domain = "catholiccharitiestulsa.org";
UPDATE company
SET address_city = "Dallas", address_state = "Texas", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "peopleanswers.com";
UPDATE company
SET address_city = "Long Beach", address_state = "Mississippi", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "175 "
WHERE domain = "triton.com";
UPDATE company
SET address_city = "Romulus", address_state = "Michigan", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "hiltongardeninn.com";
UPDATE company
SET address_city = "Hartford", address_state = "Connecticut", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "175 "
WHERE domain = "jcj.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "5northnyc.com";
UPDATE company
SET address_city = "San Jose", address_state = "California", address_country = "United States", revenue = "$520,000,000.00 ", employee_size = "1,563 "
WHERE domain = "extremenetworks.com";
UPDATE company
SET address_city = "Atlanta", address_state = "Georgia", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "3,000 "
WHERE domain = "idhasoft.com";
UPDATE company
SET address_city = "Vashon", address_state = "Washington", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "cheese.com";
UPDATE company
SET address_city = "Louisville", address_state = "Kentucky", address_country = "United States", revenue = "$36,696,000.00 ", employee_size = "375 "
WHERE domain = "pcusa.org";
UPDATE company
SET address_city = "Miami", address_state = "Florida", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "175 "
WHERE domain = "karlville.com";
UPDATE company
SET address_city = "Johnstown", address_state = "Pennsylvania", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "gautiersteel.com";
UPDATE company
SET address_city = "Pelham", address_state = "Alabama", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "juniordavis.com";
UPDATE company
SET address_city = "Hanover", address_state = "Maryland", address_country = "United States", revenue = "$600,000,000.00 ", employee_size = "3,000 "
WHERE domain = "teksystems.com";
UPDATE company
SET address_city = "Washington Dc", address_state = "District of Columbia", address_country = "United States", revenue = "$21,998,000.00 ", employee_size = "175 "
WHERE domain = "aspeninstitute.org";
UPDATE company
SET address_city = "Kansas City", address_state = "Missouri", address_country = "United States", revenue = "$175,000,000.00 ", employee_size = "3,000 "
WHERE domain = "output.net";
UPDATE company
SET address_city = "Chanhassen", address_state = "Minnesota", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "rosemount.com";
UPDATE company
SET address_city = "Waltham", address_state = "Massachusetts", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "184 "
WHERE domain = "on.com";
UPDATE company
SET address_city = "Milwaukee", address_state = "Wisconsin", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "westernplows.com";
UPDATE company
SET address_city = "Weston", address_state = "Florida", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "375 "
WHERE domain = "dhlglobalmail.com";
UPDATE company
SET address_city = "Freeland", address_state = "Washington", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "175 "
WHERE domain = "coldwellbanker.com";
UPDATE company
SET address_city = "Cincinnati", address_state = "Ohio", address_country = "United States", revenue = "$139,000,000.00 ", employee_size = "469 "
WHERE domain = "greatamericaninsurance.com";
UPDATE company
SET address_city = "Hopkins", address_state = "Minnesota", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "750 "
WHERE domain = "thermotech.com";
UPDATE company
SET address_city = "Munster", address_state = "Indiana", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "mybankcitizens.com";
UPDATE company
SET address_city = "Fayetteville", address_state = "North Carolina", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "175 "
WHERE domain = "myrtlefurmage.com";
UPDATE company
SET address_city = "Greenville", address_state = "South Carolina", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "reynoldsglue.com";
UPDATE company
SET address_city = "Dayton", address_state = "Ohio", address_country = "United States", revenue = "$44,179,000.00 ", employee_size = "261 "
WHERE domain = "ketteringhealth.org";
UPDATE company
SET address_city = "Shelbyville", address_state = "Tennessee", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "175 "
WHERE domain = "coopersteel.com";
UPDATE company
SET address_city = "Atlanta", address_state = "Georgia", address_country = "United States", revenue = "$159,000,000.00 ", employee_size = "794 "
WHERE domain = "trx.com";
UPDATE company
SET address_city = "Los Alamitos", address_state = "California", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "75 "
WHERE domain = "secsol.com";
UPDATE company
SET address_city = "McLean", address_state = "Virginia", address_country = "United States", revenue = "$6,560,000,000.00 ", employee_size = "152,000 "
WHERE domain = "hiltonworldwide.com";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "brochsteins.com";
UPDATE company
SET address_city = "Washington Dc", address_state = "District of Columbia", address_country = "United States", revenue = "$370,000,000.00 ", employee_size = "1,850 "
WHERE domain = "advisory.com";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$7,500,000.00 ", employee_size = "175 "
WHERE domain = "optics1.com";
UPDATE company
SET address_city = "Kaukauana", address_state = "Wisconsin", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "kellerbuilds.com";
UPDATE company
SET address_city = "Maywood", address_state = "Illinois", address_country = "United States", revenue = "$31,019,000.00 ", employee_size = "75 "
WHERE domain = "loyolamedicine.org";
UPDATE company
SET address_city = "Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "donhamandsweeney.com";
UPDATE company
SET address_city = "Las Vegas", address_state = "Nevada", address_country = "United States", revenue = "$22,224,000.00 ", employee_size = "259 "
WHERE domain = "bellagioresort.com";
UPDATE company
SET address_city = "Herndon", address_state = "Virginia", address_country = "United States", revenue = "$150,000,000.00 ", employee_size = "750 "
WHERE domain = "softential.com";
UPDATE company
SET address_city = "Kirkland", address_state = "Washington", address_country = "United States", revenue = "$5,628,000.00 ", employee_size = "70 "
WHERE domain = "fairfaxhospital.com";
UPDATE company
SET address_city = "Seattle", address_state = "Washington", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "taphandles.com";
UPDATE company
SET address_city = "Grand Haven", address_state = "Michigan", address_country = "United States", revenue = "$12,049,000.00 ", employee_size = "159 "
WHERE domain = "satruck.org";
UPDATE company
SET address_city = "Fountain Valley", address_state = "California", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "houseofbatteries.com";
UPDATE company
SET address_city = "Scottsdale", address_state = "Arizona", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "oneneck.com";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "zmkgroup.com";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "zocalogroup.com";
UPDATE company
SET address_city = "Erlanger", address_state = "Kentucky", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "175 "
WHERE domain = "fnsheppard.com";
UPDATE company
SET address_city = "Golden Valley", address_state = "Minnesota", address_country = "United States", revenue = "$55,073,000.00 ", employee_size = "209 "
WHERE domain = "careersathoneywell.com";
UPDATE company
SET address_city = "Oakland", address_state = "California", address_country = "United States", revenue = "$20,918,000.00 ", employee_size = "129 "
WHERE domain = "sanfrancisco.com";
UPDATE company
SET address_city = "Saint Louis", address_state = "Missouri", address_country = "United States", revenue = "$391,000,000.00 ", employee_size = "1,874 "
WHERE domain = "perficient.com";
UPDATE company
SET address_city = "Worcester", address_state = "Massachusetts", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "375 "
WHERE domain = "columbiatech.com";
UPDATE company
SET address_city = "Belmont", address_state = "California", address_country = "United States", revenue = "$7,678,000.00 ", employee_size = "71 "
WHERE domain = "sequoiamedicalgroup.org";
UPDATE company
SET address_city = "St Paul", address_state = "Minnesota", address_country = "United States", revenue = "$780,000,000.00 ", employee_size = "3,900 "
WHERE domain = "lawson.com";
UPDATE company
SET address_city = "Denver", address_state = "Colorado", address_country = "United States", revenue = "$19,904,000.00 ", employee_size = "216 "
WHERE domain = "nationaljewish.org";
UPDATE company
SET address_city = "Seattle", address_state = "Washington", address_country = "United States", revenue = "$10,356,000.00 ", employee_size = "108 "
WHERE domain = "smeincofseattle.com";
UPDATE company
SET address_city = "Lebanon", address_state = "Pennsylvania", address_country = "United States", revenue = "$13,787,000.00 ", employee_size = "203 "
WHERE domain = "gshleb.org";
UPDATE company
SET address_city = "St. Peters", address_state = "Missouri", address_country = "United States", revenue = "$22,703,000.00 ", employee_size = "137 "
WHERE domain = "sunedisonsilicon.com";
UPDATE company
SET address_city = "Seattle", address_state = "Washington", address_country = "United States", revenue = "$7,142,000.00 ", employee_size = "59 "
WHERE domain = "whi.org";
UPDATE company
SET address_city = "Waltham", address_state = "Massachusetts", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "archivas.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "175 "
WHERE domain = "cheetahmail.com";
UPDATE company
SET address_city = "Baltimore", address_state = "Maryland", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "375 "
WHERE domain = "vips.com";
UPDATE company
SET address_city = "Washington", address_state = "District of Columbia", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "175 "
WHERE domain = "aepa.com";
UPDATE company
SET address_city = "Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$34,012,000.00 ", employee_size = "304 "
WHERE domain = "dana-farber.org";
UPDATE company
SET address_city = "Altoona", address_state = "Pennsylvania", address_country = "United States", revenue = "$34,842,000.00 ", employee_size = "375 "
WHERE domain = "iu08.org";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "jwincorporated.com";
UPDATE company
SET address_city = "Merced", address_state = "California", address_country = "United States", revenue = "$5,443,000.00 ", employee_size = "195 "
WHERE domain = "mercymercedcares.org";
UPDATE company
SET address_city = "Springfield", address_state = "Massachusetts", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "pridestores.com";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "archgrille.com";
UPDATE company
SET address_city = "Washington Dc", address_state = "District of Columbia", address_country = "United States", revenue = "$102,000,000.00 ", employee_size = "750 "
WHERE domain = "amideast.org";
UPDATE company
SET address_city = "Fort Worth", address_state = "Texas", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "coopind.com";
UPDATE company
SET address_city = "Havre De Grace", address_state = "Maryland", address_country = "United States", revenue = "$25,673,000.00 ", employee_size = "175 "
WHERE domain = "fathermartinsashley.org";
UPDATE company
SET address_city = "Minneapolis", address_state = "Minnesota", address_country = "United States", revenue = "$10,487,000.00 ", employee_size = "106 "
WHERE domain = "uofmchildrenshospital.org";
UPDATE company
SET address_city = "Eagan", address_state = "Minnesota", address_country = "United States", revenue = "$600,000,000.00 ", employee_size = "3,000 "
WHERE domain = "pioneerplastic.com";
UPDATE company
SET address_city = "Alexandria", address_state = "Virginia", address_country = "United States", revenue = "$14,096,000.00 ", employee_size = "70 "
WHERE domain = "investprogram.org";
UPDATE company
SET address_city = "Colorado Springs", address_state = "Colorado", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "stuccousa.com";
UPDATE company
SET address_city = "St. Clairsville", address_state = "Ohio", address_country = "United States", revenue = "$38,228,000.00 ", employee_size = "498 "
WHERE domain = "healthplan.org";
UPDATE company
SET address_city = "Mandeville", address_state = "Louisiana", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "opsincusa.com";
UPDATE company
SET address_city = "Philadelphia", address_state = "Pennsylvania", address_country = "United States", revenue = "$175,000,000.00 ", employee_size = "3,000 "
WHERE domain = "jjwhiteinc.com";
UPDATE company
SET address_city = "Estes Park", address_state = "Colorado", address_country = "United States", revenue = "$38,221,000.00 ", employee_size = "375 "
WHERE domain = "ymcarockies.org";
UPDATE company
SET address_city = "Fairfax", address_state = "Virginia", address_country = "United States", revenue = "$2,000,000,000.00 ", employee_size = "10,000 "
WHERE domain = "cgi-ams.com";
UPDATE company
SET address_city = "Miami", address_state = "Florida", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "375 "
WHERE domain = "miami-airport.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "kennethpark.com";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "mtmayocorp.com";
UPDATE company
SET address_city = "Roseville", address_state = "Minnesota", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "hortoninc.com";
UPDATE company
SET address_city = "Jenkintown", address_state = "Pennsylvania", address_country = "United States", revenue = "$19,682,000.00 ", employee_size = "175 "
WHERE domain = "visitingangels.com";
UPDATE company
SET address_city = "Pasadena", address_state = "California", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "jbullock.com";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$23,333,000.00 ", employee_size = "175 "
WHERE domain = "ahima.org";
UPDATE company
SET address_city = "Kansas City", address_state = "Missouri", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "3,000 "
WHERE domain = "dstoutput.com";
UPDATE company
SET address_city = "CLEVELAND", address_state = "Ohio", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "375 "
WHERE domain = "metalstamping99.com";
UPDATE company
SET address_city = "Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$375,000,000.00 ", employee_size = "375 "
WHERE domain = "thecollaborative.com";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$350,000,000.00 ", employee_size = "7,500 "
WHERE domain = "katyisd.org";
UPDATE company
SET address_city = "Ann Arbor", address_state = "Michigan", address_country = "United States", revenue = "$6,599,000.00 ", employee_size = "72 "
WHERE domain = "mottchildren.org";
UPDATE company
SET address_city = "River Grove", address_state = "Illinois", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "clmmidwest.com";
UPDATE company
SET address_city = "Pittsford", address_state = "New York", address_country = "United States", revenue = "$46,327,000.00 ", employee_size = "69 "
WHERE domain = "earthlinkbusiness.com";
UPDATE company
SET address_city = "Detroit", address_state = "Michigan", address_country = "United States", revenue = "$446,000,000.00 ", employee_size = "12,500 "
WHERE domain = "dmc.org";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "ev1.net";
UPDATE company
SET address_city = "Palo Alto", address_state = "California", address_country = "United States", revenue = "$7,868,000.00 ", employee_size = "99 "
WHERE domain = "supportlpch.org";
UPDATE company
SET address_city = "Costa Mesa", address_state = "California", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "400 "
WHERE domain = "megapath.com";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "175 "
WHERE domain = "directenergy.com";
UPDATE company
SET address_city = "Norcross", address_state = "Georgia", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "allen-orton.com";
UPDATE company
SET address_city = "Yakima", address_state = "Washington", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "yakimaherald.com";
UPDATE company
SET address_city = "Indianapolis", address_state = "Indiana", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "175 "
WHERE domain = "angellearning.com";
UPDATE company
SET address_city = "Burbank", address_state = "California", address_country = "United States", revenue = "$410,000,000.00 ", employee_size = "2,918 "
WHERE domain = "ctscorp.com";
UPDATE company
SET address_city = "Milwaukie", address_state = "Oregon", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "sawchain.com";
UPDATE company
SET address_city = "Honolulu", address_state = "Hawaii", address_country = "United States", revenue = "$9,649,000.00 ", employee_size = "72 "
WHERE domain = "rehabhospital.org";
UPDATE company
SET address_city = "Russellville", address_state = "Alabama", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "citizensbank.ca";
UPDATE company
SET address_city = "Parkersburg", address_state = "West Virginia", address_country = "United States", revenue = "$8,766,000.00 ", employee_size = "224 "
WHERE domain = "camdenclark.org";
UPDATE company
SET address_city = "Springfield", address_state = "Missouri", address_country = "United States", revenue = "$6,920,000,000.00 ", employee_size = "61,909 "
WHERE domain = "oreillyauto.com";
UPDATE company
SET address_city = "San Jose", address_state = "California", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "ticoinc.com";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$34,000,000.00 ", employee_size = "170 "
WHERE domain = "projectleadership.net";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$178,000,000.00 ", employee_size = "6,155 "
WHERE domain = "mountsinai.org";
UPDATE company
SET address_city = "Cassville", address_state = "Missouri", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "arningco.com";
UPDATE company
SET address_city = "Seattle", address_state = "Washington", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "75 "
WHERE domain = "emotion.com";
UPDATE company
SET address_city = "Franklin", address_state = "Tennessee", address_country = "United States", revenue = "$12,783,000.00 ", employee_size = "74 "
WHERE domain = "clc.org.uk";
UPDATE company
SET address_city = "Redwood City", address_state = "California", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "allcovered.com";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$23,630,000.00 ", employee_size = "185 "
WHERE domain = "texaschildrenshealthplan.org";
UPDATE company
SET address_city = "Round Rock", address_state = "Texas", address_country = "United States", revenue = "$70,908,000.00 ", employee_size = "181 "
WHERE domain = "dellstorage.com";
UPDATE company
SET address_city = "Brisbane", address_state = "California", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "cloudforge.com";
UPDATE company
SET address_city = "Louisville", address_state = "Kentucky", address_country = "United States", revenue = "$87,167,000.00 ", employee_size = "359 "
WHERE domain = "bankers.com";
UPDATE company
SET address_city = "Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "matep.com";
UPDATE company
SET address_city = "Islandia", address_state = "New York", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "usu-ag.com";
UPDATE company
SET address_city = "Fairfax", address_state = "Virginia", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "175 "
WHERE domain = "networkats.com";
UPDATE company
SET address_city = "Richmond", address_state = "Virginia", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "175 "
WHERE domain = "afpind.com";
UPDATE company
SET address_city = "Milwaukee", address_state = "Wisconsin", address_country = "United States", revenue = "$4,589,000.00 ", employee_size = "223 "
WHERE domain = "froedert.com";
UPDATE company
SET address_city = "San Francisco", address_state = "California", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "279 "
WHERE domain = "jawbone.com";
UPDATE company
SET address_city = "San Jose", address_state = "California", address_country = "United States", revenue = "$2,210,000,000.00 ", employee_size = "4,143 "
WHERE domain = "brocade.com";
UPDATE company
SET address_city = "Vista", address_state = "California", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "csicommunications.com";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$76,001,000.00 ", employee_size = "750 "
WHERE domain = "habitat.org";
UPDATE company
SET address_city = "Newton", address_state = "Massachusetts", address_country = "United States", revenue = "$22,369,000.00 ", employee_size = "212 "
WHERE domain = "nwh.org";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$66,000,000.00 ", employee_size = "330 "
WHERE domain = "sidera.net";
UPDATE company
SET address_city = "Houston", address_state = "Texas", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "noah-consulting.com";
UPDATE company
SET address_city = "Killington", address_state = "Vermont", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "375 "
WHERE domain = "killington.com";
UPDATE company
SET address_city = "Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "tivoliaudio.com";
UPDATE company
SET address_city = "Meridian", address_state = "Idaho", address_country = "United States", revenue = "$3,000,000.00 ", employee_size = "75 "
WHERE domain = "russcorp.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$7,500,000.00 ", employee_size = "75 "
WHERE domain = "alcenvironmental.com";
UPDATE company
SET address_city = "Wayne", address_state = "New Jersey", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "bonlandhvac.com";
UPDATE company
SET address_city = "Washington Dc", address_state = "District of Columbia", address_country = "United States", revenue = "$41,403,000.00 ", employee_size = "375 "
WHERE domain = "maryscenter.org";
UPDATE company
SET address_city = "Laurel", address_state = "Maryland", address_country = "United States", revenue = "$37,500,000.00 ", employee_size = "175 "
WHERE domain = "irvineaccessfloors.com";
UPDATE company
SET address_city = "San Diego", address_state = "California", address_country = "United States", revenue = "$5,949,000.00 ", employee_size = "57 "
WHERE domain = "processing.org";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "phaeng.com";
UPDATE company
SET address_city = "Windsor", address_state = "Connecticut", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "morrisgroupinc.com";
UPDATE company
SET address_city = "Little Rock", address_state = "Arkansas", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "baldwinshell.com";
UPDATE company
SET address_city = "Hershey", address_state = "Pennsylvania", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "74 "
WHERE domain = "hersheypark.com";
UPDATE company
SET address_city = "Winston-Salem", address_state = "North Carolina", address_country = "United States", revenue = "$24,891,000.00 ", employee_size = "170 "
WHERE domain = "nationalgeneral.com";
UPDATE company
SET address_city = "Back Bay Boston", address_state = "Massachusetts", address_country = "United States", revenue = "$22,590,000.00 ", employee_size = "175 "
WHERE domain = "fsg.org";
UPDATE company
SET address_city = "Kingston", address_state = "New York", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "mtdbass.com";
UPDATE company
SET address_city = "Pittsburgh", address_state = "Pennsylvania", address_country = "United States", revenue = "$75,000,000.00 ", employee_size = "375 "
WHERE domain = "tmdholdings.com";
UPDATE company
SET address_city = "San Antonio", address_state = "Texas", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "175 "
WHERE domain = "wittigs.com";
UPDATE company
SET address_city = "New York", address_state = "New York", address_country = "United States", revenue = "$35,000,000.00 ", employee_size = "175 "
WHERE domain = "tbtgroup.net";
UPDATE company
SET address_city = "Brookfield", address_state = "Wisconsin", address_country = "United States", revenue = "$4,950,000,000.00 ", employee_size = "21,000 "
WHERE domain = "fiserv.com";
UPDATE company
SET address_city = "Matthews", address_state = "North Carolina", address_country = "United States", revenue = "$17,672,000.00 ", employee_size = "175 "
WHERE domain = "mecklenburgmedicalgroup.org";
UPDATE company
SET address_city = "Burlingame", address_state = "California", address_country = "United States", revenue = "$17,500,000.00 ", employee_size = "175 "
WHERE domain = "merrills.com";
UPDATE company
SET address_city = "", address_state = "", address_country = "", revenue = "$170,000,000.00 ", employee_size = "1,739 "
WHERE domain = "crowehorwath.com";
UPDATE company
SET address_city = "San Diego", address_state = "California", address_country = "United States", revenue = "$73,380,000.00 ", employee_size = "882 "
WHERE domain = "service-now.com";
UPDATE company
SET address_city = "Chicago", address_state = "Illinois", address_country = "United States", revenue = "$354,000,000.00 ", employee_size = "3,000 "
WHERE domain = "ama-assn.org";