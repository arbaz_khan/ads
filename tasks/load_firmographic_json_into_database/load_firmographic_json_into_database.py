# -*- coding: UTF-8 -*-

'''
Date: 2015-06-11
Author: Yuanfei
Task: https://everstring.atlassian.net/browse/AD-334
'''
import glob
import json
import csv

baseDir = "./autodesk_ad_campaign_comp_profile_nerichment"


def getCompanyName(c):
    companyName = c.get("comp_name")
    account = c.get("Account")
    if companyName is not None and len(companyName) > 0:
        return companyName
    else:
        return account


def getIndustry(c):
    industries = c.get("comp_industry")
    # a concated string, use this one if `comp_industry` is invalid
    industryStr = c.get("Industry")

    # only take the first level industry for UI display
    if isinstance(industries, list) and len(industries) > 0:
        return industries[0]
    elif industryStr is not None and len(industryStr) > 0:
        return industryStr
    else:
        return ""


def tryUpdateField(old_info, new_info, field):
    '''
    update the field of `old_info` with `new_info` if that's more detail
    '''
    if field == "industry":
        old_value = getIndustry(old_info)
        new_value = getIndustry(new_info)
    else:
        old_value = old_info.get(field)
        new_value = new_info.get(field)

    if new_value is None:
        new_value = ""
    if old_value is None:
        old_value = ""

    if len(new_value) > len(old_value):
        old_info[field] = new_value
    return old_info


if __name__ == "__main__":
    companyDict = {}

    for path in glob.glob(baseDir + '/*.json'):
        f = open(path)

        # these json files are not valid json, fix here
        jsonStr = "["
        jsonStr += ",".join(f.readlines())
        jsonStr += "]"

        companies = json.loads(jsonStr)

        for c in companies:
            domain = c["Domain Info"]
            # ingore the empty domain
            if len(domain) == 0:
                continue

            if not companyDict.has_key(domain):
                companyDict[domain] = {
                    "domain": domain,
                    "name": c["comp_name"],
                    "industry": getIndustry(c),
                    "revenue": c["Revenue"],
                    "employeeSize": c["Employees"],
                    "addressStreet": c["comp_street"],
                    "addressCity": c["comp_city"],
                    "addressState": c["comp_state"],
                    "addressCountry": c["comp_country"],
                    "description": c.get("description") or ""
                }
            else:
                company = companyDict[domain]

                keys = ("name", "industry", "revenue", "employeeSize", 
                        "addressStreet", "addressCity", "addressState", "addressCountry", 
                        "description")

                for key in keys:
                    tryUpdateField(company, c, key)
                companyDict[domain] = company

    with open("autodesk_ad_campaign_comp_profile_enrichment.csv", "w") as fout:
        keys = ("domain", "name", "industry", "revenue", "employeeSize", 
                "addressStreet", "addressCity", "addressState", "addressCountry", 
                "description")
        writer = csv.DictWriter(fout, keys)
        writer.writeheader()
        for d, c in companyDict.items():
            try:
                c = {k: v.encode("utf-8") for (k, v) in c.items()}
                writer.writerow(c)
            except Exception, e:
                print c
                raise e
