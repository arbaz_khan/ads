DROP TABLE IF EXISTS `account_product_map`;
CREATE TABLE `account_product_map` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `account_id` bigint NOT NULL,
    `product_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_account (`account_id`),
    KEY idx_product (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `account_id` bigint NOT NULL comment 'create by specified platform',
    `platform_id` bigint NOT NULL comment 'ads platform id, from platfrom(id)',
    `status` tinyint NOT NULL DEFAULT 0 comment '0 means paused, 1 means alive',
    `name` varchar(256) NOT NULL DEFAULT '' comment 'ads account name',
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_account_platform (`account_id`, `platform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `address` varchar(1024) NOT NULL  COMMENT 'company address',
    `company_id` bigint NOT NULL DEFAULT 0 comment 'from company(id)',
    `longitude` double,
    `latitude` double,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `adgroup`;
CREATE TABLE `adgroup` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `campaign_id` bigint NOT NULL comment 'id from campaign(id)',
    `adgroup_id` bigint NOT NULL,
    `name` varchar(256) NOT NULL DEFAULT '' comment 'ads group name',
    `status` tinyint NOT NULL DEFAULT 0 comment '0 means paused, 1 means alive',
    `platform_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_campaign_adgroup(`campaign_id`, `adgroup_id`),
    KEY idx_adgroup(`adgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ad`;
CREATE TABLE `ad` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `adgroup_id` bigint NOT NULL comment 'id from group(id)',
    `ad_id` bigint NOT NULL,
    `name` varchar(256) NOT NULL DEFAULT '' comment 'ads  name',
    `status` tinyint(4) NOT NULL DEFAULT 0 comment '0 means paused, 1 means alive',
    `creative_id` bigint NOT NULL,
    `platform_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_adgroup_ad (`adgroup_id`, `ad_id`),
    KEY idx_ad (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `advertiser`;
CREATE TABLE `advertiser` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL DEFAULT '',
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `bid`;
CREATE TABLE `bid` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `platform_id` bigint NOT NULL,
    `bid_type` varchar(256) NOT NULL,
    `bid_amount`  bigint NOT NULL,
    `company_score_map_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company_score_map (`company_score_map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `budget_bid_history`;
CREATE TABLE `budget_bid_history` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `platform_id` bigint NOT NULL,
    `company_score_map_id` bigint NOT NULL,
    `budget_type` varchar(256) NOT NULL,
    `budget_amount`  bigint NOT NULL,
    `bid_type` varchar(256) NOT NULL,
    `bid_amount`  bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company_score_map (`company_score_map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `budget`;
CREATE TABLE `budget` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `budget_id` bigint(20) DEFAULT NULL,
    `platform_id` bigint NOT NULL,
    `budget_type` varchar(256) NOT NULL DEFAULT '',
    `budget_amount` bigint NOT NULL,
    `company_score_map_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company_score (`company_score_map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `campaign_company_map`;
CREATE TABLE `campaign_company_map` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `campaign_id` bigint NOT NULL,
    `adgroup_id` bigint NOT NULL,
    `united_campaign_id` bigint NOT NULL,
    `company_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_adgroup (`adgroup_id`),
    KEY idx_campaign (`campaign_id`),
    KEY idx_product_company (`united_campaign_id`, `company_id`),
    KEY idx_company (`company_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS campaign_config;
CREATE TABLE `campaign_config` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `united_campaign_id` bigint NOT NULL,
    `platform_id` bigint NOT NULL,
    `key` varchar(256) NOT NULL,
    `value` varchar(1024) NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_product (`united_campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `campaign`;
CREATE TABLE `campaign` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `account_id` bigint NOT NULL comment 'account id from account(id)',
    `campaign_id` bigint NOT NULL,
    `name` varchar(256) NOT NULL DEFAULT '' comment 'ads campaign name',
    `status` tinyint NOT NULL DEFAULT 0 comment '0 means paused, 1 means alive',
    `start_time` datetime DEFAULT NULL,
    `end_time` datetime DEFAULT NULL,
    `platform_id` bigint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_account_campaign(`account_id`, `campaign_id`),
    KEY idx_campaign(`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `company_score_map`;
CREATE TABLE `company_score_map` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `united_campaign_id` bigint NOT NULL,
    `company_id` bigint NOT NULL DEFAULT -1 COMMENT 'Lead',
    `score` tinyint NOT NULL DEFAULT -1 COMMENT 'Fit Score',
    `engagement_score` tinyint NOT NULL DEFAULT -1 COMMENT 'Engagement Score',
    `intent_score` tinyint NOT NULL DEFAULT -1 COMMENT 'Intent Score',
    `grade` char(1) NOT NULL DEFAULT '',
    `control_group` tinyint NOT NULL DEFAULT 0,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_product_company (`united_campaign_id`,`company_id`),
    KEY idx_company (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL DEFAULT '',
    `domain` varchar(256) NOT NULL DEFAULT '',
    `industry` varchar(256) NOT NULL DEFAULT '',
    `employee_size` varchar(256) NOT NULL DEFAULT '',
    `revenue` varchar(256) COMMENT '`revenue` is to be a descriptive string, e.g. "$500K - $1M"',
    `address_street` varchar(1000),
    `address_city` varchar(256),
    `address_state` varchar(256),
    `address_country` varchar(256),
    `description` text,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `creative_history`;
CREATE TABLE `creative_history` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `creative_history_id` bigint NOT NULL,
    `united_campaign_id` bigint NOT NULL,
    `landing_page_url` varchar(256) NOT NULL DEFAULT '',
    `headline` varchar(256) NOT NULL DEFAULT '',
    `description_1` varchar(256) NOT NULL DEFAULT '',
    `description_2` varchar(256) NOT NULL DEFAULT '',
    `image_url` varchar(256) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY idx_product (`united_campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `creative`;
CREATE TABLE `creative` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `united_campaign_id` bigint NOT NULL,
    `platform_id` bigint NOT NULL,
    `platform_creative_id` bigint NOT NULL DEFAULT 0,
    `landing_page_url` varchar(256) NOT NULL DEFAULT '',
    `headline` varchar(256) NOT NULL DEFAULT '',
    `description_1` varchar(256) DEFAULT '',
    `description_2` varchar(256) DEFAULT '',
    `image_url` varchar(256) NOT NULL DEFAULT '',
    `status` tinyint NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_product (`united_campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `domain` varchar(256) NOT NULL COMMENT 'company domain',
    `company_id` bigint NOT NULL comment 'from company(id)',
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `fb_company`;
CREATE TABLE `fb_company` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `company_id` bigint NOT NULL,
    `fb_company_id` bigint NOT NULL,
    `fb_company_name` varchar(256) NOT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `insight_by_adgroup`;
CREATE TABLE `insight_by_adgroup` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `platform_id` bigint NOT NULL,
    `account_id` bigint NOT NULL,
    `adgroup_id` bigint NOT NULL,
    `adgroup_name` varchar(256) NOT NULL,
    `platform_adgroup_id` bigint NOT NULL,
    `reach` bigint NOT NULL DEFAULT 0,
    `frequency` double NOT NULL DEFAULT 0,
    `impressions` bigint NOT NULL DEFAULT 0,
    `unique_impressions` bigint NOT NULL DEFAULT 0,
    `cpm` double NOT NULL DEFAULT 0,
    `spend` double NOT NULL DEFAULT 0,
    `clicks` bigint NOT NULL DEFAULT 0,
    `unique_clicks` bigint NOT NULL DEFAULT 0,
    `ctr` double NOT NULL DEFAULT 0,
    `cpc` double NOT NULL DEFAULT 0,
    `conversion` bigint NOT NULL DEFAULT 0,
    `start_time` datetime NOT NULL,
    `end_time` datetime NOT NULL,
    `retrieve_time` datetime NOT NULL,
    PRIMARY KEY (`id`),
    KEY idx_adgroup (`adgroup_id`),
    KEY idx_account (`account_id`),
    KEY idx_time (`start_time`, `end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `insight_by_ad`;
CREATE TABLE `insight_by_ad` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `platform_id` bigint NOT NULL,
    `account_id` bigint NOT NULL,
    `ad_id` bigint NOT NULL,
    `ad_name` varchar(256) NOT NULL,
    `platform_ad_id` bigint NOT NULL,
    `reach` bigint NOT NULL DEFAULT 0,
    `frequency` double NOT NULL DEFAULT 0,
    `impressions` bigint NOT NULL DEFAULT 0,
    `unique_impressions` bigint NOT NULL DEFAULT 0,
    `cpm` double NOT NULL DEFAULT 0,
    `spend` double NOT NULL DEFAULT 0,
    `clicks` bigint NOT NULL DEFAULT 0,
    `unique_clicks` bigint NOT NULL DEFAULT 0,
    `ctr` double NOT NULL DEFAULT 0,
    `cpc` double NOT NULL DEFAULT 0,
    `conversion` bigint NOT NULL DEFAULT 0,
    `start_time` datetime NOT NULL,
    `end_time` datetime NOT NULL,
    `retrieve_time` datetime NOT NULL,
    PRIMARY KEY (`id`),
    KEY idx_ad (`ad_id`),
    KEY idx_account (`account_id`),
    KEY idx_time (`start_time`, `end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `insight_by_campaign`;
CREATE TABLE `insight_by_campaign` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `platform_id` bigint NOT NULL,
    `account_id` bigint NOT NULL,
    `campaign_id` bigint NOT NULL,
    `campaign_name` varchar(256) NOT NULL,
    `platform_campaign_id` bigint NOT NULL,
    `reach` bigint NOT NULL DEFAULT 0,
    `frequency` double NOT NULL DEFAULT 0,
    `impressions` bigint NOT NULL DEFAULT 0,
    `unique_impressions` bigint NOT NULL DEFAULT 0,
    `cpm` double NOT NULL DEFAULT 0,
    `spend` double NOT NULL DEFAULT 0,
    `clicks` bigint NOT NULL DEFAULT 0,
    `unique_clicks` bigint NOT NULL DEFAULT 0,
    `ctr` double NOT NULL DEFAULT 0,
    `cpc` double NOT NULL DEFAULT 0,
    `conversion` bigint NOT NULL DEFAULT 0,
    `start_time` datetime NOT NULL,
    `end_time` datetime NOT NULL,
    `retrieve_time` datetime NOT NULL,
    PRIMARY KEY (`id`),
    KEY idx_campagin (`campaign_id`),
    KEY idx_account (`account_id`),
    KEY idx_time (`start_time`, `end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `insight`;
CREATE TABLE `insight` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `united_campaign_id` bigint NOT NULL,
    `company_id` bigint NOT NULL,
    `reach` bigint NOT NULL DEFAULT 0,
    `frequency` double NOT NULL DEFAULT 0,
    `impressions` bigint NOT NULL DEFAULT 0,
    `unique_impressions` bigint NOT NULL DEFAULT 0,
    `cpm` double NOT NULL DEFAULT 0,
    `spend` double NOT NULL DEFAULT 0,
    `clicks` bigint NOT NULL DEFAULT 0,
    `unique_clicks` bigint NOT NULL DEFAULT 0,
    `ctr` double NOT NULL DEFAULT 0,
    `cpc` double NOT NULL DEFAULT 0,
    `conversion` bigint NOT NULL DEFAULT 0,
    `start_time` datetime NOT NULL,
    `end_time` datetime NOT NULL,
    `retrieve_time` datetime,
    PRIMARY KEY (`id`),
    KEY idx_insight (`united_campaign_id`, `company_id`),
    KEY idx_time (`start_time`, `end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ip`;
CREATE TABLE `ip` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `ip_start` varchar(256) NOT NULL COMMENT 'start of ip range',
    `ip_end` varchar(256) NOT NULL COMMENT 'end of ip range',
    `company_id` bigint NOT NULL comment 'from company(id)',
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_company (`company_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `keyword`;
CREATE TABLE `keyword` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `united_campaign_id` bigint NOT NULL,
    `keyword` varchar(256) NOT NULL DEFAULT '',
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_product (`united_campaign_id`, `keyword`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `platform`;
CREATE TABLE `platform` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL DEFAULT ' ' COMMENT 'google, facebook, or others',
  `create_time` datetime,
  `update_time` datetime,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;DROP TABLE IF EXISTS product;
CREATE TABLE `product` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL DEFAULT '',
    `advertiser_id` bigint not null comment 'advertiser company id , from company(id)',
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY idx_advertiser_product (`advertiser_id`, `name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `united_campaign`;
CREATE TABLE `united_campaign` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `product_id` bigint NOT NULL,
    `name` varchar(256) NOT NULL DEFAULT '',
    `budget` double DEFAULT 0.0,
    `status` tinyint NOT NULL DEFAULT 0 comment '0 means paused, 1 means alive',
    `start_time` datetime DEFAULT NULL,
    `end_time` datetime DEFAULT NULL,
    `create_time` datetime DEFAULT NULL,
    `update_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY idx_product (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
