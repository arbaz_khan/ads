# coding=utf-8
from ads.service.common import config_service
import adwords_client
import campaign_service
import adgroup_service
import budget_service
from model import ads_prepare_interface
from model import ads_interface
from utils import log
import traceback

__author__ = 'dianqiang'


class AdvertisingService(object):

    """Public service for google part .

    Attributes:
      adwords_client: An class instance, which you can use to link google api.
    """

    PLATFORM_NAME = 'google'

    def __init__(self):
        self.logger = log.Logger.get_logger()

    def create_campaigns_common(self, results, advertiser_id, ucampaign_id,
                                targeting_obj_list, leads_rating,
                                budget_bid_info=None):
        """Create campaigns batch ,it will create campaigns one by one.

        Keyword arguments:
        leads_info -- A list of object, include domain,target company
            name etc.
        campaign_parameters -- A object, include campaign init status,
            start date, end date .
        ad_info_list -- A list of object, inclue image name, display url,
            landing url, uri etc.
        budget -- A object, include budget
            type('STRANDARD', 'ACCELERATED'),
            budget amount(cent as unit) .
        bidding -- A object, include bidding
            type('CpaBid','CpcBid','CpmBid'), bid amount(cent as unit).
        ad_type -- Diff text ad and image ad.
        keywords -- A list of search key word.
        """
        try:
            ads_inter = ads_interface.AdsInterface()
            ads_pre = ads_prepare_interface.AdsPrepareInterface()
            # get platform id
            platform_id = ads_pre.get_platform_id(
                AdvertisingService.PLATFORM_NAME)
            advertiser = ads_pre.get_advertiser_by_id(advertiser_id)
            product = ads_pre.get_product_by_ucampaign(ucampaign_id)
            # accout obj
            accounts = ads_pre.get_accounts(platform_id, product.id)
            account_id = accounts[0].id
            google_account_id = accounts[0].account_id
            adwords_client_obj = adwords_client.GoogleAdwordClient(
                google_account_id).get_client()
            # get google search key words list
            keywords = ads_pre.get_keywords(ucampaign_id)
            # get image ad creatives list
            creatives = ads_pre.get_creatives(ucampaign_id, platform_id)
            # get campaign params
            conf_service = config_service.ConfigService()
            camp_confs = conf_service.get_campaign_configs(
                ucampaign_id, platform_id)
            cs_ins = campaign_service.GoogleCampaignService(
                adwords_client_obj, account_id)
            success_num = 0
            failure_num = 0
            ignored_num = 0
            for targeting_obj in targeting_obj_list:
                lead = targeting_obj.lead
                if len(ads_inter.
                        get_platform_campaign_company_(ucampaign_id,
                                                       lead.company_id,
                                                       platform_id)) > 0:
                    ignored_num = ignored_num + 1
                    continue
                # get long lat list
                lat_log_list = ads_pre.get_company_address(lead.company_id)
                if len(lat_log_list) == 0:
                    failure_num = failure_num + 1
                    self.logger.info(
                        'campaign name \'%s\' get geo location fail.' %
                        lead.name)
                    continue
                # get budget id
                budget_key = ads_inter.get_budget(platform_id, lead.id).id
                campaign_name = advertiser.name + '#' + \
                    product.name + '#' + leads_rating
                if lead.control_group == 1:
                    campaign_name = campaign_name + '#Control'
                else:
                    campaign_name = campaign_name + '#Test'
                campaign_name = campaign_name + '#' + lead.name
                res = cs_ins.create_campaigns(
                    ucampaign_id, campaign_name, product, lead, camp_confs,
                    budget_bid_info, creatives, keywords, lat_log_list,
                    budget_key)
                if res == 1:
                    success_num = success_num + 1
                else:
                    failure_num = failure_num + 1
            self.logger.info('Target result: %s success,%s faileure,%s ignored'
                             % (success_num, failure_num, ignored_num))
        except:
            traceback.print_exc()

    def update_budget_common(self, results, ucampaign_id, budget_info):
        """Update budget type,budget amount by product_id

        Keyword arguments:
        budget -- include budget type,budget amount(cent transfered,
            convert it to micros by multipling by 10000)
        """

        try:
            ads = ads_interface.AdsInterface()
            ads_pre = ads_prepare_interface.AdsPrepareInterface()
            # get platform id
            platform_id = ads_pre.get_platform_id(
                AdvertisingService.PLATFORM_NAME)
            # accout obj
            accounts = ads_pre.get_accounts(platform_id, ucampaign_id)
            google_account_id = accounts[0].account_id

            adwords_client_obj = adwords_client.GoogleAdwordClient(
                google_account_id).get_client()
            bs_ins = budget_service.GoogleBudgetService(adwords_client_obj)
            leads = ads_pre.get_all_leads_info(ucampaign_id)
            for lead in leads:
                budget = ads.get_budget(platform_id, lead.id)
                if budget.budget_id:
                    bs_ins.update_budget(budget.budget_id, budget_info)
                ads.update_budget_bid(platform_id, lead.id, budget_info)
        except:
            traceback.print_exc()

    def update_bidding_common(self, result_q, ucampaign_id, bid_info):
        """Update bid type,bid amount by product_id

        Keyword arguments:
        bidding -- include bid type,bid amount(cent transfered,convert
            it to micros by multipling by 10000)
        """

        try:
            ads = ads_interface.AdsInterface()
            ads_pre = ads_prepare_interface.AdsPrepareInterface()
            # get platform id
            platform_id = ads_pre.get_platform_id(
                AdvertisingService.PLATFORM_NAME)
            # accout obj
            accounts = ads_pre.get_accounts(platform_id, ucampaign_id)
            account_id = accounts[0].id
            google_account_id = accounts[0].account_id

            adwords_client_obj = adwords_client.GoogleAdwordClient(
                google_account_id).get_client()
            adgroup_ins = adgroup_service.GoogleGroupService(
                adwords_client_obj)
            # get platform adgroup id ,update batch
            campaign_id_names = ads.get_campaign_by_account(account_id)
            ids = []
            for id, campaign_id, campaign_name in campaign_id_names:
                ids.append(id)
            group_ids = ads.get_adgroup_ids_by_campaign(ids)
            adgroup_ids = []
            for id, group_id in group_ids:
                adgroup_ids.append(group_id)
            adgroup_ins.update_bid(adgroup_ids, bid_info)
            leads = ads_pre.get_all_leads_info(ucampaign_id)
            for lead in leads:
                ads.update_budget_bid(platform_id, lead.id, bid_info)
        except:
            traceback.print_exc()

    def update_status_common(self, result_q, ucampaign_id, status):
        try:
            ads = ads_interface.AdsInterface()
            ads_pre = ads_prepare_interface.AdsPrepareInterface()
            platform_id = ads_pre.get_platform_id(
                AdvertisingService.PLATFORM_NAME)
            # get account
            accounts = ads_pre.get_accounts(platform_id, ucampaign_id)
            account_id = accounts[0].id
            google_account_id = accounts[0].account_id
            campaigns = ads.get_campaigns_by_account(account_id)
            adwords_client_obj = adwords_client.GoogleAdwordClient(
                google_account_id).get_client()
            cs_ins = campaign_service.GoogleCampaignService(
                adwords_client_obj, account_id)
            for campaign in campaigns:
                cs_ins.update_status(campaign.campaign_id, status)
                ads.update_campaign(campaign.id, None, status)
        except:
            traceback.print_exc()
