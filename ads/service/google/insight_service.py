import csv
import datetime
from ads.service.google import adwords_client
from ads.model import insight_by_campaign
from ads.model import insight_by_adgroup
from ads.model import insight_by_ad
from ads.model.ads_insight_interface import AdsInsightInterface
from ads.model import ads_prepare_interface
from ads.model.ads_interface import AdsInterface

__author__ = 'dianqiang'


class GoogleInsightService(object):
    PLATFORM_NAME = 'google'
    SPEND_BASE = 1000000

    def __init__(self, adwords_client, platform_account_id):
        ads_pre = ads_prepare_interface.AdsPrepareInterface()
        self.platform_id = ads_pre.get_platform_id(
            GoogleInsightService.PLATFORM_NAME)
        self.adwords_client = adwords_client
        # self.platform_account_id = platform_account_id
        print self.platform_id, platform_account_id
        self.account_id = 1
        self.report_downloader = self.adwords_client.GetReportDownloader(
            version='v201502')

    # if start_date, end_date is None, will return today's spent
    def get_spent(self, start_date=None, end_date=None):
        today = datetime.datetime.utcnow().today()
        tomorrow = today + datetime.timedelta(days=1)
        today_str = today.strftime('%Y-%m-%d')
        tomorrow_str = tomorrow.strftime('%Y-%m-%d')
        date_range = None
        if start_date is not None:
            if end_date is not None:
                date_range = start_date + ',' + end_date
            else:
                date_range = start_date + ',' + tomorrow_str
        else:
            date_range = today_str + ',' + tomorrow_str

        print date_range
        report_query = ('''SELECT
                           CampaignId,CampaignName,Cost
                        FROM CAMPAIGN_PERFORMANCE_REPORT DURING''' + date_range
                        )
        report_str = self.report_downloader.DownloadReportAsStringWithAwql(
            report_query, 'CSV', skip_report_summary=True,
            skip_report_header=True)
        report_str = unicode(report_str).encode('utf-8')
        report_lines = report_str.split('\n')
        reader = csv.DictReader(report_lines)
        insights = []
        for row in reader:
            insights.append(
                (row['Campaign ID'],
                 float(row['Cost']) / GoogleInsightService.SPEND_BASE))
        return insights

    def get_ad_sights_by_campaign(self, start_date, end_date):
        date_range = start_date + ',' + end_date
        insights = []
        report_query = ('''SELECT
                           Date,CampaignId,CampaignName,ImpressionReach,
                           AverageFrequency,Impressions,ImpressionReach,
                           AverageCpm,Cost,Clicks,Ctr,AverageCpc,
                           ConversionsManyPerClick
                        FROM CAMPAIGN_PERFORMANCE_REPORT DURING''' + date_range
                        )
        report_str = self.report_downloader.DownloadReportAsStringWithAwql(
            report_query, 'CSV', skip_report_summary=True,
            skip_report_header=True)
        report_str = unicode(report_str).encode('utf-8')
        report_lines = report_str.split('\n')
        reader = csv.DictReader(report_lines)
        for row in reader:
            insight = insight_by_campaign.InsightByCampaign()
            campaign = AdsInterface.\
                get_campaign_by_platform_campaign(self.platform_id,
                                                  row['Campaign ID'])
            insight.platform_id = self.platform_id
            insight.account_id = self.account_id
            insight.platform_campaign_id = row['Campaign ID']
            insight.campaign_id = row[
                'Campaign ID'] if campaign is None else campaign.id
            insight.campaign_name = row['Campaign']
            reach_num = row['Unique cookies']
            if(reach_num == '< 100'):
                reach_num = 0
            insight.reach = reach_num
            insight.frequency = row['Avg. impr. freq. per cookie']
            insight.impressions = row['Impressions']
            insight.unique_impressions = reach_num
            insight.cpm = float(
                row['Avg. CPM']) / GoogleInsightService.SPEND_BASE
            insight.spend = float(
                row['Cost']) / GoogleInsightService.SPEND_BASE
            insight.clicks = row['Clicks']
            insight.unique_clicks = row['Clicks']
            insight.ctr = float(row['CTR'].strip('%')) / 100
            insight.cpc = float(
                row['Avg. CPC']) / GoogleInsightService.SPEND_BASE
            insight.conversion = row['Conversions']
            insight.retrieve_time = datetime.datetime.utcnow()
            insight.start_time = start_date
            insight.end_time = end_date
            insights.append(insight)
        AdsInsightInterface.create_insights(insights)

    def get_ad_sights_by_ad_group(self, start_date, end_date):
        date_range = start_date + ',' + end_date
        insights = []
        report_query = ('SELECT'
                        '   Date,CampaignId,CampaignName,AdGroupId,'
                        '   AdGroupName,Impressions,AverageCpm,Cost,'
                        '   Clicks,Ctr,AverageCpc,ConversionsManyPerClick '
                        'FROM ADGROUP_PERFORMANCE_REPORT '
                        'DURING ' + date_range
                        )
        report_str = self.report_downloader.DownloadReportAsStringWithAwql(
            report_query, 'CSV', skip_report_summary=True,
            skip_report_header=True)
        report_str = unicode(report_str).encode('utf-8')
        report_lines = report_str.split('\n')
        reader = csv.DictReader(report_lines)
        for row in reader:
            insight = insight_by_adgroup.InsightByAdgroup()
            adgroup = AdsInterface.get_adgroup_by_platform_adgroup(
                self.platform_id, row['Ad group ID'])
            insight.platform_id = self.platform_id
            insight.account_id = self.account_id
            insight.adgroup_id = row[
                'Ad group ID'] if adgroup is None else adgroup.id
            insight.adgroup_name = row['Ad group']
            insight.platform_adgroup_id = row['Ad group ID']
            insight.reach = 0
            insight.frequency = 0
            insight.impressions = row['Impressions']
            insight.unique_impressions = 0
            insight.cpm = float(
                row['Avg. CPM']) / GoogleInsightService.SPEND_BASE
            insight.spend = float(
                row['Cost']) / GoogleInsightService.SPEND_BASE
            insight.clicks = row['Clicks']
            insight.unique_clicks = 0
            insight.ctr = float(row['CTR'].strip('%')) / 100
            insight.cpc = float(
                row['Avg. CPC']) / GoogleInsightService.SPEND_BASE
            insight.conversion = row['Conversions']
            insight.retrieve_time = insight.retrieve_time = \
                datetime.datetime.utcnow()
            insight.start_time = start_date
            insight.end_time = end_date
            insights.append(insight)
        AdsInsightInterface.create_insights(insights)

    def get_ad_sights_by_ad(self, start_date, end_date):
        date_range = start_date + ',' + end_date
        insights = []
        report_query = ('SELECT'
                        '   Date,CampaignId,CampaignName,AdGroupId,'
                        '   AdGroupName,Id,ImageCreativeName,Impressions,'
                        '   AverageCpm,Cost,Clicks,Ctr,AverageCpc,'
                        '   ConversionsManyPerClick '
                        'FROM AD_PERFORMANCE_REPORT '
                        'DURING ' + date_range
                        )
        report_str = self.report_downloader.DownloadReportAsStringWithAwql(
            report_query, 'CSV', skip_report_summary=True,
            skip_report_header=True)
        report_str = unicode(report_str).encode('utf-8')

        report_lines = report_str.split('\n')
        reader = csv.DictReader(report_lines)
        for row in reader:
            insight = insight_by_ad.InsightByAd()
            ad = AdsInterface.get_ad_by_platform_ad(
                self.platform_id, row['Ad ID'])
            insight.platform_id = self.platform_id
            insight.account_id = self.account_id
            insight.ad_id = row['Ad ID'] if ad is None else ad.id
            insight.ad_name = row['Image ad name']
            insight.platform_ad_id = row['Ad ID']
            insight.reach = 0
            insight.frequency = 0
            insight.impressions = row['Impressions']
            insight.unique_impressions = 0
            insight.cpm = float(
                row['Avg. CPM']) / GoogleInsightService.SPEND_BASE
            insight.spend = float(
                row['Cost']) / GoogleInsightService.SPEND_BASE
            insight.clicks = row['Clicks']
            insight.unique_clicks = 0
            insight.ctr = float(row['CTR'].strip('%')) / 100
            insight.cpc = float(
                row['Avg. CPC']) / GoogleInsightService.SPEND_BASE
            insight.conversion = row['Conversions']
            insight.retrieve_time = insight.retrieve_time = \
                datetime.datetime.utcnow()
            insight.start_time = start_date
            insight.end_time = end_date
            insights.append(insight)
        # AdsInsightInterface.create_insights(insights)

if __name__ == '__main__':
    google_account_id = '6089274671'
    client_obj = adwords_client.GoogleAdwordClient(
        google_account_id).get_client()
    insightobj = GoogleInsightService(client_obj, google_account_id)
    today = datetime.date.today()
    yesterday = today - datetime.timedelta(days=1)
    start_date = datetime.datetime.strptime(
        str(yesterday), '%Y-%m-%d').strftime('%Y%m%d')
    end_date = datetime.datetime.strptime(
        str(today), '%Y-%m-%d').strftime('%Y%m%d')
    date_range = '%s,%s' % (start_date, end_date)
    start_date = '20150429'
    end_date = '20150430'
    company_dict = insightobj.get_ad_sights_by_campaign(start_date, end_date)
    company_dict = insightobj.get_ad_sights_by_ad_group(start_date, end_date)
    company_dict = insightobj.get_ad_sights_by_ad(start_date, end_date)
