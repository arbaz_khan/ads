__author__ = 'dianqiang'
import sys
from os import path
sys.path.insert(0, path.dirname(path.realpath(__file__)))
sys.path.insert(0, path.dirname(path.dirname(path.realpath(__file__))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.realpath(__file__)))))
import base64
from model import ads_interface
from model import ads_prepare_interface
from utils import log
import uuid
class GoogleAdService(object):
    """Service in adgroup ad level.
    
    Attributes:
        adwords_client: An class instance ,which you can use to link google api .
        adgroup_ad_service: google api for adgroup ad level service .
        ads_interface: dao level interface
    """

    STATUS_ENABLE = 1
    PLATFORM = 'google'

    def __init__(self,adwords_client):

        self.logger=log.Logger.get_logger()
        self.adwords_client=adwords_client
        self.adgroup_ad_service=self.adwords_client.GetService('AdGroupAdService', version='v201409')
        self.ads_interface=ads_interface.AdsInterface()
        self.ads_pre=ads_prepare_interface.AdsPrepareInterface()

    def create_image_ad(self,ad_group_id, ad_group_key,creatives,image_data_map,base_ad_name):
        """Create a image ad with more image creatives
        
        Keyword arguments:
        ad_group_id: ad group id  google adwords api
        ad_group_key: the primary key of adgroup table
        ad_list: A list of object describe creative images info.
        """
        ad_name_creative_map = dict()
        operations=[]
        for creative in creatives:
            ad_name = base_ad_name+'#'+str(uuid.uuid4())
            operation = {
                    'operator': 'ADD',
                    'operand': {
                        'xsi_type': 'AdGroupAd',
                        'adGroupId': ad_group_id,
                        'ad': {
                            'xsi_type': 'ImageAd',
                            'displayUrl': creative.landing_page_url,
                            'url': creative.landing_page_url,
                            'image': {
                                'xsi_type': 'Image',
                                'data': image_data_map[creative.image_url],
                                'type': 'IMAGE'
                            },
                            'name': ad_name
                        },
                        'status': 'ENABLED'
                    }
                }
            operations.append(operation)
            ad_name_creative_map[ad_name] = creative
        image_ads = self.adgroup_ad_service.mutate(operations)
        ad_id_key_list=[]
        platform_id = self.ads_pre.get_platform_id(GoogleAdService.PLATFORM)
        for image_ad in image_ads['value']:
            self.logger.info('adgroup id \'%s\' Ad with id \'%s\' and of type \'%s\' was added' \
                  % (ad_group_id,image_ad['ad']['id'], image_ad['ad']['Ad.Type']))
            creative_ins = ad_name_creative_map[image_ad['ad']['name']]
            ad_key=self.ads_interface.set_ad(ad_group_key,image_ad['ad']['id'],creative_ins.platform_id,image_ad['ad']['name'],GoogleAdService.STATUS_ENABLE, creative_ins.id)
            ad_id_key_list.append((image_ad['ad']['id'],ad_key))
        return ad_id_key_list
