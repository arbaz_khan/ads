__author__ = 'dianqiang'
from googleads import adwords
import account_service
import adwords_client
class AccountService(object):

    def __init__(self,adwords_client):
        self.adwords_client=adwords_client
        self.managed_customer_service=self.adwords_client.GetService('ManagedCustomerService', version='v201409')

    def create_account(self,name):
        operations = [{
        'operator': 'ADD',
        'operand': {
            'name': name,
            'currencyCode': 'EUR',
            'dateTimeZone': 'Europe/London',
            }
        }]
        accounts = self.managed_customer_service.mutate(operations)
        for account in accounts['value']:
            print ('Account with customer ID \'%s\' was successfully created.'
            % account['customerId'])
        return account['customerId']

if __name__ == '__main__':
    (client_id, client_secret, refresh_token)=('876778996900-6vh1kmbkvj8bt5c6l3cr0qu1nbfugqaj.apps.googleusercontent.com','q_qGTBQwIbaL6m2d2SZCFFOt','1/bqDHgsR2j5_itiBbaBTi_8YRXaRyes7tB9WI0tZIL-wMEudVrK5jSpoR30zcRFq6')
    (developer_token,user_agent, client_customer_id)=('GSSq7FyPE_KBBXIIPp8AYg','everstring.com:AdwordsTry:V0.1','633-578-2857')
    client_obj = adwords_client.GoogleAdwordClient(client_id,client_secret,refresh_token,developer_token,user_agent,client_customer_id)
    adwords_client=client_obj.get_client()
    ac=AccountService(adwords_client)
    ac.create_account('test12');
