__author__ = 'dianqiang'
from googleads import adwords
from googleads import oauth2
import ConfigParser
import account_service

class GoogleAdwordClient(object):
    """Read google account info from config file
       Create client for link api
    """

    def __init__(self,client_customer_id):
        """Read google account info from config file  
        """

        from os import path
        #config_file= path.join(path.dirname(path.dirname(path.dirname(__file__))), 'conf/google_conf_online.ini')
        config_file= path.join(path.dirname(path.dirname(path.dirname(__file__))), 'conf/google_api_client.ini')
        config=ConfigParser.ConfigParser()
        with open(config_file,'r') as cfgfile:  
            config.readfp(cfgfile)  
        self.client_id=config.get('client','client_id')
        self.client_secret=config.get('client','client_secret')
        self.refresh_token=config.get('client','refresh_token')
        self.developer_token=config.get('client','developer_token')
        self.user_agent=config.get('client','user_agent')
        self.mcc_account=config.get('client','mcc_account')
        #self.client_customer_id=config.get('client','client_customer_id')
        self.client_customer_id=client_customer_id

    def get_client(self):
        """Get google api client ,which used to link google service
        """

        oauth2_client = oauth2.GoogleRefreshTokenClient(self.client_id, self.client_secret, self.refresh_token)
        adwords_client = adwords.AdWordsClient(self.developer_token, oauth2_client, self.user_agent, self.client_customer_id)
        return adwords_client

    def get_account(self):
        """get google account id,will be used in campaign service
        """

        return long(self.client_customer_id)

if __name__ == '__main__':
    client=GoogleAdwordClient()
    client.get_client()
