__author__ = 'dianqiang'
from googleads import adwords
import sys
import uuid
import time
import traceback
import base64
from os import path
sys.path.insert(0, path.dirname(path.realpath(__file__)))
sys.path.insert(0, path.dirname(path.dirname(path.realpath(__file__))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.realpath(__file__)))))
import budget_service
import campaign_criterion
import adgroup_service
import ad_service
import adwords_client
import campaign_criterion
from model import ads_interface
from model import ads_prepare_interface
from utils import log

class GoogleCampaignService(object):
    """service in compaign level.
    
    Attributes:
        adwords_client: An class instance ,which you can use to link google api .
        account_id: will be used for db storeing,some mapping table need this column .
        campaign_service: google api service instance .
        ads_interface: dao level interface
    """ 
    
    STATUS_PAUSED = 0
    STATUS_ENABLE = 1
      
    def __init__(self,adwords_client,account_id):
        self.logger=log.Logger.get_logger()
        self.adwords_client=adwords_client
        self.campaign_service = self.adwords_client.GetService('CampaignService',version='v201409')
        self.account_id=account_id
        self.ads_interface=ads_interface.AdsInterface()
        self.ads_pre = ads_prepare_interface.AdsPrepareInterface()

    def create_campaigns(self,ucampaign_id,campaign_name, product, lead, camp_confs, budget_bid_info, creatives, keywords,lat_log_list, budget_key):
        """Create single campaign ,work flow as follows:
           1: create budget
           2: create campaign with the budget
           3: create adgroup belong to campaign
           4: add search keywords for adgroup level
           5: add image ad batch

        Keyword arguments:
        lead -- A object, include domain,target company name etc .
        campaign_parameters -- A object, include campaign init status, start date, end date .
        ad_info_list -- A list of object, inclue image name, display url, landing url, uri etc.
        budget -- A object, include budget type('STRANDARD','ACCELERATED'), budget amount(cent as unit) .
        bidding -- A object, include bidding type('CpaBid','CpcBid','CpmBid'), bid amount(cent as unit) .
        keyword_list -- A list of search key word .
        """
        domain=lead.domain
        self.logger.info('campaign name \'%s\' create start.' % campaign_name)
        try:
            #get lat_log
            gcc=campaign_criterion.GoogleCampaignCriterion(self.adwords_client)
            #lat_log_list = gcc.get_lat_log(domain)
            #if len(lat_log_list) == 0:
            #    self.logger.info('campaign name \'%s\' get geo location fail.' % campaign_name)
            #    return
            #create budget
            google_budget_service = budget_service.GoogleBudgetService(self.adwords_client)
            budget_bid_info['budget_name']='Budget#'+campaign_name+'#'+str(uuid.uuid4())
            budget_id = google_budget_service.create_budget(budget_bid_info)
            self.ads_interface.update_budget_id(budget_key,budget_id)
            #create blank campaign
            (campaign_id,campaign_key)=self.create_blank_campaign(campaign_name,camp_confs,budget_id)
            self.logger.info('campaign_id \'%s\', campaign_key:\'%s\',campaign_name: \'%s\'  create successly. '
                            % (str(campaign_id),str(campaign_key),campaign_name))
            
            #add lat_log criterion
            gcc=campaign_criterion.GoogleCampaignCriterion(self.adwords_client)
            gcc.add_campaign_criterion(campaign_id,lat_log_list)
            #add group
            gas=adgroup_service.GoogleGroupService(self.adwords_client)
            group_name=campaign_name
            (ad_group_id,ad_group_key)=gas.create_ad_group(campaign_id,campaign_key,group_name,budget_bid_info,ucampaign_id,lead.company_id)
            self.logger.info('adgroup_id \'%s\', group_key:\'%s\',group_name: \'%s\'  create successly. '
                            % (str(ad_group_id),str(ad_group_key),group_name))

            #add keywords
            gas.add_keywords([ad_group_id],keywords)
            #add image ads
            image_data_map={}
            for image_ad in creatives:
                with open(image_ad.image_url, 'r') as image_handle:
                    image_binary = base64.encodestring(image_handle.read())
                image_data_map[image_ad.image_url]=image_binary
            gads=ad_service.GoogleAdService(self.adwords_client)
            ad_id_key_list=gads.create_image_ad(ad_group_id,ad_group_key,creatives,image_data_map,group_name)
            self.logger.info('campaign name \'%s\' create end.' % campaign_name)
            return 1
        except Exception,e:
            self.logger.error(traceback.format_exc())
            self.logger.error(campaign_name+' created unsuccessly!')
            return 0

    def create_blank_campaign(self,campaign_name,camp_confs,budget_id):
        """Create the campaign,but without adgroup,without ad

        Keyword arguments:
        campaign_name --  defined by company_name,product_name,lead_name
        campaign_parameters -- A object, include campaign init status, start date, end date .
        budget_id -- returned by google api when you create budget
        """
        operations = [{
            'operator': 'ADD',
            'operand': {
                'name': campaign_name,
                'status': camp_confs.get('status'),
                'advertisingChannelType': camp_confs.get('ad_channel_type'),
                'biddingStrategyConfiguration': {
                    'biddingStrategyType': camp_confs.get('bidding_strategy_type'),
                },
                'budget': {
                    'budgetId': budget_id
                },
                'networkSetting': {
                    'targetGoogleSearch': 'false',
                    'targetSearchNetwork': 'false',
                    'targetContentNetwork': 'true',
                    'targetPartnerSearchNetwork': 'false'
                },
                'startDate': camp_confs.get('start_date'),
                'endDate': camp_confs.get('end_date'),
                'adServingOptimizationStatus': 'ROTATE',
                'frequencyCap': {
                    'impressions': '5',
                    'timeUnit': 'DAY',
                    'level': 'ADGROUP'
                },
                'settings': [
                    {
                        'xsi_type': 'GeoTargetTypeSetting',
                        'positiveGeoTargetType': 'DONT_CARE',
                        'negativeGeoTargetType': 'DONT_CARE'
                    }
                ]
            }
        }]
        campaigns = self.campaign_service.mutate(operations)
        campaign_id=campaigns['value'][0]['id']
        platform_id = self.ads_pre.get_platform_id('google')
        campaign_key=self.ads_interface.set_campaign(self.account_id,campaign_id,camp_confs.get('start_date'),\
                camp_confs.get('end_date'),platform_id, campaign_name, GoogleCampaignService.STATUS_PAUSED);
        return (campaign_id,campaign_key)
        
    def update_budget(self,campaign_id,campaign_name,budget):
        budget_service_obj = budget_service.GoogleBudgetService(self.adwords_client)
	budget_name=campaign_name+'#'+str(uuid.uuid4())
        budget_id = budget_service_obj.create_budget(budget)
        operation = [{
                'operator': 'SET',
                'operand': {
                    'id': campaign_id,
                    'budget': {
                        'budgetId':budget_id
                    }
                }
            }]
        self.campaign_service.mutate(operation)

    def update_status(self,campaign_id,status):
        campaign_status = ''
        if status == GoogleCampaignService.STATUS_PAUSED:
            campaign_status = 'PAUSED'
        elif status == GoogleCampaignService.STATUS_ENABLE:
            campaign_status = 'ENABLED'
        else:
            return False
        operation = [{
                'operator': 'SET',
                'operand': {
                    'id': campaign_id,
                    'status': campaign_status
                 }
            }]
        self.campaign_service.mutate(operation)
        return True
