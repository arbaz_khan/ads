__author__ = 'dianqiang'
from googleads import adwords
import urllib2
import json
import traceback
from utils import log

class GoogleCampaignCriterion(object):
    """Service in criterion level.
    Attributes:
        __domain2latlong_url: http service for get latitude longituede by domain
        ad_group_criterion_service: google api for criterion level service, is this project, we use it to define latitude-longitude
    """ 

    def __init__(self,adwords_client):
        self.logger=log.Logger.get_logger()
        self.__domain2latlong_url = 'http://54.89.207.30:9090/getdomain/d2l/%s' #url for get latitude longituede by domain
        self.adwords_client = adwords_client
        self.campaign_criterion_service = self.adwords_client.GetService('CampaignCriterionService', version='v201409')

    def add_campaign_criterion(self,campaign_id,lat_long_lst):
        """Get latitude longituede by domain
           Add latitude-longituede as a criterion for a comapaign
        """
        #lat_long_lst=self.__get_lat_log(domain)
        operations = []
        for lat_long in lat_long_lst:
            if len(lat_long)==3:
                operation = {
                    'operator': 'ADD',
                    'operand': {
                        'campaignId': campaign_id,
                        'criterion': {
                            'xsi_type': 'Proximity',
                            'geoPoint': {
                                'latitudeInMicroDegrees': int(float(lat_long.get('latitude')) * 1000000),
                                'longitudeInMicroDegrees': int(float(lat_long.get('longitude')) * 1000000)
                            },
                            'radiusDistanceUnits': 'KILOMETERS',
                            'radiusInUnits': 1.0
                        }
                    }
                }
                operations.append(operation)
        result = self.campaign_criterion_service.mutate(operations)
        for campaign_criterion in result['value']:
            self.logger.info('Campaign criterion with campaign id \'%s\', criterion id \'%s\' was added.'
                   % (campaign_criterion['campaignId'],campaign_criterion['criterion']['id']))


    def update_ad_schedule(self,campaign_ids,day_list):
        """Modify ad schedule
           Day level as 'MONDAY','TUESDAY'
           Hour level as '0','1'.....'23'
        """

        operations = []
        #day_list=['MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY']
        for day in day_list:
            for campaign_id in campaign_ids:
                timeofday = {
                    'xsi_type': 'AdSchedule',
                    'dayOfWeek': day,
                    'startHour': 0,
                    'startMinute': 'ZERO',
                    'endHour': 23,
                    'endMinute': 'FORTY_FIVE'
                }
                campaign_criterion = {
                    'campaignId': campaign_id,
                    'criterion': timeofday
                }
                operation={
                    'operator': 'ADD',
                    'operand': campaign_criterion
                }
                operations.append(operation)
            self.campaign_criterion_service.mutate(operations)
            operations=[]

    def get_lat_log(self,domain):
        """Get longitude and latitude list by domain
        """
        lat_long_lst = []
        response_json = None
        try:
            connection = urllib2.urlopen(self.__domain2latlong_url % domain)
            response_json = connection.read()
            connection.close()
            json_dict = {}
            json_dict = json.loads(response_json, encoding='utf-8')
            for geo_location in json_dict['data']:
                lat_long_lst.append(geo_location['location'])
        except:
            self.logger.error(traceback.format_exc())
        finally:
            return lat_long_lst

if __name__ == '__main__':
    yaml_file_name='/work/job/google_adword/ads/conf/googleads_test.yaml'
    adwords_client=adwords.AdWordsClient.LoadFromStorage(yaml_file_name)
    gcc=GoogleCampaignCriterion(adwords_client)
    #print gcc.get_lat_log('amazon.com')
    gcc.add_campaign_criterion('234572847','google.com')
