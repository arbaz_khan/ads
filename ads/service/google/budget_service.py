__author__ = 'dianqiang'
from googleads import adwords
import uuid
from utils import log

class GoogleBudgetService(object):
    """Service in budget level.    
    Attributes:
        adwords_client: An class instance ,which you can use to link google api .
        budget_service: google api for budget level service
    """ 

    def __init__(self,adwords_client):
        self.logger=log.Logger.get_logger()
        self.adwords_client=adwords_client
        self.budget_service = self.adwords_client.GetService('BudgetService', version='v201409')

    def create_budget(self,budget_bid_info):
        """create budget
        """
        budget_operation = {
            'name': budget_bid_info.get('budget_name'),
            'amount': {
                'microAmount': int(budget_bid_info.get('budget_amount')) * 10000
            },
            'deliveryMethod': budget_bid_info.get('budget_type'),
            'isExplicitlyShared':budget_bid_info.get('is_share'),
            'period': budget_bid_info.get('budget_period'),
        }

        budget_operations = [{
            'operator': 'ADD',
            'operand': budget_operation
        }]
        budget_res = self.budget_service.mutate(budget_operations)
        budget_id = budget_res['value'][0]['budgetId']

        self.logger.info('budget name \'%s\',budget amount \'%s\', budget id \'%s\' ,budget type \'%s\' was added'
                      % (budget_bid_info.get('budget_name'), budget_bid_info.get('budget_amount'),budget_id, budget_bid_info.get('budget_type')))
        return budget_id

    def update_budget(self, budget_id, budget):
        budget_operation = {
            'deliveryMethod': budget.get('budget_type'),
            'amount': {
                'microAmount': int(budget.get('budget_amount')) * 10000
            },
            'budgetId': budget_id
        }
        
        budget_operations = [{
            'operator': 'SET',
            'operand': budget_operation
        }]
        print budget_operations
        self.budget_service.mutate(budget_operations)
        self.logger.info('budget id \'%s\' was update,new budget is \'%s\'' \
                    % (budget_id, budget.get('budget_amount')))
