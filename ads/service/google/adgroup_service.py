__author__ = 'dianqiang'
from os import sys
from os import path
sys.path.insert(0, path.dirname(path.realpath(__file__)))
sys.path.insert(0, path.dirname(path.dirname(path.realpath(__file__))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.realpath(__file__)))))
from googleads import adwords
import adwords_client
from model import ads_interface
from model import ads_prepare_interface
from utils import log

class GoogleGroupService(object):
    """Service in group level.
    
    Attributes:
        adwords_client: An class instance ,which you can use to link google api .
        ad_group_service: google api for adgroup level service
        ad_group_criterion_service: google api for criterion level service, is this project, we use it to define latitude-longitude
        ads_interface: dao level interface
    """
 
    STATUS_ENABLE = 1
    PLATFORM = 'google'
    def __init__(self,adwords_client):
        self.logger=log.Logger.get_logger()
        self.adwords_client=adwords_client
        self.ad_group_service=self.adwords_client.GetService('AdGroupService', version='v201409')
        self.ad_group_criterion_service = self.adwords_client.GetService('AdGroupCriterionService', version='v201409')
        self.ads_interface=ads_interface.AdsInterface()
        self.ads_pre=ads_prepare_interface.AdsPrepareInterface()

    def create_ad_group(self,campaign_id,campaign_key,group_name,budget_bid_info,ucampaign_id,company_id):
        """Create a group based on campaign
        """
        if budget_bid_info['bid_type']=='CPC':
            bid_type = 'CpcBid'
        operations = [
            {
                'operator': 'ADD',
                'operand': {
                    'campaignId': campaign_id,
                    'name': group_name,
                    'biddingStrategyConfiguration': {
                        'bids': [
                            {
                                'xsi_type': bid_type,
                                'bid': {
                                    'microAmount': int(budget_bid_info.get('bid_amount')) * 10000
                                }
                            }
                        ]
                    },
                }
            }
        ]
        ad_groups = self.ad_group_service.mutate(operations)
        group_id=ad_groups['value'][0]['id']
        platform_id = self.ads_pre.get_platform_id(GoogleGroupService.PLATFORM)
        group_key=self.ads_interface.set_adgroup(ucampaign_id,campaign_key, group_id,str(group_name),company_id,platform_id,GoogleGroupService.STATUS_ENABLE)
        return (group_id,group_key)

    def update_bid(self, ad_group_ids, bid_info):
        """Update bid by ad group ids batch
        """
        bid_type = bid_info['bid_type']
        if bid_type == 'CPC':
            bid_type = 'CpcBid'
        operations=[]
        for ad_group_id in ad_group_ids:
            operation = {
                              'operator': 'SET',
                              'operand': {
                                  'id': ad_group_id,
                                  'biddingStrategyConfiguration': {
                                       'bids': [{
                                                   'xsi_type': bid_type,
                                                   'bid': {
                                                       'microAmount': bid_info.get('bid_amount') * 10000
                                                   }
                                               }]
                                  }
                              }
                          }
            operations.append(operation)
        ad_groups = self.ad_group_service.mutate(operations)
        for ad_group in ad_groups['value']:
            self.logger.info('Bid price of Ad group id \'%s\' with name \'%s\' was updated , type is \'%s, price is \'%s\'' \
                 % (ad_group['id'],ad_group['name'],bid_info.get('bid_type'), bid_info.get('bid_amount')))

    def update_status(self,ad_group_ids,status):
        operations = []
        for ad_group_id, ad_group_ad_id in ad_group_ids:
            operation = {
                'operator': 'SET',
                'operand': {
                    'adGroupId': ad_group_id,
                    'ad': {
                        'id': ad_group_ad_id
                    },
                    'status': status
                }
            }
            operations.append(operation)
        self.ad_group_ad_service.mutate(operations)

    def add_keywords(self, ad_group_ids, keyword_list):
        """Add keywords by ad group ids batch
        """

        for ad_group_id in ad_group_ids:
            operations = []
            for keyword in keyword_list:
                operation = {
                    'operator': 'ADD',
                    'operand': {
                        'xsi_type': 'BiddableAdGroupCriterion',
                        'adGroupId': ad_group_id,
                        'criterion': {
                            'xsi_type': 'Keyword',
                            'matchType': 'BROAD',
                            'text': keyword
                        }
                    }
                }
                operations.append(operation)
            ad_group_criterion = self.ad_group_criterion_service.mutate(operations)
            for criterion in ad_group_criterion['value']:
                log_info='Keyword \'%s\' of criterion id \'%s\'  for ad group \'%s\' was added' \
                      % (criterion['criterion']['text'], criterion['criterion']['id'], criterion['adGroupId'])
                self.logger.info(log_info)

if __name__ == '__main__':
    (client_id, client_secret, refresh_token)=('876778996900-6vh1kmbkvj8bt5c6l3cr0qu1nbfugqaj.apps.googleusercontent.com','q_qGTBQwIbaL6m2d2SZCFFOt','1/bqDHgsR2j5_itiBbaBTi_8YRXaRyes7tB9WI0tZIL-wMEudVrK5jSpoR30zcRFq6')
    (developer_token,user_agent, client_customer_id)=('GSSq7FyPE_KBBXIIPp8AYg','everstring.com:AdwordsTry:V0.1','443-052-9228')
    client_obj = adwords_client.GoogleAdwordClient(client_id,client_secret,refresh_token,developer_token,user_agent,client_customer_id)
    adwords_client=client_obj.get_client()
    gas=GoogleGroupService(adwords_client)
    gas.add_keywords(['236228967'],['test1','test2'])
