from facebookads.objects import AdAccount
from threading import Thread
import time
import datetime
from ads.model.insight_by_campaign import InsightByCampaign
from ads.model.insight_by_adgroup import InsightByAdgroup
from ads.model.insight_by_ad import InsightByAd
from ads.model.ads_insight_interface import AdsInsightInterface
from ads.model.ads_prepare_interface import AdsPrepareInterface
from ads.model.ads_interface import AdsInterface
from ads.utils.date_util import DateUtil

__author__ = 'boxia'


class FBInsightService(object):

    data_columns = [
        'reach',
        'frequency',
        'impressions',
        'unique_impressions',
        'cpm',
        'spend',
        'clicks',
        'unique_clicks',
        'ctr',
        'cpc',
        'actions',
    ]

    # our 'campaign' is called 'campaign_group' in facebook for insight
    campaign_columns = [
        'campaign_group_id',
        'campaign_group_name',
    ]

    # our 'adgroup' is called 'campaign' in facebook for insight
    adgroup_columns = [
        'campaign_id',
        'campaign_name',
    ]

    # our 'ad' is called 'adgroup' in facebook for insight
    ad_columns = [
        'adgroup_id',
        'adgroup_name',
    ]

    class InsightPullingThread(Thread):

        def __init__(self, platform_id, thread_id, thread_name,
                     account_id, fb_account, params):
            Thread.__init__(self)
            self.thread_id = thread_id
            self.thread_name = thread_name
            self.fb_account = fb_account
            self.account_id = account_id
            self.params = params
            self.platform_id = platform_id

        def run(self):
            print "%s: %s %s %s" % (self.thread_id, time.ctime(time.time()),
                                    self.thread_name, 'Starting')
            self.get_insight()
            print "%s: %s %s %s" % (self.thread_id, time.ctime(time.time()),
                                    self.thread_name, 'Ending')

        def get_insight(self):
            stats = self.fb_account.get_report_stats(params=self.params)
            insights = list()
            for item in stats:
                insight = None
                if self.thread_id == 'Thread-1':
                    insight = InsightByCampaign()
                    campaign = AdsInterface.get_campaign_by_platform_campaign(
                        self.platform_id, item['campaign_group_id'])

                    if campaign is None:
                        insight.campaign_id = item['campaign_group_id']
                        insight.campaign_name = item['campaign_group_name']
                        insight.platform_campaign_id = \
                            item['campaign_group_id']
                    else:
                        insight.campaign_id = campaign.id
                        insight.campaign_name = campaign.name
                        insight.platform_campaign_id = campaign.campagin_id
                elif self.thread_id == 'Thread-2':
                    insight = InsightByAdgroup()
                    adgroup = AdsInterface.get_adgroup_by_platform_adgroup(
                        self.platform_id, item['campaign_id'])

                    if adgroup is None:
                        insight.adgroup_id = item['campaign_id']
                        insight.adgroup_name = item['campaign_name']
                        insight.platform_adgroup_id = item['campaign_id']
                    else:
                        insight.adgroup_id = adgroup.id
                        insight.adgroup_name = adgroup.name
                        insight.platform_adgroup_id = adgroup.adgroup_id
                elif self.thread_id == 'Thread-3':
                    insight = InsightByAd()
                    ad = AdsInterface.get_ad_by_platform_ad(self.platform_id,
                                                            item['adgroup_id'])
                    if ad is None:
                        insight.ad_id = item['adgroup_id']
                        insight.ad_name = item['adgroup_name']
                        insight.platform_ad_id = item['adgroup_id']
                    else:
                        insight.ad_id = ad.id
                        insight.ad_name = ad.name
                        insight.platform_ad_id = ad.ad_id
                else:
                    continue

                insight.platform_id = self.platform_id
                insight.account_id = self.account_id
                insight.reach = item['reach']
                insight.frequency = item['frequency']
                insight.impressions = item['impressions']
                insight.unique_impressions = item['unique_impressions']
                insight.cpm = item['cpm']
                insight.spend = item['spend']
                insight.clicks = item['clicks']
                insight.unique_clicks = item['unique_clicks']
                insight.ctr = item['ctr']
                insight.cpc = item['cpc']
                insight.start_time = item['date_start']
                insight.end_time = item['date_stop']
                insight.retrieve_time = datetime.datetime.utcnow()

                insight.conversion = 0
                for action in item['actions']:
                    if action['action_type'] is not None and \
                            action['action_type'] == 'offsite_conversion':
                        insight.conversion = action['value']
                        break

                insights.append(insight)
            AdsInsightInterface.create_insights(insights)

    def __init__(self, facebook_ads_client):
        facebook_ads_client.connect()
        self.ads_pre = AdsPrepareInterface()
        self.platform_id = self.ads_pre.get_platform_id('facebook')

    # method for cron job. Should be called once a day
    # Note: Facebook use campaign_group, campaign, adgroup instead
    # of campaign, adgroup, ad
    def get_insight_cron(self, platform_account_id, query_field):
        fb_account = AdAccount(platform_account_id)
        account = self.ads_pre.get_account(self.platform_id,
                                           platform_account_id)

        # campaign level insight
        params_1 = {
            'data_columns': self.campaign_columns + self.data_columns,
            'actions_group_by': ['action_type', 'action_target_id'],
            'sort_by': 'spend',
            'sort_order': 'desc',
            'limit': int(query_field.get('limit', 100000)),
        }
        time_ranges = None
        if 'day_start' in query_field:
            day_start_str = query_field.get('day_start')
            day_start = DateUtil.validate(day_start_str)
            if 'day_stop' in query_field:
                day_stop_str = query_field.get('day_stop')
                day_stop = DateUtil.validate(day_stop_str)
            else:
                day_stop = datetime.datetime.utcnow()
            time_ranges = [{
                'day_start': {
                    'day': day_start.day,
                    'month': day_start.month,
                    'year': day_start.year,
                },
                'day_stop': {
                    'day': day_stop.day,
                    'month': day_stop.month,
                    'year': day_stop.year,
                },
            }]
        if time_ranges is not None:
            params_1['time_ranges'] = time_ranges
        else:
            params_1['date_preset'] = query_field.get('time', 'yesterday')

        thread_campaign = self.InsightPullingThread(self.platform_id,
                                                    'Thread-1',
                                                    'Campaign Insight',
                                                    account.id,
                                                    fb_account, params_1)

        # adgroup level insight
        params_2 = params_1.copy()
        params_2['data_columns'] = self.adgroup_columns + self.data_columns
        thread_adgroup = self.InsightPullingThread(self.platform_id,
                                                   'Thread-2',
                                                   'Adgroup  Insight',
                                                   account.id,
                                                   fb_account,
                                                   params_2)

        # ad level insight
        params_3 = params_2.copy()
        params_3['data_columns'] = self.ad_columns + self.data_columns
        thread_ad = self.InsightPullingThread(self.platform_id, 'Thread-3',
                                              'Ad       Insight', account.id,
                                              fb_account, params_3)

        thread_campaign.start()
        thread_adgroup.start()
        thread_ad.start()

        thread_campaign.join()
        thread_adgroup.join()
        thread_ad.join()

    def __validate(self, date_str):
        try:
            return datetime.datetime.strptime(date_str, '%Y-%m-%d')
        except ValueError:
            raise ValueError("Incorrect data format, should be YYYY-MM-DD")

    # method for getting insight on campaign level
    # query_field include 'start_date' e.g. '2015-01-01'
    # Note: Facebook use campaign_group, campaign, adgroup instead of campaign,
    # adgroup, ad
    def get_insight_by_campaign(self, account_id, campaign_id,
                                query_field=None):
        # get insights before today
        start_time = None
        if query_field is not None and 'start_date' in query_field:
            start_time = self.__validate(query_field['start_date'])
        insights_dict = AdsInsightInterface.get_insights_by_campaign(
            FBInsightService.platform_id, campaign_id, start_time)

        # get insights today
        account_id = str(account_id)
        if not account_id.startswith('act_'):
            account_id = 'act_' + account_id
        account = AdAccount(account_id)
        params = {
            'data_columns': self.campaign_columns + self.data_columns,
            'date_preset': 'today',
            'actions_group_by': ['action_type', 'action_target_id'],
            'sort_by': 'spend',
            'sort_order': 'desc',
            'limit': 100000,
        }
        stats = account.get_report_stats(params=params)
        for item in stats:
            if int(item['campaign_group_id']) == campaign_id:
                insight = dict()
                insight['campaign_id'] = campaign_id
                insight['reach'] = item['reach']
                insight['frequency'] = item['frequency']
                insight['impressions'] = item['impressions']
                insight['unique_impressions'] = item['unique_impressions']
                insight['cpm'] = item['cpm']
                insight['spend'] = item['spend']
                insight['clicks'] = item['clicks']
                insight['unique_clicks'] = item['unique_clicks']
                insight['ctr'] = item['ctr']
                insight['cpc'] = item['cpc']
                insight['start_time'] = item['date_start']
                insight['end_time'] = item['date_stop']
                insights_dict[item['date_start']] = insight

        return insights_dict

    # method for getting insight on adgroup level
    # query_field include 'start_date'
    # Note: Facebook use campaign_group, campaign, adgroup instead of campaign,
    # adgroup, ad
    def get_insight_by_adgroup(self, account_id, adgroup_id, query_field=None):
        # get insights before today
        start_time = None
        if query_field is not None and 'start_date' in query_field:
            start_time = self.__validate(query_field['start_date'])
        insights_dict = AdsInsightInterface.get_insights_by_adgroup(
            FBInsightService.platform_id, adgroup_id, start_time)

        # get insights today
        account_id = str(account_id)
        if not account_id.startswith('act_'):
            account_id = 'act_' + account_id
        account = AdAccount(account_id)
        params = {
            'data_columns': self.adgroup_columns + self.data_columns,
            'date_preset': 'today',
            'actions_group_by': ['action_type', 'action_target_id'],
            'sort_by': 'spend',
            'sort_order': 'desc',
            'limit': 100000,
        }
        stats = account.get_report_stats(params=params)
        for item in stats:
            if int(item['campaign_id']) == adgroup_id:
                insight = dict()
                insight['adgrop_id'] = adgroup_id
                insight['reach'] = item['reach']
                insight['frequency'] = item['frequency']
                insight['impressions'] = item['impressions']
                insight['unique_impressions'] = item['unique_impressions']
                insight['cpm'] = item['cpm']
                insight['spend'] = item['spend']
                insight['clicks'] = item['clicks']
                insight['unique_clicks'] = item['unique_clicks']
                insight['ctr'] = item['ctr']
                insight['cpc'] = item['cpc']
                insight['start_time'] = item['date_start']
                insight['end_time'] = item['date_stop']
                insights_dict[item['date_start']] = insight

        return insights_dict

    # method for getting insight on ad level
    # query_field include 'start_date'
    # Note: Facebook use campaign_group, campaign, adgroup instead of campaign,
    # adgroup, ad
    def get_insight_by_ad(self, account_id, ad_id, query_field=None):
        # get insights before today
        start_time = None
        if query_field is not None and 'start_date' in query_field:
            start_time = self.__validate(query_field['start_date'])
        insights_dict = AdsInsightInterface.get_insights_by_ad(
            FBInsightService.platform_id, ad_id, start_time)

        # get insights today
        account_id = str(account_id)
        if not account_id.startswith('act_'):
            account_id = 'act_' + account_id
        account = AdAccount(account_id)
        params = {
            'data_columns': self.ad_columns + self.data_columns,
            'date_preset': 'today',
            'actions_group_by': ['action_type', 'action_target_id'],
            'sort_by': 'spend',
            'sort_order': 'desc',
            'limit': 100000,
        }
        stats = account.get_report_stats(params=params)
        for item in stats:
            if int(item['adgroup_id']) == ad_id:
                insight = dict()
                insight['ad_id'] = ad_id
                insight['reach'] = item['reach']
                insight['frequency'] = item['frequency']
                insight['impressions'] = item['impressions']
                insight['unique_impressions'] = item['unique_impressions']
                insight['cpm'] = item['cpm']
                insight['spend'] = item['spend']
                insight['clicks'] = item['clicks']
                insight['unique_clicks'] = item['unique_clicks']
                insight['ctr'] = item['ctr']
                insight['cpc'] = item['cpc']
                insight['start_time'] = item['date_start']
                insight['end_time'] = item['date_stop']
                insights_dict[item['date_start']] = insight

        return insights_dict

    # account_id is platform account id
    # if day_start, day_stop is none, will get today's insight
    def get_spent(self, account_id, day_start=None, day_stop=None):
        data_columns = [
            'spend',
        ]
        # get insights today
        account_id = str(account_id)
        if not account_id.startswith('act_'):
            account_id = 'act_' + account_id
        account = AdAccount(account_id)
        params = {
            'data_columns': self.campaign_columns + data_columns,
            'date_preset': 'today',
            'actions_group_by': ['action_type', 'action_target_id'],
            'sort_by': 'spend',
            'sort_order': 'desc',
            'limit': 100000,
        }
        time_ranges = None
        if day_start is not None:
            day_start = DateUtil.validate(day_start)
            if day_stop is not None:
                day_stop = DateUtil.validate(day_stop)
            else:
                day_stop = datetime.datetime.utcnow()
            time_ranges = [{
                'day_start': {
                    'day': day_start.day,
                    'month': day_start.month,
                    'year': day_start.year,
                },
                'day_stop': {
                    'day': day_stop.day,
                    'month': day_stop.month,
                    'year': day_stop.year,
                },
            }]
        if time_ranges is not None:
            params['time_ranges'] = time_ranges
        else:
            params['date_preset'] = 'today'
        stats = account.get_report_stats(params=params)
        insights = []
        for item in stats:
            insights.append((item['campaign_group_id'], item['spend']))
        return insights
