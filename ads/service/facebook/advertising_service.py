from fb_ads_client import FBAdsClient
from fb_campaign_service import CampaignService
from fb_adset_service import AdSetService
from ads.service.common.config_service import ConfigService
from ads.model.ads_interface import AdsInterface
from ads.model.ads_prepare_interface import AdsPrepareInterface
from ads.utils.ads_exception import AdsExceptionConst
from ads.utils.ads_exception import AdsException
from ads.campaign_management.criteria_fb import FBCriteria
from ads.campaign_management.criteria_fb import FBCriteriaCommon

import traceback
from ads.utils.log import Logger
__author__ = 'boxia'


class AdvertisingService(object):
    PLATFORM_NAME = 'facebook'

    def create_campaigns_common(self, result_q, advertiser_id, ucampaign_id,
                                targeting_obj_list, leads_rating,
                                budget_bid_info=None):
        if len(targeting_obj_list) <= 0:
            return
        control_group = targeting_obj_list[0].lead.control_group
        try:
            self.create_campaigns(advertiser_id, ucampaign_id,
                                  targeting_obj_list, leads_rating,
                                  budget_bid_info)
        except Exception as e:
            err_code = -1
            err_msg = 'Error code {0} when creating {1} leads in \
                       Facebook. Here is ERROR message: {2}' \
                .format(err_code, leads_rating, e.message)
            result_q.put((err_code, err_msg))
            traceback.print_exc()
        else:
            if control_group == 0:
                result_q.put((
                    0, 'Create type {0} campaigns in Facebook Successfully!'.
                    format(leads_rating)))
            else:
                msg = 'Create type {0} campaigns in Facebook ' +\
                    'Successfully! Control group'
                result_q.put((0, msg.format(leads_rating)))

    def create_campaigns(self, advertiser_id, ucampaign_id,
                         targeting_obj_list, leads_rating,
                         budget_bid_info=None):
        if not advertiser_id or not ucampaign_id or \
                isinstance(targeting_obj_list, list) is False:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if len(targeting_obj_list) <= 0:
            Logger.get_logger().warning(
                " input param targeting_obj_list lenght is 0 ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        ads = AdsInterface()
        ads_pre = AdsPrepareInterface()

        company_ids = list()
        for targeting_obj in targeting_obj_list:
            lead = targeting_obj.lead
            company_ids.append(lead.company_id)
        if ads_pre.has_fb_company(company_ids) is False:
            return

        control_group = targeting_obj_list[0].lead.control_group
        fb_client = FBAdsClient.get_facebook_ads_client()

        advertiser = ads_pre.get_advertiser_by_id(advertiser_id)
        product = ads_pre.get_product_by_ucampaign(ucampaign_id)
        platform_id = ads_pre.get_platform_id(AdvertisingService.PLATFORM_NAME)

        # get account
        accounts = ads_pre.get_accounts(platform_id, product.id)
        if len(accounts) <= 0:
            Logger.get_logger().warning(" No ad account availabe ")
            raise AdsException(AdsExceptionConst.LOGIC_ERR)

        account_id = accounts[0].id
        platform_account_id = 'act_' + str(accounts[0].account_id)

        # create campaign in FB
        config_s = ConfigService()
        configs = config_s.get_campaign_configs(ucampaign_id, platform_id)
        objective = configs.get('objective', 'WEBSITE_CLICKS')
        campaign_s = CampaignService(fb_client)
        campaign_name = \
            advertiser.name + '#' + product.name + '#' + leads_rating
        if control_group == 1:
            campaign_name = campaign_name + '#Control'
        else:
            campaign_name = campaign_name + '#Test'
        campaign = campaign_s.create_campaign(platform_account_id,
                                              campaign_name, 0, objective)

        # create campaign in DB
        campaign_id = ads.set_campaign(account_id, campaign.get_id(),
                                       None, None, platform_id,
                                       campaign_name, 0)

        # create image on facebook
        creatives = ads_pre.get_creatives(ucampaign_id, platform_id)
        creative = creatives[0]
        adset_s = AdSetService(fb_client)
        image = adset_s.create_image(platform_account_id, creative.image_url)

        # create creative
        creative_fields = {}
        creative_fields['title'] = creative.headline
        creative_fields['body'] = creative.description_1
        creative_fields['object_url'] = creative.landing_page_url
        platform_creative = adset_s.create_creative(platform_account_id,
                                                    creative_fields, image)

        fb_criteria = FBCriteria()
        fb_criteria_common = FBCriteriaCommon()
        for targeting_obj in targeting_obj_list:
            lead = targeting_obj.lead
            fb_company = fb_criteria.get_work_employers(lead.company_id)

            if fb_company is None:
                continue

            fb_company_id = fb_company.fb_company_id
            fb_company_name = fb_company.fb_company_name
            company_id = fb_company.company_id

            fields = {}
            fields['fb_company_id'] = fb_company_id
            fields['fb_company_name'] = fb_company_name
            fields['grade'] = lead.rating
            fields['page_type'] = fb_criteria_common.get_placement()
            fields['bid_type'] = budget_bid_info['bid_type']
            fields['clicks'] = budget_bid_info['bid_amount']
            fields['daily_budget'] = budget_bid_info['budget_amount']

            # set adset/ad status as active,
            # campaign status paused on campaign level
            fields['adset_status'] = 1
            fields['ad_status'] = 1

            if 'pixel_id' in configs:
                fields['pixel_id'] = configs.get('pixel_id')
            fields['campaign_id'] = campaign.get_id()
            # Create adset on facebook
            if isinstance(fb_company_name, unicode):
                fb_company_name = fb_company_name.encode('utf-8')
            adgroup_name = \
                str(fb_company_name + '#' + lead.rating.encode('utf-8'))
            if lead.control_group == 1:
                adgroup_name = adgroup_name + '#Control'
            fields['adset_name'] = adgroup_name
            adset = adset_s.create_adset(platform_account_id, fields)

            if adset.get_id() is None:
                continue
            # Create adgroup in DB
            adgroup_id = ads.set_adgroup(ucampaign_id, campaign_id,
                                         adset.get_id(), adgroup_name,
                                         company_id, platform_id, 1)

            ad_name = adgroup_name + '#' + 'ad'
            if lead.control_group == 1:
                ad_name = ad_name + '#Control'
            # Create ad on facebook
            fields['ad_name'] = ad_name
            ad = adset_s.create_ad(platform_account_id, adset,
                                   fields, platform_creative)
            if ad.get_id() is None:
                continue
            # Create ad in DB
            ads.set_ad(adgroup_id, ad.get_id(), platform_id,
                       ad_name, 1, creative.id)

    def __update_adset(self, campaign_id, fields):
        fb_client = FBAdsClient.get_facebook_ads_client()
        adset_s = AdSetService(fb_client)
        adsets = adset_s.get_adsets_by_campaign(campaign_id)
        for adset in adsets:
            adset_s.update_adset(adset, fields)
            ads = adset_s.get_ads_by_adset(adset.get_id())
            for ad in ads:
                adset_s.update_ad(ad, fields)

    def update_budget_common(self, result_q, ucampaign_id, budget_info):
        try:
            self.update_budget(ucampaign_id, budget_info)
        except Exception as e:
            err_code = -1
            err_msg = 'Error code {0} when update budget {1} for Product ID {2}\
                       in Facebook Ads. Here is ERROR message: {2}' \
                .format(err_code, budget_info['budget_amount'],
                        ucampaign_id, e.message)
            result_q.put((err_code, err_msg))
            traceback.print_exc()
        else:
            msg = 'Update budget {0} for Product ID {1} in ' +\
                'Facebook Ads Successfully!'
            result_q.put((0, msg.format(
                budget_info['budget_amount'], ucampaign_id)))

    def update_budget(self, ucampaign_id, budget_info):
        if 'budget_amount' not in budget_info:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        ads = AdsInterface()
        ads_pre = AdsPrepareInterface()
        platform_id = ads_pre.get_platform_id('facebook')

        # campaigns = ads.get_campaigns_by_account(account_id)
        campaigns = ads.get_campaigns_by_ucampaign(ucampaign_id, platform_id)
        fields = {'daily_budget': budget_info['budget_amount']}
        for campaign in campaigns:
            self.__update_adset(campaign.campaign_id, fields)

        leads = ads_pre.get_all_leads_info(ucampaign_id)
        for lead in leads:
            ads.update_budget_bid(platform_id, lead.id, budget_info)

    def update_bidding_common(self, result_q, ucampaign_id, bid_info):
        try:
            self.update_bidding(ucampaign_id, bid_info)
        except Exception as e:
            err_code = -1
            err_msg = 'Error code {0} when update bid {1} for Product ID {2} in\
                       Facebook Ads. Here is ERROR message: {2}' \
                .format(err_code, bid_info['bid_amount'],
                        ucampaign_id, e.message)
            result_q.put((err_code, err_msg))
            traceback.print_exc()
        else:
            msg = 'Update bid {0} for Product ID {1} in ' +\
                'Facebook Ads Successfully!'
            result_q.put((0, msg.format(
                bid_info['bid_amount'], ucampaign_id)))

    def update_bidding(self, ucampaign_id, bid_info):
        if 'bid_type' not in bid_info or \
                'bid_amount' not in bid_info:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        ads = AdsInterface()
        ads_pre = AdsPrepareInterface()
        platform_id = ads_pre.get_platform_id('facebook')

        # campaigns = ads.get_campaigns_by_account(account_id)
        campaigns = ads.get_campaigns_by_ucampaign(ucampaign_id, platform_id)
        fields = {
            'clicks': bid_info['bid_amount'],
            'bid_type': bid_info['bid_type']}
        for campaign in campaigns:
            self.__update_adset(campaign.campaign_id, fields)

        leads = ads_pre.get_all_leads_info(ucampaign_id)
        for lead in leads:
            ads.update_budget_bid(platform_id, lead.id, bid_info)

    def update_status_common(self, result_q, ucampaign_id, status):
        try:
            self.update_status(ucampaign_id, status)
        except Exception as e:
            err_code = -1
            err_msg = 'Error code {0} when update status for Product ID {1} in\
                       Facebook Ads. Here is ERROR message: {2}' \
                .format(err_code, ucampaign_id, e.message)
            result_q.put((err_code, err_msg))
            traceback.print_exc()
        else:
            msg = 'Update status for Product ID {0} in ' + \
                'Facebook Ads Successfully!'
            result_q.put((0, msg.format(ucampaign_id)))

    def update_status(self, ucampaign_id, status):
        ads = AdsInterface()
        ads_pre = AdsPrepareInterface()
        platform_id = ads_pre.get_platform_id('facebook')

        # campaigns = ads.get_campaigns_by_account(account_id)
        campaigns = ads.get_campaigns_by_ucampaign(ucampaign_id, platform_id)

        if status != 0:
            status = 1
        fb_client = FBAdsClient.get_facebook_ads_client()
        campaign_s = CampaignService(fb_client)
        update_info = {'status': status}
        for campaign in campaigns:
            campaign_s.update_campaign(campaign.campaign_id, update_info)
            ads.update_campaign(campaign.id, None, status)
