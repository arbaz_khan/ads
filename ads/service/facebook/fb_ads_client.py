from facebookads.api import FacebookAdsApi
from ads.utils.log import Logger
import ConfigParser
import os

__author__ = 'boxia'


class FBAdsClient(object):
    @staticmethod
    def get_facebook_ads_client():
        path = os.path.dirname(os.path.realpath(__file__))
        fb_client_conf = path + '/../../conf/facebook_api_client.ini'
        # read facebook api client conf
        cf_client = ConfigParser.ConfigParser()
        cf_client.read(fb_client_conf)
        access_token = cf_client.get("token", "access_token")
        app_id = cf_client.get('app', 'app_id')
        app_secret = cf_client.get('app', 'app_secret')
        return FacebookAdsClient(app_id, app_secret, access_token)


class FacebookAdsClient(object):
    class __Singleton:
        def __init__(self, app_id, app_secret, access_token):
            FacebookAdsApi.init(app_id, app_secret, access_token)

    instance = None

    def __init__(self, app_id, app_secret, access_token):
        self.app_id = app_id
        self.app_secret = app_secret
        self.access_token = access_token

    def connect(self):
        if not FacebookAdsClient.instance:
            FacebookAdsClient.instance = FacebookAdsClient.__Singleton(
                self.app_id, self.app_secret, self.access_token)


if __name__ == '__main__':
    cf = ConfigParser.ConfigParser()
    cf.read('../../conf/ads_api.ini')
    access_token = cf.get("token", "access_token")
    app_id = cf.get("app", "app_id")
    app_secret = cf.get("app", "app_secret")

    client = FacebookAdsClient(app_id, app_secret, access_token)
    client.connect()
    Logger.get_logger().info(
        'FB ad - FacebookAdsClient with app_id \'%s\' connect end.' % app_id)
