__author__ = 'boxia'

from facebookads.objects import AdSet
from facebookads.objects import AdGroup
from facebookads.objects import AdImage
from facebookads.objects import AdCreative
from facebookads.objects import AdCampaign
from facebookads.specs import ObjectStorySpec, LinkData
from facebookads.exceptions import FacebookRequestError
from datetime import datetime
from ads.utils.log import Logger
import time
import traceback


class AdSetService(object):

    def __init__(self, facebook_ads_client):
        facebook_ads_client.connect()

    def create_adset(self, account_id, fields):
        adset = AdSet(parent_id=account_id)
        data = self.__set_adset_data(adset, fields)

        success = False
        response = None
        count = 0
        while not success:
            try:
                adset.remote_create(params=data)
                Logger.get_logger().info(
                    'FB ad - adset \'%s\' create end.' % fields['adset_name'])
                success = True
            except Exception as e:
                if count == 10:
                    break

                # handle facebook company id/name invalid case
                if isinstance(e, FacebookRequestError):
                    response = e.body()
                    error = response.get('error', None)
                    if error is not None:
                        error_user_title = error.get('error_user_title', None)
                        if error_user_title is not None:
                            if error_user_title == 'Invalid Targeting Spec':
                                break
                seconds = 70
                print e.message
                traceback.print_exc()
                msg = 'FB ad - adset API exception, sleep \'%s\' seconds.'
                Logger.get_logger().info(msg % seconds)
                time.sleep(seconds)
                count = count + 1

        return adset

    def create_ad(self, account_id, adset, fields, creative):
        if (not isinstance(adset, AdSet)) and (type(adset) is int):
            adset = AdSet(adset)

        status = fields.get('ad_status', 0)
        ad_status = AdGroup.Status.paused
        if int(status) == 1:
            ad_status = AdGroup.Status.active

        fb_company_name = fields.get('fb_company_name')
        level = fields.get('grade', None)

        adgroup = AdGroup(parent_id=account_id)
        adgroup[AdGroup.Field.name] = fields.get(
            'ad_name', self.__get_ad_name(fb_company_name, level))
        adgroup[AdGroup.Field.campaign_id] = adset.get_id()
        adgroup[AdGroup.Field.creative] = {'creative_id': creative.get_id()}
        adgroup[AdGroup.Field.status] = ad_status
        if 'pixel_id' in fields:
            adgroup[AdGroup.Field.tracking_specs] = [{
                'action.type': 'offsite_conversion',
                'offsite_pixel': int(fields['pixel_id'])}]

        success = False
        count = 0
        while not success:
            try:
                adgroup.remote_create()
                msg = 'FB ad - adgroup \'%s\' create end.'
                Logger.get_logger().info(msg % adgroup[AdGroup.Field.name])
            except:
                if count == 10:
                    break

                seconds = 70
                msg = 'FB ad - ad API exception, sleep \'%s\' seconds.'
                Logger.get_logger().info(msg % seconds)
                time.sleep(seconds)
                count = count + 1
            else:
                success = True

        return adgroup

    def __set_adset_data(self, adset, fields):
        if adset is None or fields is None:
            return

        fb_company_id = fields.get('fb_company_id')
        fb_company_name = fields.get('fb_company_name')
        level = fields.get('grade', None)
        status = fields.get('adset_status', 0)
        page_type = fields.get('page_type', 'rightcolumn')
        daily_budget = fields.get('daily_budget', 0)
        campaign_id = fields.get('campaign_id', None)
        if campaign_id is None:
            return
        # adset_name = self.__get_adset_name(fb_company_name, level)
        adset_name = fields.get('adset_name',
                                self.__get_adset_name(fb_company_name, level))
        adset_status = AdSet.Status.paused
        if int(status) == 1:
            adset_status = AdSet.Status.active
        bid_type = fields.get('bid_type', 'CPC')
        bid_info = None
        if bid_type == 'CPC':
            bid_info = {'CLICKS': int(fields.get('clicks', 0))}
        elif bid_type == 'CPM':
            bid_info = {'IMPRESSIONS': int(fields.get('impressions', 0))}
        elif bid_type == 'CPA':
            bid_info = {'ACTIONS': int(fields.get('actions', 0))}
        elif bid_type == 'ABSOLUTE_OCPM':
            bid_info = {
                'ACTIONS': int(fields.get('actions', 0)),
                'REACH': int(fields.get('reach', 0)),
                'CLICKS': int(fields.get('clicks', 0)),
                'SOCIAL': int(fields.get('social', 0))}
        else:
            return

        data = {
            AdSet.Field.name: adset_name,
            AdSet.Field.bid_type: bid_type,
            AdSet.Field.bid_info: bid_info,
            AdSet.Field.status: adset_status,
            AdSet.Field.daily_budget: daily_budget,
            AdSet.Field.campaign_group_id: campaign_id,
            AdSet.Field.start_time: datetime.utcnow().isoformat(),
            AdSet.Field.end_time: 0,
            AdSet.Field.targeting: {
                'geo_locations': {
                    'countries': ['US', ]
                },
                'work_employers': [
                    {
                        'id': fb_company_id,
                        'name': fb_company_name,
                    }
                ],
                'page_types': [page_type]
            }
        }

        Logger.get_logger().info('adset_data used \'%s\'' % data)
        return data

    def __get_adset_name(self, fb_company_name, level):
        if level is not None:
            return fb_company_name + '_' + level
        return fb_company_name

    def __get_ad_name(self, fb_company_name, level):
        if level is not None:
            return fb_company_name + '_' + level + '_ad'
        return fb_company_name

    def create_image(self, account_id, file_path):
        image = AdImage(parent_id=account_id)
        image[AdImage.Field.filename] = file_path
        image.remote_create()
        msg = 'FB ad - create_image with file \'%s\' '
        Logger.get_logger().info(msg % file_path)
        return image

    def create_creative(self, account_id, fields, image):
        creative = AdCreative(parent_id=account_id)
        creative[AdCreative.Field.title] = fields['title']
        creative[AdCreative.Field.body] = fields['body']
        creative[AdCreative.Field.object_url] = fields['object_url']
        creative[AdCreative.Field.image_hash] = image.get_hash()
        creative.remote_create()
        msg = 'FB ad - creative \'%s\' create end.'
        Logger.get_logger().info(msg % fields['title'])
        return creative

    # generate creative which could be used as news feed placement
    def create_creative2(self, account_id, fields, image):
        link_data = LinkData()
        link_data[LinkData.Field.name] = fields['link_name']
        link_data[LinkData.Field.description] = fields['description']
        link_data[LinkData.Field.message] = fields['message']
        link_data[LinkData.Field.link] = fields['link']
        link_data[LinkData.Field.image_hash] = image.get_hash()

        object_story_spec = ObjectStorySpec()
        object_story_spec[ObjectStorySpec.Field.page_id] = fields['page_id']
        object_story_spec[ObjectStorySpec.Field.link_data] = link_data

        creative = AdCreative(parent_id=account_id)
        creative[AdCreative.Field.name] = fields['creative_name']
        creative[AdCreative.Field.object_story_spec] = object_story_spec
        creative.remote_create()
        return creative

    def update_adset(self, adset, fields):
        if (not isinstance(adset, AdSet)) and (type(adset) is int):
            adset = AdSet(adset)

        if 'adset_name' in fields:
            adset[AdSet.Field.name] = fields['adset_name']

        if 'adset_status' in fields:
            adset_status = AdSet.Status.paused
            if int(fields['adset_status']) == 1:
                adset_status = AdSet.Status.active
            adset[AdSet.Field.status] = adset_status

        if 'daily_budget' in fields:
            adset[AdSet.Field.daily_budget] = int(fields.get('daily_budget'))

        if 'bid_type' in fields:
            bid_type = fields.get('bid_type')
            bid_info = None
            if bid_type == 'CPC' and 'clicks' in fields:
                bid_info = {'CLICKS': int(fields.get('clicks'))}
            elif bid_type == 'CPM' and 'impressions' in fields:
                bid_info = {'IMPRESSIONS': int(fields.get('impressions'))}
            elif bid_type == 'CPA' and 'actions' in fields:
                bid_info = {'ACTIONS': int(fields.get('actions'))}
            elif bid_type == 'ABSOLUTE_OCPM':
                data = dict()
                if 'action' in fields:
                    data['ACTIONS'] = int(fields.get('actions'))
                if 'reach' in fields:
                    data['REACH'] = int(fields.get('reach'))
                if 'clicks' in fields:
                    data['CLICKS'] = int(fields.get('clicks'))
                if 'social' in fields:
                    data['ACTIONS'] = int(fields.get('social'))
                bid_info = data
            if bid_info is not None:
                adset[AdSet.Field.bid_type] = bid_type
                adset[AdSet.Field.bid_info] = bid_info

        adset.remote_update()

    def update_ad(self, ad, fields):
        if (not isinstance(ad, AdGroup)) and (type(ad) is int):
            ad = AdGroup(ad)

        if 'ad_name' in fields:
            ad[AdGroup.Field.name] = fields.get('ad_name')
        if 'ad_status' in fields:
            ad_status = AdGroup.Status.paused
            if int(fields.get('ad_status')) == 1:
                ad_status = AdGroup.Status.active
            ad[AdGroup.Field.status] = ad_status

        ad.remote_update()

    def get_adsets_by_campaign(self, campaign_id):
        adcampaign = AdCampaign(campaign_id)
        adsets = adcampaign.get_ad_sets([
            AdSet.Field.id,
            AdSet.Field.name,
            AdSet.Field.status,
            AdSet.Field.daily_budget,
            AdSet.Field.lifetime_budget,
            AdSet.Field.targeting,
            AdSet.Field.start_time,
            AdSet.Field.end_time,
        ])
        return adsets

    def get_ads_by_adset(self, adset_id):
        adset = AdSet(adset_id)
        return adset.get_ad_groups()
