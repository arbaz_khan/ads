__author__ = 'boxia'

from facebookads.objects import AdCampaign
from ads.utils.log import Logger

class CampaignService(object):
    def __init__(self, facebook_ads_client):
        facebook_ads_client.connect()

    def create_campaign(self, account_id, campaign_name, status, objective):
        campaign_status = AdCampaign.Status.paused
        if status == 1:
            campaign_status = AdCampaign.Status.active

        campaign = AdCampaign(parent_id = account_id)
        campaign[AdCampaign.Field.name] = campaign_name
        campaign[AdCampaign.Field.status] = campaign_status
        campaign[AdCampaign.Field.objective] = objective
        campaign.remote_create()
        Logger.get_logger().info('FB ad - campaign name \'%s\' create end.' % campaign_name)
        return campaign

    def update_campaign(self, campaign_id, update_dict):
        if update_dict == None:
            return

        campaign = AdCampaign(campaign_id)
        
        if update_dict.get('name') != None:
            campaign[AdCampaign.Field.name] = update_dict['name']

        if update_dict.get('objective') != None:
            campaign[AdCampaign.Field.objective] = update_dict['objective']

        if update_dict.get('status') != None:
            status = update_dict['status']
            if status == 1:
                campaign[AdCampaign.Field.status] = AdCampaign.Status.active
            elif status == 0:
                campaign[AdCampaign.Field.status] = AdCampaign.Status.paused
        campaign.remote_update()


