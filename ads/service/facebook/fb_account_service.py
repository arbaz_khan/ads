__author__ = 'boxia'

from facebookads.objects import AdAccount
from facebookads.objects import AdUser

class AccountService(object):
    def __init__(self, facebook_ads_client):
        facebook_ads_client.connect()

    def get_account_by_id(self, account_id):
        account = AdAccount(account_id)
        objects = account.remote_read(fields=[
            AdAccount.Field.name,
            AdAccount.Field.account_groups,
            AdAccount.Field.account_status,
            AdAccount.Field.balance,
            AdAccount.Field.amount_spent,
            AdAccount.Field.daily_spend_limit,
            AdAccount.Field.spend_cap])
        return account

    def get_user_accounts(self):
        me = AdUser('me')
        accounts = me.get_ad_accounts()
        return accounts



