import csv
import urllib2
import json
import os
import ConfigParser
import traceback
import tldextract
import time

from ads.model.ads_prepare_interface import AdsPrepareInterface
from ads.model.ads_interface import AdsInterface
from ads.utils import log
from ads.utils.ads_exception import AdsExceptionConst
from ads.utils.ads_exception import AdsException
from ads.utils.api_call import ApiCall

__author__ = 'dianqiang, boxia'


class TargetingService(object):
    def __init__(self):
        self.__domain2latlong_url = 'http://54.89.207.30:9090/getdomain/d2l/%s'
        self.ads_prepare = AdsPrepareInterface()
        self.ads = AdsInterface()
        self.logger = log.Logger.get_logger()

    # modified by boxia
    def import_leads(self, ucampaign_id, csv_file):
        real_path = os.path.dirname(os.path.realpath(__file__))
        fb_client_conf = real_path + '/../../conf/facebook_api_client.ini'
        cf_client = ConfigParser.ConfigParser()
        cf_client.read(fb_client_conf)
        access_token = cf_client.get("token", "access_token")

        lead_ids = list()
        with open(csv_file, mode='rb') as f:
            reader = csv.DictReader(f)
            for row in reader:
                domain = row['Company Website']
                company_name = row['Company Name']
                grade = row.get('Company Rating', '')
                industry = row.get('Industry', '')
                employee_size = row.get('Employee Size', '')
                fit_score = row.get('Score', -1)
                engagement_score = row.get('Engagement Score', -1)
                intent_score = row.get('Intent Score', -1)
                control_type = row.get('Type', '')
                # This is more depended on CSV format
                if control_type == 'control group':
                    control_group = 1
                else:
                    control_group = 0

                domain = self.__extract_domain(domain)

                fb_company_id = None
                fb_company_name = None
                if not self.ads_prepare.company_exists(
                        company_name, domain, industry, employee_size):
                    # Try to solve facebook api limitation
                    # Facebook API call limitation 600 calls per 600 seconds
                    time.sleep(1.1)

                    fb_company_lst = ApiCall.get_fb_company_list(access_token,
                                                                 company_name)
                    # fb_company_id, fb_company_name = \
                    #        self.__extract_company_with_largest_coverage(fb_company_lst)
                    fb_company_id, fb_company_name = \
                        self.__extract_fb_company(fb_company_lst, domain)

                # create company
                company = self.ads_prepare.set_company(company_name, domain,
                                                       industry, employee_size)
                # create fb_company
                if fb_company_id is not None:
                    self.ads_prepare.set_fb_company(company.id,
                                                    fb_company_id,
                                                    fb_company_name)

                # create company address,add by dianqiang
                self.__add_address(company.id, domain)
                target_company = {
                    'company_id': company.id,
                    'score': fit_score,
                    'engagement_score': engagement_score,
                    'intent_score': intent_score,
                    'grade': grade,
                    'control_group': control_group,
                }
                lead_id = self.ads_prepare.set_company_score_map(
                    ucampaign_id, target_company)
                lead_ids.append(lead_id)

        return lead_ids

    def __extract_domain(self, input_domain):
        tld_extract = tldextract.extract(input_domain)
        try:
            input_domain = ".".join([tld_extract.domain, tld_extract.tld])
        except:
            input_domain = ".".join([tld_extract.domain, tld_extract.suffix])
        return input_domain

    def __extract_fb_company(self, company_arr, input_domain):
        fb_company = None
        fb_company_id = None
        fb_company_name = None
        coverage = 0
        for company in company_arr:
            domain = ApiCall.company2domain(company['name'])
            if isinstance(domain, unicode):
                domain = domain.encode('utf-8')
            if input_domain == domain:
                if int(company['coverage']) > coverage:
                    fb_company = company
                    coverage = int(company['coverage'])

        if fb_company is not None:
            fb_company_id = int(fb_company['id'])
            if isinstance(fb_company['name'], unicode):
                fb_company_name = fb_company['name'].encode('utf-8')
            else:
                fb_company_name = fb_company['name']

        return fb_company_id, fb_company_name

    # added by boxia
    def __extract_company_with_largest_coverage(self, company_arr):
        coverage = 0
        fb_company = None
        for company in company_arr:
            if int(company['coverage']) > coverage:
                fb_company = company
                coverage = int(company['coverage'])
        fb_company_id = None
        fb_company_name = None
        if fb_company is not None:
            fb_company_id = int(fb_company['id'])

            # add by ChengLei
            if isinstance(fb_company['name'], unicode):
                fb_company_name = fb_company['name'].encode('utf-8')
                # fb_company_name = quote(fb_company_name)
            else:
                fb_company_name = fb_company['name']

        return fb_company_id, fb_company_name

    # add company address
    def __add_address(self, company_id, domain):
        lat_log_list = self.__get_lat_log(domain)
        for lat_long in lat_log_list:
            self.ads_prepare.set_company_address(company_id, 'address',
                                                 lat_long[1], lat_long[0])

    # get lat_log list by domain, added by dianqiang
    def __get_lat_log(self, domain):
        """Get longitude and latitude list by domain
        """
        lat_long_lst = []
        response_json = None
        try:
            connection = urllib2.urlopen(self.__domain2latlong_url % domain)
            response_json = connection.read()
            connection.close()
            json_dict = {}
            json_dict = json.loads(response_json, encoding='utf-8')
            for geo_location in json_dict['data']:
                lat_long_lst.append(geo_location['location'])
        except:
            self.logger.error(traceback.format_exc())
        finally:
            return lat_long_lst

    # TODO
    def get_company_ip(self, domain):
        ip_start = 0
        ip_end = 0
        return ip_start, ip_end

    # TODO
    def get_company_address(self, domain):
        pass

    def get_fb_company_id_name(self, domain):
        # get facebook company id and name
        return 0, 'Dummy'

    def get_leads(self, ucampaign_id, grade='', control_group=0):
        if not ucampaign_id:
            log.Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        # get leads info from db
        return self.ads_prepare.get_leads_info(ucampaign_id, grade,
                                               control_group)

    def set_budget_bid_batch(self, platform_id, lead_ids, budget_bid_info):
        if not platform_id or isinstance(lead_ids, list) is False or \
                isinstance(budget_bid_info, dict) is False:
            log.Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if 'budget_type' not in budget_bid_info or \
           'budget_amount' not in budget_bid_info or \
           'bid_type' not in budget_bid_info or \
           'bid_amount' not in budget_bid_info:

            log.Logger.get_logger.warning(" input param missing")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        self.ads.set_budget_bid_batch(platform_id, lead_ids, budget_bid_info)

    # set budget/bid, make sure budget can divide bid
    def set_budget_bid(self, platform_id, company_score_map_id,
                       budget_bid_info):
        if not platform_id or not company_score_map_id or \
                isinstance(budget_bid_info, dict) is False:
            log.Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if 'budget_type' not in budget_bid_info or \
           'budget_amount' not in budget_bid_info or \
           'bid_type' not in budget_bid_info or \
           'bid_amount' not in budget_bid_info:

            log.Logger.get_logger.warning(" input param missing")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        budget_amount = budget_bid_info.get('budget_amount')
        bid_amount = budget_bid_info.get('bid_amount')
        if budget_amount < bid_amount or budget_amount < bid_amount * 2:
            log.Logger.get_logger.warning(" budget, bid doesn't match")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        self.ads.set_budget_bid(platform_id, company_score_map_id,
                                budget_bid_info)

    # set budget/bid, make sure budget can divide bid
    def update_budget_bid(self, platform_id, company_score_map_id,
                          budget_bid_info):
        if not platform_id or not company_score_map_id or \
                isinstance(budget_bid_info, dict) is False:

            log.Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        if ('budget_amount' in budget_bid_info and (
            'bid_amount' not in budget_bid_info.has_key)) \
            or ('budget_amount' not in budget_bid_info and
                'bid_amount' in budget_bid_info):

            log.Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        budget_amount = budget_bid_info.get('budget_amount')
        bid_amount = budget_bid_info.get('bid_amount')
        if budget_amount < bid_amount or budget_amount < 2 * bid_amount:
            log.Logger.get_logger.warning(" budget, bid doesn't match")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        self.ads.update_budget_bid(platform_id, company_score_map_id,
                                   budget_bid_info)

    def get_budget(self, platform_id, company_score_map_id):
        return self.ads.get_budget(platform_id, company_score_map_id)

    def get_bid(self, platform_id, company_score_map_id):
        return self.ads.get_bid(platform_id, company_score_map_id)

    def set_keywords(self, ucampaign_id, keywords_list):
        return self.ads_prepare.set_keywords(ucampaign_id, keywords_list)

    def get_keywords(self, ucampaign_id):
        return self.ads_prepare.get_keywords(ucampaign_id)

    def get_facebook_company(self, company_id):
        return self.ads_prepare.get_facebook_company(company_id)
