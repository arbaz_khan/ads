__author__ = 'boxia'

from ads.model.ads_prepare_interface import AdsPrepareInterface


class PlatformService(object):

    def __init__(self):
        self.ads_prepare = AdsPrepareInterface()

    def get_platform_id(self, platform_name):
        return self.ads_prepare.get_platform_id(platform_name)
