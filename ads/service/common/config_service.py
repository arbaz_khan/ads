__author__ = 'boxia'

from ads.model.ads_interface import AdsInterface


class ConfigService(object):

    def __init__(self):
        self.ads = AdsInterface()

    def set_campaign_config(self, ucampaign_id, platform_id, key, value):
        self.ads.set_campaign_config(ucampaign_id, platform_id, key, value)

    def get_campaign_configs(self, ucampaign_id, platform_id):
        return self.ads.get_campaign_configs(ucampaign_id, platform_id)
