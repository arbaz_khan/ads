__author__ = 'boxia'

from ads.model.ads_prepare_interface import AdsPrepareInterface


class AccountService(object):

    def __init__(self):
        self.ads_prepare = AdsPrepareInterface()

    def set_account(self, platform_id, platform_account_id,\
            platform_account_name, product_id):

        account = self.ads_prepare.set_account(platform_id, \
                platform_account_id, platform_account_name, product_id)
        return account

    def get_accounts(self, platform_id, product_id):
        accounts = self.ads_prepare.get_accounts(platform_id, product_id)
        return accounts
