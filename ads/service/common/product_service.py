__author__ = 'dianqiang'

from ads.model import ads_prepare_interface


class ProductService(object):
    def __init__(self):
        self.ads_prepare_intf = ads_prepare_interface.AdsPrepareInterface()
        pass

    def set_product(self, advertiser_id, product_name):
        product = self.ads_prepare_intf.set_product(advertiser_id, product_name)
        return product

    def get_product(self, product_id):
        product = self.ads_prepare_intf.get_product_by_id(product_id)
        return product

if __name__ == '__main__':
    #call product service
    pass
