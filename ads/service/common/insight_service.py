# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

from ads.model.ads_insight_interface import AdsInsightInterface
from ads.model.ads_interface import AdsInterface
from ads.model.ads_prepare_interface import AdsPrepareInterface
from ads.service.facebook.fb_insight_service import FBInsightService
from ads.service.facebook.fb_ads_client import FBAdsClient
from ads.service.google.adwords_client import GoogleAdwordClient
from ads.service.google.insight_service import GoogleInsightService
import datetime


# TODO: Temp
BUDGT = 100000


class InsightService(object):

    @staticmethod
    def get_united_campaign_list(search_key=None):
        data = {}
        data['count'] = 0
        data['list'] = []
        try:
            campaign_list = AdsInterface.get_united_campaign_list(search_key)
            data['list'] = campaign_list
            data['count'] = len(campaign_list)

            return data
        except:
            raise

    @staticmethod
    def get_campaign_list():
        data = {}
        data['count'] = 0
        data['list'] = []
        try:
            advertiser_list = AdsInterface.get_advertiser_info()
            product_list = []

            for a_dic in advertiser_list:
                a_id = a_dic.id
                a_name = a_dic.name

                p_list = \
                    AdsInterface.get_product_list_by_advertiser_id(a_id)

                for p_dic in p_list:
                    p_name = p_dic.name
                    p_id = p_dic.id

                    tmp_dic = {}
                    tmp_dic['name'] = "%s_%s" % (a_name, p_name)
                    tmp_dic['id'] = p_id

                    product_list.append(tmp_dic)

            data['list'] = product_list
            data['count'] = len(product_list)

            return data
        except:
            raise

    @staticmethod
    def get_united_campaign_name(camp_id):
        try:
            ret = AdsInsightInterface.get_united_campaign_name_by_id(camp_id)
            return ret
        except:
            raise

    @staticmethod
    def get_es_company_score_map(company_id, campaign_id):
        try:
            ret = AdsInsightInterface.get_es_company_score_map(
                company_id, campaign_id)
            return ret
        except:
            raise

    @staticmethod
    def get_company_data_by_name(company_name):
        try:
            ret = AdsInsightInterface.get_company_data_by_name(company_name)
            return ret
        except:
            raise

    @staticmethod
    def get_company_data_by_id(company_id):
        try:
            ret = AdsInsightInterface.get_company_data_by_id(company_id)
            return ret
        except:
            raise

    @staticmethod
    def get_campaign_name_by_id(camp_id):
        try:
            return AdsInsightInterface. \
                get_united_campaign_name_by_id(camp_id)
        except:
            raise

    @staticmethod
    def get_company_insight(camp_id, cur_page, page_size, sort_key,
                            sort_dir, search_key, start_time, end_time):
        ret = AdsInsightInterface.\
            get_united_company_insight(camp_id, cur_page,
                                       page_size, sort_key,
                                       sort_dir, search_key,
                                       start_time, end_time)

        # TODO: remove these lines when we figure out how to return engagement
        # scores
        for row in ret.get('list', []):
            row['engage'] = 90

        return ret

    @staticmethod
    def get_campaign_info(product_id, start_time, end_time):
        data = {}
        try:
            campaign_info_map = {}
            platform_campaign_map = {}
            campaign_info_map, platform_campaign_map = \
                InsightService.get_campaign_info_by_product_id(
                    product_id, start_time, end_time)

            platform_insight_info = {}  # {platform_id : compaign_info_list}
            for platform_id, campaign_id_list in platform_campaign_map.items():
                tmp_info = []
                for campaign_id in campaign_id_list:
                    insight_info = \
                        AdsInsightInterface. \
                        get_insights_by_campaign(platform_id,
                                                 campaign_info_map[campaign_id]
                                                 ['platform_campaign_id'],
                                                 campaign_info_map[campaign_id]
                                                 ['start_time'])

                    for t, insight in insight_info.items():
                        tmp_info.append(insight)

                platform_insight_info[platform_id] = tmp_info

            spend = 0
            remaining = BUDGT
            cpc = 0.0  # Need to confirm
            cac = 0.0  # Need to confirm
            ave_conv = 0.0
            for p_id, c_insight_list in platform_insight_info.items():
                for item in c_insight_list:
                    spend = spend + item['spend']
                    remaining = remaining - item['spend']

            data['spend'] = spend
            data['remaining'] = remaining
            data['cpc'] = cpc
            data['cac'] = cac
            data['convRate'] = ave_conv

            return data
        except:
            raise

    @staticmethod
    def get_campaign_spend_data(campaign_id, start_time, end_time):
        cac = 0.0

        spend = AdsInsightInterface.\
            get_campaign_spend(campaign_id, start_time, end_time)
        remaining = AdsInsightInterface.\
            get_campaign_remaining(campaign_id, spend)
        cpc = AdsInsightInterface.\
            get_campaign_cpc(campaign_id, spend, start_time, end_time)

        conversions = AdsInsightInterface.\
            get_people_converted_count(campaign_id, start_time, end_time)
        clicks = AdsInsightInterface.\
            get_people_clicks_number(campaign_id, start_time, end_time)
        conv_rate = conversions * 1.0 / clicks

        data = {}
        data['spend'] = spend
        data['remaining'] = remaining
        data['cpc'] = cpc
        data['cac'] = cac
        data['convRate'] = conv_rate

        return data

    @staticmethod
    def get_insight_data_by_united_campaign_id(campaign_id,
                                               start_time, end_time):
        return AdsInsightInterface.\
            get_insight_data_by_united_campaign_id(campaign_id,
                                                   start_time, end_time)

    @staticmethod
    def get_campaign_funnel_data(campaign_id, start_time, end_time):
        data = {}
        try:
            targetComp = 0  # How many companies we want to advertise

            compFound = 0  # How many companies have impressions
            peopleImp = 0  # How many impressions the campaign have

            compInterested = 0  # How many companies have clicks
            peopleClicks = 0  # How many clicks the campaign have

            peopleEngaged = 0
            compEngaged = 0

            compSold = 0  # count(company w/ conversions > 0)
            peopleConv = 0  # sum(conversions)

            targetComp = \
                AdsInsightInterface.get_target_company_number(campaign_id)
            if targetComp is None:
                targetComp = 0

            compFound = \
                AdsInsightInterface.get_company_found_number(campaign_id)
            if compFound is None:
                compFound = 0

            peopleImp = AdsInsightInterface.\
                get_people_impression_number(campaign_id,
                                             start_time, end_time)
            if peopleImp is None:
                peopleImp = 0

            compInterested = AdsInsightInterface.\
                get_company_interested_number(campaign_id,
                                              start_time, end_time)
            if compInterested is None:
                compInterested = 0

            peopleClicks = AdsInsightInterface.\
                get_people_clicks_number(campaign_id, start_time, end_time)
            if peopleClicks is None:
                peopleClicks = 0

            campaign_configs = AdsInterface().\
                get_campaign_configs(campaign_id, 0)

            # TODO: use real data source when it's ready
            RANDOM_FACTOR_FOR_DEMO = 2.2
            peopleEngaged = int(int(peopleClicks) / RANDOM_FACTOR_FOR_DEMO)
            compEngaged = int(int(compInterested) / RANDOM_FACTOR_FOR_DEMO)

            compSold = AdsInsightInterface.\
                get_company_converted_count(campaign_id, start_time, end_time)
            peopleConv = AdsInsightInterface.\
                get_people_converted_count(campaign_id, start_time, end_time)

            data['targetComp'] = targetComp
            data['compFound'] = compFound
            data['peopleImp'] = peopleImp
            data['compInterested'] = compInterested
            data['peopleClicks'] = peopleClicks
            data['peopleEngaged'] = peopleEngaged
            data['compEngaged'] = compEngaged
            data['compSold'] = compSold
            data['peopleConv'] = peopleConv
            if 'show_conversion' in campaign_configs:
                data['showConversion'] = campaign_configs['show_conversion']
            if 'show_engagement' in campaign_configs:
                data['showEngagement'] = campaign_configs['show_engagement']
            return data
        except:
            raise

    @staticmethod
    def get_campaign_info_by_product_id(product_id, start_time, end_time):
        if product_id is None:
            return None

        c_id_list = \
            AdsInterface.get_campaign_ids_by_product_id(product_id)

        campaign_account_map = {}  # {account_id: campaign_id_list}
        campaign_info_map = {}  # {id : campaign_info}

        for c_id in c_id_list:
            c_info = AdsInterface.get_campaign_info_by_campaign_id(c_id)

            if c_info is None:
                continue

            # start_time and end_time need to filter
            s_time = c_info.start_time
            e_time = c_info.end_time

            final_s_time = None
            final_e_time = None

            if start_time > e_time:
                continue
            elif end_time < s_time:
                continue
            else:
                if start_time > s_time:
                    final_s_time = start_time
                else:
                    final_s_time = s_time

                if end_time > e_time:
                    final_e_time = e_time
                else:
                    final_e_time = end_time

            a_id = c_info.account_id
            platform_campaign_id = c_info.campaign_id
            c_status = c_info.status

            tmp_dict = {}
            tmp_dict['status'] = c_status
            tmp_dict['platform_campaign_id'] = platform_campaign_id
            tmp_dict['start_time'] = final_s_time
            tmp_dict['end_time'] = final_e_time
            campaign_info_map[c_id] = tmp_dict

            tmp_list = []
            if a_id in campaign_account_map:
                tmp_list = campaign_account_map[a_id]
                tmp_list.append(c_id)
            else:
                tmp_list = [c_id]
            campaign_account_map[a_id] = tmp_list

        platform_campaign_map = {}  # {platform_id : campaign_id_list}
        for a_id, c_id_list in campaign_account_map.items():
            p_id = AdsInterface.get_platform_id_by_account_id(a_id)
            if p_id is None:
                continue

            tmp_list = []
            if p_id in platform_campaign_map:
                tmp_list = platform_campaign_map[p_id]
                tmp_list.extend(c_id_list)
            else:
                tmp_list = c_id_list

            platform_campaign_map[p_id] = tmp_list

        return campaign_info_map, platform_campaign_map

    @staticmethod
    def get_company_campaign_info(company_id, campaign_id):
        return {
            "company": InsightService.get_company_data_by_id(company_id),
            "scores": InsightService.get_es_company_score_map(company_id,
                                                              campaign_id),
            "daily_metrics":
                InsightService.get_company_campaign_daily_metrics(company_id,
                                                                  campaign_id),
            "aggregated_metrics":
                InsightService.
                get_company_campaign_aggregated_metrics(company_id,
                                                        campaign_id),
        }

    # # return [
    # #     { "start_time": "2015-04-01", "impressions": 28, "clicks": 1,
    #         "conversions": 0 },
    # #     { "start_time": "2015-04-02", "impressions": 78, "clicks": 24,
    #         "conversions": 1 },
    # # ]
    @staticmethod
    def get_company_campaign_daily_metrics(company_id, campaign_id):
        return AdsInsightInterface.\
            get_company_campaign_daily_metrics(company_id, campaign_id)

    @staticmethod
    def get_company_campaign_aggregated_metrics(company_id, campaign_id):
        return AdsInsightInterface.\
            get_company_campaign_aggregated_metrics(company_id, campaign_id)
        # raise RuntimeError("Hello Exception!")
        # return {
        #     "days": "2015-04-01",
        #     "impressions": 28,
        #     "clicks": 1,
        #     "conversions": 0
        # }

    @staticmethod
    def __merge_insights(insightA, insightB):
        insightA.reach = insightA.reach + insightB.reach
        insightA.impressions = insightA.impressions + insightB.impressions
        if insightA.reach == 0:
            insightA.frequency = 0
        else:
            insightA.frequency = float(insightA.impressions) / insightA.reach
        insightA.unique_impressions = insightA.unique_impressions + \
            insightB.unique_impressions
        insightA.spend = insightA.spend + insightB.spend
        insightA.clicks = insightA.clicks + insightB.clicks
        insightA.unique_clicks = insightA.unique_clicks + \
            insightB.unique_clicks
        if insightA.impressions == 0:
            insightA.cpm = 0
            insightA.ctr = 0
        else:
            insightA.cpm = insightA.spend * 1000 / float(insightA.impressions)
            insightA.ctr = float(insightA.clicks) / insightA.impressions
        if insightA.clicks == 0:
            insightA.cpc = 0
        else:
            insightA.cpc = insightA.spend / float(insightA.clicks)
        insightA.conversion = insightA.conversion + insightB.conversion
        return insightA

    @staticmethod
    def merge_insight(ucampaign_id, start_time, end_time):
        facebook_insights = AdsInsightInterface.get_facebook_insights(
            ucampaign_id, start_time, end_time)
        google_insights = AdsInsightInterface.get_google_insights(
            ucampaign_id, start_time, end_time)
        for company_id, insight in facebook_insights.items():
            if company_id in google_insights:
                google_insights[company_id] = InsightService.__merge_insights(
                    google_insights[company_id], insight)
            else:
                google_insights[company_id] = insight

        insights = google_insights.values()
        for insight in insights:
            insight.retrieve_time = datetime.datetime.utcnow()
        AdsInsightInterface.create_insights(google_insights.values())

    @staticmethod
    def merge_insight_cron(start_time, end_time):
        ucampaign_ids = AdsInterface().get_all_united_campaign_ids()
        for ucampaign_id in ucampaign_ids:
            InsightService.merge_insight(ucampaign_id, start_time, end_time)

    # For monitoring budget
    @staticmethod
    def get_ucampaign_spent(ucampaign_id, platform_name):
        platform_id = AdsPrepareInterface().get_platform_id(platform_name)
        campaigns = AdsInterface().get_campaigns_by_ucampaign(ucampaign_id,
                                                              platform_id)
        campaign_ids = []
        platform_campaign_ids = []
        for campaign in campaigns:
            campaign_ids.append(campaign.id)
            platform_campaign_ids.append(campaign.campaign_id)
        # get total spent in DB
        total_spent = AdsInsightInterface.get_campaigns_spend(campaign_ids)

        # get today spent on platforms
        accounts = AdsPrepareInterface().get_accounts_by_ucampaign(
            platform_id, ucampaign_id)

        campaign_spent_lst = []
        if platform_name == 'facebook':
            fb_client = FBAdsClient.get_facebook_ads_client()
            fb_insight_s = FBInsightService(fb_client)
            for account in accounts:
                campaign_spent_lst = campaign_spent_lst + \
                    fb_insight_s.get_spent(account.account_id)
        elif platform_name == 'google':
            for account in accounts:
                google_client = GoogleAdwordClient(account.account_id).\
                    get_client()
                google_insight_s = GoogleInsightService(google_client,
                                                        account.account_id)
                campaign_spent_lst = campaign_spent_lst + \
                    google_insight_s.get_spent()

        for platform_campaign_id, spent in campaign_spent_lst:
            if platform_campaign_id in platform_campaign_ids:
                total_spent = total_spent + spent

        return total_spent
