__author__ = 'boxia'

from abc import ABCMeta, abstractmethod


class AbstractAdsService(object):
    __metaclass__ = ABCMeta

    # advertiser: everstring's client
    # product: advertiser's product
    # targeting_obj_list: targeting_object list, from targeting_object
    # we can get lead info, budget, bid, criteria for each lead
    # common_criteria: criteria share by all leads
    # creative: ad creative
    # budget: if it is not None, each lead share the same budget
    # bid: if it is not None, each lead share the same bid
    @abstractmethod
    def create_campaigns_common(self, advertiser_id, product_id, targeting_obj_list,\
            leads_rating, budget_bid_info=None):
        pass

    @abstractmethod
    def update_budget(self, product_id, budget_info):
        pass

    @abstractmethod
    def update_bidding(self, product_id, bid_info):
        pass
    
    @abstractmethod
    def update_status(self, product_id, status):
        pass
