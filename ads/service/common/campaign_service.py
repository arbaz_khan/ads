from ads.model import ads_interface
__author__ = 'boxia'


class CampaignService(object):

    def __init__(self):
        self.ads = ads_interface.AdsInterface()

    def set_united_campaign(self, product_id, name, budget, status=0,
                            start_time=None, end_time=None):
        return self.ads.set_united_campaign(product_id, name, budget, status,
                                            start_time, end_time)

    def update_ucampaign_status(self, ucampaign_id, status):
        self.ads.update_ucampaign_status(ucampaign_id, status)
