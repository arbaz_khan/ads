__author__ = 'dianqiang'
import sys
from os import path
import ConfigParser
sys.path.insert(0, path.dirname(path.realpath(__file__)))
sys.path.insert(0, path.dirname(path.dirname(path.realpath(__file__))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.realpath(__file__)))))
from model import ads_prepare_interface
from ads.service.common import advertiser_service
from ads.service.common import product_service
def get_google_account():
    config_file= path.join(path.dirname(path.dirname(path.dirname(__file__))), 'conf/google_api_client.ini')
    config=ConfigParser.ConfigParser()
    with open(config_file,'r') as cfgfile:  
        config.readfp(cfgfile)
    return config.get('client','client_customer_id')
if __name__ == '__main__':
    google_account_id=get_google_account()
    google_platform_id='1'
    google_platform_account_id=get_google_account()
    google_platform_account_name='test'
    company_name = 'test'
    product_name='test'

    adver_service = advertiser_service.AdvertiserService()
    adver_id = adver_service.set_advertiser(company_name)
    product_id = product_service.ProductService().add_product(adver_id, product_name)

    prepare_interface=ads_prepare_interface.AdsPrepareInterface()
    prepare_interface.set_account(google_platform_id,google_platform_account_id,google_platform_account_name,product_id)
       

