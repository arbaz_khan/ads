__author__ = 'nenggong'

from ads.model import ads_prepare_interface


class AdvertiserService(object):

    def __init__(self):
        self.ads_prepare = ads_prepare_interface.AdsPrepareInterface()

    def set_advertiser(self, name):
        advertiser = self.ads_prepare.set_advertiser(name)
        return advertiser

    def get_advertiser(self, advertiser_id):
        return self.ads_prepare.get_advertiser_by_id(advertiser_id)
