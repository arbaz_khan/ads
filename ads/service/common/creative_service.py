__author__ = 'boxia'

from ads.model.ads_prepare_interface import AdsPrepareInterface


class CreativeService(object):

    def __init__(self):
        self.ads_prepare = AdsPrepareInterface()


    def set_creative(self, ucampaign_id, platform_id, landing_page_url, headline,
               description_1, description_2, image_url):
        creative = self.ads_prepare.set_creative(ucampaign_id, platform_id, \
                landing_page_url, headline, description_1, description_2, image_url)
        return creative

    def get_creatives(self, platform_id, ucampaign_id):
        creatives = self.ads_prepare.get_creatives(ucampaign_id, platform_id)
        return creatives
