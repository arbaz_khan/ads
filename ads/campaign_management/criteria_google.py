__author__ = 'boxia'

from abc import ABCMeta, abstractmethod
from criteria import Criteria
from criteria import CriteriaCommon
from ads.service.common.targeting_service import TargetingService


class GoogleCriteria(Criteria):


    def get_geo_location(self):
        pass

    def get_criteria(self):
        criteria = dict()
        criteria['geo_location'] = self.get_geo_location()
        return criteria



    
class GoogleCriteriaCommon(CriteriaCommon):
    def __init__():
        self.targeting_s = TargetingService()

    def get_placement(self):
        pass

    def get_keywords(self):
        return self.targeting_s.get_keywords()

    def get_ad_schedule(self):
        pass

    def get_criteria(self):
        criteria = dict()
        criteria['placement'] = self.get_placement()
        criteria['keywords'] = self.get_keywords()
        criteria['ad_schedule'] = self.get_ad_schedule()
        return criteria
