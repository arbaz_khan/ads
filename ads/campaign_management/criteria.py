__author__ = 'boxia'

from abc import ABCMeta, abstractmethod

class Criteria(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_geo_location(self):
        pass

    @abstractmethod
    def get_criteria(self):
        pass

    
class CriteriaCommon(object):
    __metaclass__ = ABCMeta
    

    @abstractmethod
    def get_placement(self):
        pass

    @abstractmethod
    def get_criteria(self):
        pass

