import os
from multiprocessing import Queue
from threading import Thread

from ads.service.common.advertiser_service import AdvertiserService
from ads.service.common.product_service import ProductService
from ads.service.common.targeting_service import TargetingService
from ads.service.common.account_service import AccountService
from ads.service.common.platform_service import PlatformService
from ads.service.common.creative_service import CreativeService
from ads.service.common.config_service import ConfigService
from ads.service.common.campaign_service import CampaignService
from targeting_object import TargetingObj

import ConfigParser

__author__ = 'nenggong, boxia'


class Advertising(object):

    def __init__(self, platform_list=['facebook']):
        self.platforms = platform_list

        self.modules = []
        # dynamically import modules according to platforms
        for directory in self.platforms:
            # import like from service.google import advertising_service
            self.modules.append(
                __import__('ads.service.{0}'.format(directory),
                           globals(),
                           locals(),
                           ['advertising_service'], -1))

    def set_advertiser(self, advertiser_name):
        # create advertiser
        advertiser_s = AdvertiserService()
        advertiser = advertiser_s.set_advertiser(advertiser_name)
        return advertiser

    def set_product(self, advertiser_id, product_name):
        # create product
        product_s = ProductService()
        product = product_s.set_product(advertiser_id, product_name)
        return product

    def set_account(self, platform_name, platform_account_id,
                    platform_account_name, product_id):
        # create account
        account_s = AccountService()
        platform_id = PlatformService().get_platform_id(platform_name)
        account_s.set_account(
            platform_id, platform_account_id,
            platform_account_name, product_id)

    def set_united_campaign(self, product_id, name,
                            budget, start_time=None, end_time=None):
        campaign_s = CampaignService()
        return campaign_s.set_united_campaign(product_id, name, budget, 0,
                                              start_time, end_time)

    def set_creative_config(self, ucampaign_id, config_file_path):
        platform_s = PlatformService()
        targeting_s = TargetingService()
        cf = ConfigParser.ConfigParser()
        cf.read(config_file_path)
        image_file = cf.get("image", "img_name")
        object_url = cf.get("creative", "object_url")
        title = cf.get("creative", "title"),
        body = cf.get("creative", "body"),
        pixel_id = cf.get("facebook", "fb_pixel_id"),
        objective = cf.get("facebook", "fb_objective"),
        show_conversion = cf.get("others", "show_conversion")
        show_engagement = cf.get("others", "show_engagement")

        path = os.path.dirname(os.path.realpath(__file__))
        image_file = os.path.realpath(path + '/../' + image_file)

        creative_s = CreativeService()
        config_s = ConfigService()

        # 0 means config for all platform
        config_s.set_campaign_config(ucampaign_id, 0,
                                     'show_conversion', show_conversion)
        config_s.set_campaign_config(ucampaign_id, 0,
                                     'show_engagement', show_engagement)

        # for facebook
        facebook_platform_id = platform_s.get_platform_id('facebook')
        creative_s.set_creative(ucampaign_id, facebook_platform_id,
                                object_url, title, body, None, image_file)
        config_s.set_campaign_config(ucampaign_id, facebook_platform_id,
                                     'pixel_id', pixel_id)
        config_s.set_campaign_config(ucampaign_id, facebook_platform_id,
                                     'objective', objective)
        # for google
        google_platform_id = platform_s.get_platform_id('google')
        dir_path = cf.get("google", "input_dir")
        image_list = cf.get("google", "image_list").split(',')
        for image in image_list:
            image_file = dir_path + '/' + image
            creative_s.set_creative(ucampaign_id, google_platform_id,
                                    object_url, title, body, None, image_file)
        # for google keyword
        keyword_file = dir_path + '/' + cf.get("google", "keywords")
        file_object = open(keyword_file)
        keywords = []
        for line in file_object:
            keywords.append(line.strip('\n'))
        targeting_s.set_keywords(ucampaign_id, keywords)
        # for google campaign config
        config_s.set_campaign_config(ucampaign_id, google_platform_id,
                                     'bidding_strategy_type',
                                     cf.get("google", 'bidding_strategy_type'))
        config_s.set_campaign_config(ucampaign_id, google_platform_id,
                                     'start_date',
                                     cf.get("google", 'start_date'))
        config_s.set_campaign_config(ucampaign_id, google_platform_id,
                                     'end_date',
                                     cf.get("google", 'end_date'))
        config_s.set_campaign_config(ucampaign_id, google_platform_id,
                                     'status', cf.get("google", 'status'))
        config_s.set_campaign_config(ucampaign_id, google_platform_id,
                                     'ad_channel_type',
                                     cf.get("google", 'ad_channel_type'))

    def import_leads(self, ucampaign_id, input_file):
        targeting_s = TargetingService()
        lead_ids = targeting_s.import_leads(ucampaign_id, input_file)
        return lead_ids

    def set_budget_bid(self, platform_name, lead_ids, budget_bid_info):
        platform_id = PlatformService().get_platform_id(platform_name)
        targeting_s = TargetingService()
        targeting_s.set_budget_bid_batch(platform_id, lead_ids,
                                         budget_bid_info)

    def create_campaigns_old(self, advertiser_id, ucampaign_id,
                             budget_bid_info=None):

        threads = []
        # to put return values from threads/processes
        results = Queue()

        targeting_s = TargetingService()
        for module in self.modules:
            adv_service = module.advertising_service.AdvertisingService()
            # create 4 threads for different types of leads from A to D
            for lead_type in range(ord('A'), ord('E')):
                grade = chr(lead_type)
                leads = targeting_s.get_leads(ucampaign_id, grade, 0)
                targeting_obj_lst = list()
                for lead in leads:
                    targeting_obj = TargetingObj(lead)
                    targeting_obj_lst.append(targeting_obj)
                if targeting_obj_lst and len(targeting_obj_lst) > 0:
                    thread = Thread(target=adv_service.create_campaigns_common,
                                    args=(results, advertiser_id, ucampaign_id,
                                          targeting_obj_lst, grade,
                                          budget_bid_info))
                    threads.append(thread)
                    thread.start()

                leads_control = targeting_s.get_leads(ucampaign_id, grade, 1)
                targeting_obj_lst_control = list()
                for lead_control in leads_control:
                    targeting_obj_control = TargetingObj(lead_control)
                    targeting_obj_lst_control.append(targeting_obj_control)
                if targeting_obj_lst_control and \
                        len(targeting_obj_lst_control) > 0:
                    thread = Thread(target=adv_service.create_campaigns_common,
                                    args=(results, advertiser_id, ucampaign_id,
                                          targeting_obj_lst_control, grade,
                                          budget_bid_info))
                    threads.append(thread)
                    thread.start()

        for thread in threads:
            thread.join()

        return self.parse_error_log(results)

    def create_campaigns(self, advertiser_id, ucampaign_id,
                         budget_bid_info=None):

        threads = []

        for module in self.modules:
            thread = Thread(
                target=self.create_campaigns_thread,
                args=(module, advertiser_id, ucampaign_id, budget_bid_info),
                name=module.
                advertising_service.AdvertisingService.PLATFORM_NAME)
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

    def create_campaigns_thread(self, module, advertiser_id,
                                ucampaign_id, budget_bid_info=None):

        # to put return values from threads/processes
        results = Queue()

        targeting_s = TargetingService()
        adv_service = module.advertising_service.AdvertisingService()
        # create 4 threads for different types of leads from A to D
        for lead_type in range(ord('A'), ord('E')):
            grade = chr(lead_type)
            leads = targeting_s.get_leads(ucampaign_id, grade, 0)
            targeting_obj_lst = list()
            for lead in leads:
                targeting_obj = TargetingObj(lead)
                targeting_obj_lst.append(targeting_obj)
            if targeting_obj_lst and len(targeting_obj_lst) > 0:
                adv_service.create_campaigns_common(results,
                                                    advertiser_id,
                                                    ucampaign_id,
                                                    targeting_obj_lst,
                                                    grade,
                                                    budget_bid_info)

            leads_control = targeting_s.get_leads(ucampaign_id, grade, 1)
            targeting_obj_lst_control = list()
            for lead_control in leads_control:
                targeting_obj_control = TargetingObj(lead_control)
                targeting_obj_lst_control.append(targeting_obj_control)
            if targeting_obj_lst_control and \
                    len(targeting_obj_lst_control) > 0:
                adv_service.create_campaigns_common(results, advertiser_id,
                                                    ucampaign_id,
                                                    targeting_obj_lst_control,
                                                    grade, budget_bid_info)

        return self.parse_error_log(results)

    def update_budget(self, ucampaign_id, budget_info):
        threads = []
        results = Queue()
        for module in self.modules:
            adv_service = module.advertising_service.AdvertisingService()
            thread = Thread(target=adv_service.update_budget_common,
                            args=(results, ucampaign_id, budget_info))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

        return self.parse_error_log(results)

    def update_bidding(self, ucampaign_id, bid_info):
        threads = []
        results = Queue()
        for module in self.modules:
            adv_service = module.advertising_service.AdvertisingService()
            thread = Thread(target=adv_service.update_bidding_common,
                            args=(results, ucampaign_id, bid_info))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

        return self.parse_error_log(results)

    def parse_error_log(self, results):
        all_complete = True

        try:
            size = results.qsize()
        except NotImplementedError:
            '''
            OSX doesn't implement this method, hardcore the queue size using
            thread amount.
            '''
            size = len(self.modules)

        for i in range(size):
            err_code, err_msg = results.get()
            if err_code != 0:
                # TODO LOG HERE
                all_complete = False
            print err_msg
        if all_complete:
            # TODO LOG HERE
            print 'Successfully finish advertising in %s platforms.'\
                % str(self.platforms)
        return all_complete

    def update_campaign_status(self, ucampaign_id, status):
        if status != 0:
            status = 1

        threads = []
        results = Queue()
        for module in self.modules:
            adv_service = module.advertising_service.AdvertisingService()
            thread = Thread(target=adv_service.update_status_common,
                            args=(results, ucampaign_id, status))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

        campaign_s = CampaignService()
        campaign_s.update_ucampaign_status(ucampaign_id, status)

        return self.parse_error_log(results)


if __name__ == '__main__':
    pass
