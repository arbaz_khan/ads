__author__ = 'boxia'

from abc import ABCMeta, abstractmethod
from criteria import Criteria
from criteria import CriteriaCommon
from ads.service.common.targeting_service import TargetingService


class FBCriteria(Criteria):
    def __init__(self):
        self.targeting_s = TargetingService()

    def get_geo_location(self):
        pass

    def get_work_employers(self, company_id):
        return self.targeting_s.get_facebook_company(company_id)

        

    def get_criteria(self):
        criteria = dict()
        criteria['geo_location'] = self.get_geo_location()
        criteria['work_employers'] = self.get_work_employers()
        return criteria


    
class FBCriteriaCommon(CriteriaCommon):

    # temperarily hard code here
    def get_placement(self):
        return 'rightcolumn'

    def get_criteria():
        criteria = dict(self)
        criteria['placement'] = self.get_placement()
        return criteria
    



