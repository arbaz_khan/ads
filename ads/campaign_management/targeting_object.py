__author__ = 'boxia'

from ads.service.common.targeting_service import TargetingService


class TargetingObj(object):
    targeting_s = TargetingService()

    def __init__(self, lead):
        self.lead = lead

    def get_budget(self, platform_id):
        return self.targeting_s.get_budget(platform_id, self.lead.id)

    def get_bid(self, platform_id):
        return self.targeting_s.get_bid(platform_id, self.lead.id)

    def get_criteria(self, platform_id, criteria_obj):
        return criteria_obj.get_criteria()
