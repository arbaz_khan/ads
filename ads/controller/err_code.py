# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

from flask import jsonify
from ads.model.session import DBSession


class ERR_INFO(object):
    SUCCESS = 200
    BAD_REQUEST = 400
    INTERNAL_SERV_ERR = 500

    ERR_DIC = {
        SUCCESS: "",
        BAD_REQUEST: "bad request",
        INTERNAL_SERV_ERR: "internal server error"
    }


class controller_data(object):

    def __init__(self):
        self.status = ERR_INFO.SUCCESS
        self.status_info = ERR_INFO.ERR_DIC[ERR_INFO.SUCCESS]
        self.data = {}

    def set_status(self, status):
        self.status = status

    def set_status_info(self, status_info):
        self.status_info = status_info

    def set_data(self, data={}):
        self.data = data

    def get_ret_data(self):
        ret_dic = {}
        ret_dic['status'] = self.status
        ret_dic['statusInfo'] = self.status_info
        ret_dic['data'] = self.data
        response = jsonify(ret_dic)
        response.headers['Access-Control-Allow-Origin'] = "*"

        session = DBSession.get_db_session()
        if session:
            session.close()

        return response

if __name__ == "__main__":
    # print ERR_INFO.BAD_REQUEST
    # print ERR_INFO.ERR_DIC[ERR_INFO.BAD_REQUEST]
    pass
