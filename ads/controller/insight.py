# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

# This python file includes ads insight
# controller interface.

import StringIO
import csv
from datetime import datetime
from flask import Flask, make_response
from flask import request
import traceback
from ads.controller.err_code import ERR_INFO
from ads.controller.err_code import controller_data
from ads.service.common.insight_service import InsightService
from ads.utils.log import Logger
from ads.controller.ignore_decimal_json_encoder import IgnoreDecimalJSONEncoder

application = Flask(__name__)
application.json_encoder = IgnoreDecimalJSONEncoder


@application.errorhandler(BaseException)
def general_exception_handler(error):
    '''
    handle all exceptions, log detailed error trace.

    BaseException is the base for all exception classes,
    including built-in and user-defined ones. This one can
    capture all unhandled exceptions.
    '''
    errormsg = ''
    if __debug__:
        errormsg = str(error)
    else:
        errormsg = ERR_INFO.ERR_DIC[ERR_INFO.INTERNAL_SERV_ERR]

    ret = controller_data()
    ret.set_status(ERR_INFO.INTERNAL_SERV_ERR)
    ret.set_status_info(errormsg)
    log(ERR_INFO.INTERNAL_SERV_ERR, str(error))

    res = ret.get_ret_data()
    res.status_code = 500

    return res


@application.route('/status')
def status():
    ret = controller_data()
    ret.data = {"status": "ok"}
    return ret.get_ret_data()


# Example: /ads/insight/campaigns/1/companies/13
@application. \
    route('/ads/insight/campaigns/<campaign_id>/companies/<company_id>')
def show_campaign_company_profile(campaign_id, company_id):
    ret = controller_data()
    data = InsightService.get_company_campaign_info(company_id, campaign_id)
    data = {key: to_dict(value) for key, value in data.iteritems()}
    ret.set_data(data)
    return ret.get_ret_data()


@application.route('/ads/insight/campaigns', methods=['GET'])
def campaignList():
    data = get_campaign_insight_data_list()

    if request.args.get('format') == 'csv':
        start_date = request.args.get('startTime')
        end_date = request.args.get('endTime')
        filename = 'Campaign_List_(%s/%s).csv' % (start_date, end_date)

        keys = [
            ('Campaign', 'campaignName'),
            ('Status', 'status'),
            ('Impressions', 'imp'),
            ('Clicks', 'clicks'),
            ('Conversion', 'conv'),
            ('CTR', 'ctr'),
            ('CPM', 'cpm'),
            ('CPC', 'cpc'),
            ('Spend', 'spend'),
            ('Budget', 'budget'),
        ]

        rows = data['list']
        csv = build_csv(keys, rows)

        return make_csv_response(filename, csv)
    else:
        ret = controller_data()
        ret.set_data(data)
        return ret.get_ret_data()


# TODO: -> /ads/insight/campaigns/<:campaign_id>/funnel
@application.route('/ads/insight/funnel', methods=['GET'])
def funnel():
    campaign_id = request.args.get("campId")
    start_date = request.args.get("startTime")
    end_date = request.args.get("endTime")

    response = controller_data()
    data = InsightService.get_campaign_funnel_data(campaign_id,
                                                   start_date,
                                                   end_date)
    response.set_data(data)

    return response.get_ret_data()


# TODO: -> /ads/insight/campaigns/<:campaign_id>/spend-totals
@application.route('/ads/insight/spendTotals', methods=['GET'])
def campaignInfo():
    campaign_id = request.args.get("campId")
    start_date = request.args.get("startTime")
    end_date = request.args.get("endTime")

    response = controller_data()
    data = InsightService.get_campaign_spend_data(campaign_id,
                                                  start_date,
                                                  end_date)
    response.set_data(data)

    return response.get_ret_data()


# TODO: -> /ads/insight/campaigns/<:campaign_id>/companies
@application.route('/ads/insight/companies', methods=['GET'])
def companyList():
    data = get_company_data_list(request.args)

    if request.args.get('format') == 'csv':
        start_date = request.args.get('startTime')
        end_date = request.args.get('endTime')
        filename = 'Company_List_(%s/%s).csv' % (start_date, end_date)

        keys = [
            ('Account', 'name'),
            ('Domain Info', 'domain'),
            ('EverString Rating', 'grade'),
            ('Industry', 'industry'),
            ('Employee', 'employee_size'),
            ('Revenue', 'revenue'),
            ('Reach', 'reaches'),
            ('Impressions', 'impressions'),
            ('Clicks', 'clicks'),
            ('CTR', 'ctr'),
            ('CPM', 'cpm'),
            ('CPC', 'cpc'),
            ('Spend', 'spend')
        ]

        rows = data['list']
        csv = build_csv(keys, rows)

        return make_csv_response(filename, csv)
    else:
        ret = controller_data()
        ret.set_data(data)
        return ret.get_ret_data()


def get_company_data_list(params):
    '''
    sanitize params, date validation.
    '''
    # http://werkzeug.pocoo.org/dget_campaign_spend_dataocs/0.10/datastructures/#werkzeug.datastructures.MultiDict.to_dict
    cur_page = int(params.get('currentPage', 1))
    page_size = int(params.get('pageSize', 50))
    sort_key = str(params.get('sortKey', 'clicks'))
    sort_dir = str(params.get('sortDir', 'DESC'))
    search_key = str(params.get('searchKey', ''))
    start_time = str(params.get("startTime", ''))
    end_time = str(params.get("endTime", ''))
    camp_id = int(params.get('campId'))

    if start_time == "" or end_time == "":
        raise ValueError("`startTime` or `endTime` parameters are required.")

    if not before(start_time, end_time):
        raise ValueError("`startTime` must be before `endTime`.")

    return InsightService.get_company_insight(camp_id, cur_page, page_size,
                                              sort_key, sort_dir, search_key,
                                              start_time, end_time)


def get_campaign_insight_data_list():
    ret_data = {}
    ret_data['list'] = []
    ret_data['count'] = 0

    params = request.args

    sort_key = ""
    if 'sortKey' in params:
        sort_key = params['sortKey']

    sort_dir = ""
    if 'sortDir' in params:
        sort_dir = params['sortDir']

    search_key = None
    if 'searchKey' in params:
        search_key = params['searchKey']

    start_time = ""
    end_time = ""
    if 'startTime' in params:
        start_time = params['startTime']

    if 'endTime' in params:
        end_time = params['endTime']

    s_time = None
    e_time = None

    ret = controller_data()
    if start_time == "" or end_time == "":
        ret.status(ERR_INFO.BAD_REQUEST)
        ret.status_info(ERR_INFO.ERR_DIC[ERR_INFO.BAD_REQUEST])
        return ret.get_ret_data()
    else:
        s_time = datetime.strptime(start_time, "%Y-%m-%d")
        e_time = datetime.strptime(end_time, "%Y-%m-%d")
        if s_time > e_time:
            ret.status(ERR_INFO.BAD_REQUEST)
            ret.status_info(ERR_INFO.ERR_DIC[ERR_INFO.BAD_REQUEST])
            return ret.get_ret_data()

    data = InsightService.get_united_campaign_list(search_key)

    for one in data['list']:
        tmp_dic = {}
        tmp_dic['id'] = one['id']
        tmp_dic['campaignName'] = one['name']
        tmp_dic['status'] = ""
        if one['status'] == 1:
            tmp_dic['status'] = "Alive"
        elif one['status'] == 0:
            tmp_dic['status'] = "Paused"
        elif one['status'] == 2:
            tmp_dic['status'] = "Complete"

        campaign_data = InsightService. \
            get_insight_data_by_united_campaign_id(one['id'],
                                                   start_time,
                                                   end_time)

        tmp_dic['imp'] = campaign_data['imp']
        tmp_dic['clicks'] = campaign_data['clicks']
        tmp_dic['conv'] = campaign_data['conv']
        tmp_dic['ctr'] = campaign_data['ctr']
        tmp_dic['cpm'] = campaign_data['cpm']
        tmp_dic['cpc'] = campaign_data['cpc']
        tmp_dic['spend'] = campaign_data['spend']
        tmp_dic['budget'] = campaign_data['budget']
        ret_data['list'].append(tmp_dic)

    if sort_key:
        if sort_key == "name":
            sort_key = "campaignName"
        if sort_dir == "desc":
            ret_data['list'] = sorted(ret_data['list'], cmp=lambda x, y: cmp(x[sort_key], y[sort_key]), reverse=True)
        else:
            ret_data['list'] = sorted(ret_data['list'], cmp=lambda x, y: cmp(x[sort_key], y[sort_key]))

    print 'almost there...'
    ret_data['count'] = len(ret_data['list'])

    return ret_data


# TODO: move to independant module
def build_csv(keys, rows):
    '''
    keys: [(descriptive field name, key of dict), ...]
    rows: [{ key1: value1, key2: value2 }, ...]
    '''
    field_names = [item[0] for item in keys]
    sio = StringIO.StringIO()
    writer = csv.DictWriter(sio, fieldnames=field_names)

    writer.writeheader()
    for row in rows:
        mapped = {}
        for field_name, key in keys:
            value = unicode(row.get(key, '')).encode('utf-8')
            mapped[field_name] = value
        print mapped
        writer.writerow(mapped)

    return sio.getvalue()


def make_csv_response(filename, csv):
    '''
    make downloadable file response
    '''
    response = make_response(csv)
    response.headers["Content-Disposition"] = "attachment; filename=\"%s\"" % \
        (filename)
    response.headers["Content-type"] = "text/csv"

    return response


def before(d1, d2):
    print d1, d2
    dateformat = "%Y-%m-%d"
    dt1 = datetime.strptime(d1, dateformat)
    dt2 = datetime.strptime(d2, dateformat)
    return dt1 < dt2


def to_dict(obj):
    if hasattr(obj, "to_dict"):
        return obj.to_dict()
    else:
        return obj


def log(status, status_info):
    log_msg = 'Error code: {0}, Error msg: {1}, Stack: {2}'.\
        format(status, status_info, traceback.print_exc())
    Logger.get_logger().error(log_msg)


if __name__ == "__main__":
    application.run(host='0.0.0.0', port=5000)
