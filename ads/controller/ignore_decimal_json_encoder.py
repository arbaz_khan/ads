from decimal import Decimal
from flask.json import JSONEncoder


class IgnoreDecimalJSONEncoder(JSONEncoder):

    '''
    Sqlachelmy would return numeric values as Decimal type, which is not
    JSON convertible.
    use this class to provide supports for that additional type.
    Why bring in this obstacle in the first place...
    '''

    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        else:
            return super(IgnoreDecimalJSONEncoder, self).default(obj)
