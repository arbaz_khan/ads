import datetime
from sqlalchemy import Column, String, Float, BigInteger, DateTime
from sqlalchemy import SmallInteger, TIMESTAMP
from session import DBSession
from base import Base
__author__ = 'boxia'


class UnitedCampaign(Base):
    __tablename__ = "united_campaign"
    id = Column(BigInteger, primary_key=True)
    name = Column(String(256))
    product_id = Column(BigInteger)
    budget = Column(Float)
    status = Column(SmallInteger)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(product_id, ucampaign_name, budget, status=0,
               start_time=None, end_time=None):
        if not ucampaign_name or not product_id:
            return None
        try:
            session = DBSession.get_db_session()
            ucampaign = session.query(UnitedCampaign).\
                filter(UnitedCampaign.product_id == product_id).\
                filter(UnitedCampaign.name == ucampaign_name).first()
            if ucampaign is None:
                ucampaign = UnitedCampaign()
                ucampaign.name = ucampaign_name
                ucampaign.product_id = product_id
                ucampaign.budget = budget
                ucampaign.status = status
                ucampaign.start_time = start_time
                ucampaign.end_time = end_time
                current_time = datetime.datetime.utcnow()
                ucampaign.create_time = current_time
                ucampaign.update_time = current_time
                session.add(ucampaign)
                session.commit()
            return ucampaign
        except:
            raise

    @staticmethod
    def get(ucampaign_id):
        session = DBSession.get_db_session()
        ucampaign = session.query(UnitedCampaign).get(ucampaign_id)
        return ucampaign

    @staticmethod
    def get_all_united_campaign_core_data(search_key=None):
        try:
            session = DBSession.get_db_session()

            search_str = ""
            if search_key:
                search_str = "%" + search_key + "%"
            else:
                search_str = "%%"

            campaigns = session.query(UnitedCampaign).\
                filter(UnitedCampaign.name.like(search_str)).\
                order_by(UnitedCampaign.name).all()
            data = []
            for one in campaigns:
                tmp = {}
                tmp['name'] = one.name
                tmp['id'] = one.id
                tmp['status'] = one.status
                data.append(tmp)

            return data

        except:
            raise
