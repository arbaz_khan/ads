# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from base import Base
from session import DBSession


class AdsKeyword(Base):
    __tablename__ = "keyword"
    id = Column(BigInteger, primary_key=True)
    united_campaign_id = Column(BigInteger)
    keyword = Column(String(256))

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create_batch(united_campaign_id, keywords_list):
        ads_keywords = list()
        try:
            for word in keywords_list:
                ads_keyword = AdsKeyword()
                ads_keyword.set(united_campaign_id, word)
                ads_keywords.append(ads_keyword)

                session = DBSession.get_db_session()
                session.add_all(ads_keywords)
                session.commit()
                return 1
        except:
            session.rollback()
            raise

    def set(self, united_campaign_id, keyword):
        self.united_campaign_id = united_campaign_id
        self.keyword = keyword
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def create(united_campaign_id, keyword):
        if not united_campaign_id or not keyword.strip():
            return None

        session = DBSession.get_db_session()
        ads_keyword = session.query(AdsKeyword).\
            filter(AdsKeyword.united_campaign_id == united_campaign_id).\
            filter(AdsKeyword.keyword == keyword).first()
        if ads_keyword is None:
            ads_keyword = AdsKeyword()
            ads_keyword.set(united_campaign_id, keyword)
            session.add(ads_keyword)
            session.commit()
        return ads_keyword

    @staticmethod
    def get_keywords(united_campaign_id):
        keywords = list()
        if not united_campaign_id:
            return keywords
        session = DBSession.get_db_session()
        ads_keywords = session.query(AdsKeyword).\
            filter(AdsKeyword.united_campaign_id == united_campaign_id).all()
        for ads_keyword in ads_keywords:
            keywords.append(ads_keyword.keyword)
        return keywords


if __name__ == '__main__':
    print AdsKeyword.create_batch(1, ['autodesk product', 'CAD'])
    keyword = AdsKeyword.create(1, "CAD product")
    print keyword.keyword
    print AdsKeyword.get_keywords(1)
