# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, SmallInteger, DateTime
from sqlalchemy import TIMESTAMP

from base import Base
from session import DBSession


class Campaign(Base):
    __tablename__ = "campaign"
    id = Column(BigInteger, primary_key=True)

    account_id = Column(BigInteger)
    campaign_id = Column(String(256))

    name = Column(String(256))
    status = Column(SmallInteger)

    start_time = Column(DateTime)
    end_time = Column(DateTime)
    platform_id = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP(timezone=True),
                         onupdate=datetime.datetime.utcnow)

    @staticmethod
    def get_campaigns_by_account(account_id):
        session = DBSession.get_db_session()
        campaigns = session.query(Campaign). \
            filter_by(account_id=account_id).all()

        return campaigns

    @staticmethod
    def create(account_id, campaign_id, start_time, end_time, name,
               status, platform_id):
        if not account_id or not campaign_id:
            return None
        session = DBSession.get_db_session()
        campaign_obj = session.query(Campaign).filter_by(
            account_id=account_id, campaign_id=campaign_id).first()
        if campaign_obj is None:
            campaign_obj = Campaign()
            campaign_obj.campaign_id = campaign_id
            campaign_obj.name = name
            campaign_obj.status = status
            campaign_obj.start_time = start_time
            campaign_obj.end_time = end_time
            campaign_obj.account_id = account_id
            campaign_obj.platform_id = platform_id

            current_time = datetime.datetime.utcnow()
            campaign_obj.create_time = current_time
            campaign_obj.update_time = current_time
            session = DBSession.get_db_session()
            session.add(campaign_obj)
            session.commit()

        return campaign_obj

    # campaign_id is our system campaign id
    @staticmethod
    def update(campaign_id, name, status):
        session = DBSession.get_db_session()
        campaign = session.query(Campaign).get(campaign_id)

        if name is not None:
            campaign.name = name

        if status is not None:
            campaign.status = status

        session.commit()
        return 0

    @staticmethod
    def get_campaign_info_by_campaign_id(c_id):
        session = DBSession.get_db_session()
        campaign = session.query(Campaign).get(c_id)
        return campaign

    @staticmethod
    def get_campaign_by_platform_campaign(platform_id, platform_campaign_id):
        session = DBSession.get_db_session()
        campaign = session.query(Campaign).\
            filter_by(platform_id=platform_id).\
            filter_by(campaign_id=platform_campaign_id).first()
        return campaign
