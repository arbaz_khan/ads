# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, Float, TIMESTAMP

from base import Base
from session import DBSession


class Address(Base):
    __tablename__ = "address"
    id = Column(BigInteger, primary_key=True)
    address = Column(String(1024))
    longitude = Column(Float)
    latitude = Column(Float)

    company_id = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(company_id, address, longitude, latitude):
        if not company_id:
            return None
        session = DBSession.get_db_session()
        address_obj = session.query(Address).\
            filter_by(company_id=company_id,
                      longitude=longitude,
                      latitude=latitude,
                      address=address).\
            first()

        if address_obj is None:
            address_obj = Address()
            address_obj.address = address
            address_obj.longitude = longitude
            address_obj.latitude = latitude
            address_obj.company_id = company_id
            current_time = datetime.datetime.now()
            address_obj.create_time = current_time
            address_obj.update_time = current_time
            session.add(address_obj)
            session.commit()

        return address_obj

    @staticmethod
    def get_address_list(company_id):
        address_list = list()
        if not company_id:
            return address_list

        session = DBSession.get_db_session()
        address_objs = session.query(Address).filter_by(
            company_id=company_id).all()
        for address_obj in address_objs:
            address = dict()
            address['longitude'] = address_obj.longitude
            address['latitude'] = address_obj.latitude
            address['address'] = address_obj.address
            address_list.append(address)

        return address_list
