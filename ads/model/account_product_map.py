import datetime
from sqlalchemy import Column, BigInteger, DateTime
from sqlalchemy import TIMESTAMP
from base import Base
from session import DBSession

__author__ = 'boxia'


class AccountProductMap(Base):
    __tablename__ = "account_product_map"
    id = Column(BigInteger, primary_key=True)
    # Our system account id, not other platform account_id
    account_id = Column(BigInteger)
    product_id = Column(BigInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    # param account_id is our system account id
    @staticmethod
    def create(account_id, product_id):
        try:
            session = DBSession.get_db_session()
            account_product_map = session.query(AccountProductMap).\
                filter(AccountProductMap.product_id == product_id).\
                filter(AccountProductMap.account_id == account_id).first()

            if account_product_map is None:
                account_product_map = AccountProductMap()
                account_product_map.set(account_id, product_id)
                session.add(account_product_map)
                session.commit()
            return 0
        except:
            raise

    def set(self, account_id, product_id):
        self.account_id = account_id
        self.product_id = product_id
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def get_accounts_by_product(product_id):
        session = DBSession.get_db_session()
        items = session.query(AccountProductMap.account_id).\
            filter(AccountProductMap.product_id == product_id).all()
        account_ids = list()
        for item in items:
            account_ids.append(item.account_id)
        return account_ids

    @staticmethod
    def get_products_by_account(account_id):
        session = DBSession.get_db_session()
        items = session.query(AccountProductMap.product_id).\
            filter(AccountProductMap.account_id == account_id).all()
        product_ids = list()
        for item in items:
            product_ids.append(item.product_id)
        return product_ids


if __name__ == '__main__':
    account = AccountProductMap.create(1, 2)
    print AccountProductMap.get_accounts_by_product(5)
    print AccountProductMap.get_accounts_by_product(2)
    print AccountProductMap.get_products_by_account(5)
    print AccountProductMap.get_products_by_account(1)
