
import datetime

from sqlalchemy import Column, String, BigInteger, DateTime

from base import Base
from session import DBSession
__author__ = 'boxia'


class FBCompany(Base):
    __tablename__ = "fb_company"
    id = Column(BigInteger, primary_key=True)
    company_id = Column(BigInteger)
    fb_company_id = Column(BigInteger)
    fb_company_name = Column(String(256))

    create_time = Column(DateTime)
    update_time = Column(DateTime)

    @staticmethod
    def create(company_id, fb_company_id, fb_company_name):
        if not company_id or not fb_company_id:
            return -1

        session = DBSession.get_db_session()
        fb_company = session.query(FBCompany).\
            filter_by(company_id=company_id,
                      fb_company_id=fb_company_id).\
            first()

        if fb_company is None:
            fb_company = FBCompany()
            fb_company.set(company_id, fb_company_id, fb_company_name)
            session.add(fb_company)
            session.commit()

        return fb_company

    def set(self, company_id, fb_company_id, fb_company_name):
        self.company_id = company_id
        self.fb_company_id = fb_company_id
        self.fb_company_name = fb_company_name
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def get_facebook_company(company_id):
        session = DBSession.get_db_session()
        fb_company = session.query(FBCompany).\
            filter_by(company_id=company_id).\
            first()
        return fb_company
