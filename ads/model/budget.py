# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from base import Base
from session import DBSession


class Budget(Base):
    __tablename__ = "budget"

    id = Column(BigInteger, primary_key=True)
    budget_id = Column(String(256))
    platform_id = Column(BigInteger)
    budget_type = Column(String(256))
    budget_amount = Column(BigInteger)
    company_score_map_id = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(platform_id, budget_type, budget_amount,
               company_score_map_id, budget_id=None):
        if not platform_id or not budget_type or \
                not budget_amount or not company_score_map_id:
            return -1

        session = DBSession.get_db_session()
        budget = session.query(Budget).\
            filter_by(platform_id=platform_id,
                      company_score_map_id=company_score_map_id).first()
        if budget is None:
            budget = Budget()
            budget.set(platform_id, budget_type, budget_amount,
                       company_score_map_id, budget_id)
            session.add(budget)
            session.commit()

        return budget

    def set(self, platform_id, budget_type, budget_amount,
            company_score_map_id, budget_id=None):
        self.budget_id = budget_id
        self.platform_id = platform_id
        self.budget_type = budget_type
        self.budget_amount = budget_amount
        self.company_score_map_id = company_score_map_id

        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def get(platform_id, company_score_map_id):
        session = DBSession.get_db_session()
        budget = session.query(Budget).\
            filter_by(platform_id=platform_id,
                      company_score_map_id=company_score_map_id).first()
        return budget

    @staticmethod
    def update_budget_id(id, budget_id):
        session = DBSession.get_db_session()
        budget = session.query(Budget).get(id)
        if budget is not None:
            budget.budget_id = budget_id
            session.commit()

    @staticmethod
    def get_budgets_by_campaigns(campaign_ids):
        session = DBSession.get_db_session()
        budgets = session.query(Budget). \
            filter(Budget.campaign_id.in_(campaign_ids)).all()
        return budgets

    @staticmethod
    def update_budget_by_campaigns(campaign_ids, update_info):
        session = DBSession.get_db_session()
        session.query(Budget). \
            filter(Budget.campaign_id.in_(campaign_ids)).update(
                update_info, synchronize_session=False)
        session.commit()
        return 0
