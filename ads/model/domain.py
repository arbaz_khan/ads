# -*- coding: utf-8 -*-
# author: Cheng Lei(leichengeverstring.com)

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from base import Base
from session import DBSession


class domain(Base):
    __tablename__ = "domain"
    # auto increment id.
    id = Column(BigInteger, primary_key=True)
    domain = Column(String(256))

    # come from company(id).
    company_id = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    def set_domain_info(self, company_id, company_domain):
        try:
            session = DBSession.get_db_session()
            domain_obj = session.query(domain). \
                filter_by(domain=company_domain).first()

            if domain_obj is None:
                domain_obj = domain()
                domain_obj.domain = company_domain
                domain_obj.company_id = company_id
                current_time = datetime.datetime.utcnow()
                create_time = current_time.strftime('%Y-%m-%d %H:%M:%S')
                update_time = current_time.strftime('%Y-%m-%d %H:%M:%S')
                domain_obj.create_time = create_time
                domain_obj.update_time = update_time
                session.add(domain_obj)
                session.flush()
                id = domain_obj.id
                session.commit()
                session.close()
                return id
            else:
                return domain_obj.id
        except:
            raise
