import datetime
from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP
from base import Base
from session import DBSession
__author__ = 'boxia'


class Advertiser(Base):
    __tablename__ = "advertiser"
    id = Column(BigInteger, primary_key=True)
    name = Column(String(256))
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(advertiser_name):
        session = DBSession.get_db_session()
        try:
            advertiser = \
                session.query(Advertiser).filter(
                    Advertiser.name == advertiser_name).first()
            if advertiser is None:
                advertiser = Advertiser()
                advertiser.name = advertiser_name
                time = datetime.datetime.utcnow()
                advertiser.create_time = time
                advertiser.update_time = time
                session.add(advertiser)
                session.commit()
            return advertiser
        except:
            raise

    @staticmethod
    def get(advertiser_id):
        session = DBSession.get_db_session()
        advertiser = session.query(Advertiser).get(advertiser_id)
        return advertiser

    @staticmethod
    def get_advertiser_info():
        session = DBSession.get_db_session()
        try:
            advertiser = session.query(Advertiser).all()
            return advertiser
        except:
            raise

if __name__ == '__main__':
    advertiser = Advertiser.create('autodesk')
    print advertiser
