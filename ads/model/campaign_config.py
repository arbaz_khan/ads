

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from base import Base
from session import DBSession
__author__ = 'boxia'


class CampaignConfig(Base):
    __tablename__ = "campaign_config"
    id = Column(BigInteger, primary_key=True)
    united_campaign_id = Column(BigInteger)
    platform_id = Column(BigInteger)
    key = Column(String(256))
    value = Column(String(1024))

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    def set(self, united_campaign_id, platform_id, key, value):
        self.united_campaign_id = united_campaign_id
        self.platform_id = platform_id
        self.key = key
        self.value = value

        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def create(united_campaign_id, platform_id, key, value):
        session = DBSession.get_db_session()
        config = session.query(CampaignConfig).\
            filter_by(united_campaign_id=united_campaign_id,
                      platform_id=platform_id, key=key).\
            first()
        if config is None:
            config = CampaignConfig()
            config.set(united_campaign_id, platform_id, key, value)
            session.add(config)
        else:
            config.value = value
        session.commit()

    @staticmethod
    def get_configs(united_campaign_id, platform_id):
        session = DBSession.get_db_session()
        configs = session.query(CampaignConfig).\
            filter_by(united_campaign_id=united_campaign_id,
                      platform_id=platform_id).all()
        conf_dict = dict()
        for config in configs:
            conf_dict[config.key] = config.value

        return conf_dict
