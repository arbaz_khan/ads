__author__ = 'boxia'

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

import ConfigParser
import os
from base import Base

class DBSession(object):

    class __SessionCreator:

        @staticmethod
        def create_session():
            real_path = os.path.dirname(os.path.realpath(__file__))
            mysql_conf = real_path + '/../conf/mysql.conf'
            config = ConfigParser.ConfigParser()
            config.read(mysql_conf)        
            user = config.get('database', 'user')
            password = config.get('database', 'password')
            host = config.get('database', 'host')
            port = config.get('database', 'port')
            dbname = config.get('database', 'dbname')
            mysql_uri = "mysql+pymysql://%s:%s@%s:%s/%s?charset=utf8" %(user, password, host, port, dbname)
            engine = create_engine(mysql_uri, pool_recycle=3600, encoding='utf-8')
            Base.metadata.bind = engine
            session_factory = sessionmaker(bind=engine, autoflush=False)
            return scoped_session(session_factory)
        
    Session = __SessionCreator.create_session()

    @staticmethod
    def get_db_session():
        return DBSession.Session()
