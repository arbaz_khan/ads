# -*- coding: utf-8 -*-
# author: Cheng Lei(leichengeverstring.com)
# modified by boxia

from sqlalchemy.ext.declarative import declarative_base

class Base(object):
	def to_dict(self):
	    """
	    Jsonify the sql alchemy query result.
	    Notice: it can't handle relation for now, manually embed the relation if needed.
	    """
	    inst = self
	    cls = self.__class__
	    convert = dict()
	    # add your coversions for things like datetime's 
	    # and what-not that aren't serializable.
	    d = dict()
	    for c in cls.__table__.columns:
	        v = getattr(inst, c.name)
	        if c.type in convert.keys() and v is not None:
	            try:
	                d[c.name] = convert[c.type](v)
	            except:
	                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
	        elif v is None:
	            d[c.name] = str()
	        else:
	            d[c.name] = v
	    return d

	def __repr__(self):
		return "<{}({})>".format(
	        self.__class__.__name__,
	        ', '.join(
	            ["{}={}".format(k, repr(self.__dict__[k]))
	                for k in sorted(self.__dict__.keys())
	                if k[0] != '_']
	        )
	    )
		
# Initilize declarative base object globally.
Base = declarative_base(cls=Base)

