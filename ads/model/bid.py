# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP
from sqlalchemy import func

from base import Base
from session import DBSession


class Bid(Base):
    __tablename__ = "bid"
    id = Column(BigInteger, primary_key=True)
    platform_id = Column(BigInteger)
    company_score_map_id = Column(BigInteger)
    bid_type = Column(String(256))
    bid_amount = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(platform_id, bid_type, bid_amount, company_score_map_id):

        if not platform_id or not bid_type or \
           not bid_amount or not company_score_map_id:
            return -1

        session = DBSession.get_db_session()
        bid = session.query(Bid).\
            filter_by(platform_id=platform_id,
                      company_score_map_id=company_score_map_id).\
            first()

        if bid is None:
            bid = Bid()
            bid.platform_id = platform_id
            bid.bid_type = bid_type
            bid.bid_amount = bid_amount
            bid.company_score_map_id = company_score_map_id

            current_time = datetime.datetime.utcnow()
            bid.create_time = current_time
            bid.update_time = current_time
            session.add(bid)
            session.commit()

        return bid

    def set(self, platform_id, bid_type, bid_amount, company_score_map_id):
        self.platform_id = platform_id
        self.bid_type = bid_type
        self.bid_amount = bid_amount
        self.company_score_map_id = company_score_map_id

        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def get(platform_id, company_score_map_id):
        session = DBSession.get_db_session()
        bid = session.query(Bid).\
            filter_by(platform_id=platform_id,
                      company_score_map_id=company_score_map_id).first()
        return bid

    @staticmethod
    def update_bid_by_campaigns(campaign_ids, update_info):
        session = DBSession.get_db_session()
        session.query(Bid). \
            filter(Bid.campaign_id.in_(campaign_ids)).\
            update(update_info, synchronize_session=False)
        session.commit()
        return 0

    @staticmethod
    def update(update_keys, update_values):
        session = DBSession.get_db_session()
        bid = session.query(Bid). \
            filter_by(campaign_id=update_keys['campaign_id'],
                      adgroup_id=update_keys['adgroup_id'],
                      ad_id=update_keys['ad_id'],
                      product_id=update_keys['product_id']).first()

        # TODO:
        # Need to save this bid record.

        if 'name' in update_values and update_values['name']:
            bid.name = update_values['name']

        if 'bid_type' in update_values and update_values['bid_type']:
            bid.bid_type = update_values['bid_type']

        if 'bid_schema' in update_values and update_values['bid_schema']:
            bid.bid_schema = update_values['bid_schema']

        if 'bid_amount' in update_values and update_values['bid_amount']:
            bid.bid_amount = update_values['bid_amount']

        current_time = datetime.datetime.utcnow()
        bid.update_time = current_time.strftime('%Y-%m-%d %H:%M:%S')

        session.commit()

        return 0
