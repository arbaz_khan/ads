# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from session import DBSession
from base import Base

# modified by boxia


class Product(Base):
    __tablename__ = "product"
    id = Column(BigInteger, primary_key=True)
    name = Column(String(256))
    advertiser_id = Column(BigInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(product_name, advertiser_id):
        if not product_name or not advertiser_id:
            return None
        try:
            product = Product.get_product(product_name, advertiser_id)
            if product is None:
                product = Product()
                product.name = product_name
                product.advertiser_id = advertiser_id
                current_time = datetime.datetime.utcnow()
                product.create_time = current_time.strftime(
                    '%Y-%m-%d %H:%M:%S')
                product.update_time = current_time.strftime(
                    '%Y-%m-%d %H:%M:%S')
                session = DBSession.get_db_session()
                session.add(product)
                session.commit()
            return product
        except:
            raise

    @staticmethod
    def get_product(product_name, advertiser_id):
        session = DBSession.get_db_session()
        try:
            product = session.query(Product).\
                filter(Product.advertiser_id == advertiser_id).\
                filter(Product.name == product_name).first()
            return product
        except:
            raise

    @staticmethod
    def get(product_id):
        session = DBSession.get_db_session()
        product = session.query(Product).get(product_id)
        return product

    @staticmethod
    def get_product_by_advertiser_id(advertiser_id):
        session = DBSession.get_db_session()
        try:
            product = \
                session.query(Product).filter(
                    Product.advertiser_id == advertiser_id).all()
            return product
        except:
            raise


if __name__ == '__main__':
    print Product.create('office 365', 1).id
