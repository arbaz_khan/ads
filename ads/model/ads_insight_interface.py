from insight_by_campaign import InsightByCampaign
from insight_by_adgroup import InsightByAdgroup
from insight_by_ad import InsightByAd
from united_campaign import UnitedCampaign
from insight import Insight
from company_score_map import CompanyScoreMap
from campaign_company_map import CampaignCompanyMap
from adgroup import Adgroup
from company import Company
from session import DBSession
from sqlalchemy import func, distinct
from sqlalchemy.sql import label
from sqlalchemy.sql.expression import literal_column, text
from sqlalchemy.sql.expression import and_, or_
from ads.utils.date_util import DateUtil
from ads_prepare_interface import AdsPrepareInterface

__author__ = 'boxia'


class AdsInsightInterface():

    # param insights is a list of insight dict
    @staticmethod
    def create_insights(insights):
        session = DBSession.get_db_session()
        session.add_all(insights)
        session.commit()
        # We close here because we use multithread to get different granular
        # insight
        session.close()

    # TODO: need to refactor
    @staticmethod
    def get_interested_company_num(platform_id, campaign_id):
        session = DBSession.get_db_session()
        adgroup = session.query(Adgroup). \
            filter(Adgroup.campaign_id == campaign_id).all()

        if adgroup is None:
            return 0

        group_ids = []
        for one in adgroup:
            group_ids.append(one.adgroup_id)

        num = 0
        for id in group_ids:
            num = num + session. \
                query(InsightByAdgroup.platform_adgroup_name). \
                filter(InsightByAdgroup.platform_id == platform_id). \
                filter(InsightByAdgroup.platform_adgroup_id == id). \
                filter(InsightByAdgroup.clicks > 0).distinct().count()

        return num

    @staticmethod
    def get_found_company_num(platform_id, campaign_id):
        session = DBSession.get_db_session()
        adgroup = session.query(Adgroup). \
            filter(Adgroup.campaign_id == campaign_id).all()

        if adgroup is None:
            return 0

        group_ids = []
        for one in adgroup:
            group_ids.append(one.adgroup_id)

        num = 0
        for id in group_ids:
            num = num + session. \
                query(InsightByAdgroup.platform_adgroup_name). \
                filter(InsightByAdgroup.platform_id == platform_id). \
                filter(InsightByAdgroup.platform_adgroup_id == id). \
                filter(InsightByAdgroup.impressions > 0).distinct().count()

        return num

    @staticmethod
    def get_campaign_spend(campaign_id, start_time, end_time):
        try:
            session = DBSession.get_db_session()
            spends = session.query(label('spend', func.sum(Insight.spend))). \
                filter(Insight.united_campaign_id == int(campaign_id)). \
                filter(Insight.start_time >= start_time).\
                filter(Insight.end_time <= end_time)

            all_spend = 0.0
            for one in spends:
                if not one.spend:
                    continue
                all_spend = float(all_spend) + float(one.spend)

            return float(all_spend)
        except:
            raise

    @staticmethod
    def get_campaign_remaining(campaign_id, spend):
        try:
            session = DBSession.get_db_session()
            result = session.query(UnitedCampaign). \
                filter(UnitedCampaign.id == int(campaign_id)).first()

            return float(result.budget) - float(spend)
        except Exception:
            raise

    @staticmethod
    def get_campaign_cpc(campaign_id, spend, start_time, end_time):
        try:
            session = DBSession.get_db_session()
            clicks = session. \
                query(label('clicks', func.sum(Insight.clicks))). \
                filter(Insight.united_campaign_id == int(campaign_id)). \
                filter(Insight.start_time >= start_time).\
                filter(Insight.end_time <= end_time)

            all_click = 0
            for one in clicks:
                if not one.clicks:
                    continue
                all_click = all_click + int(one.clicks)

            if all_click > 0:
                return float(spend) / float(all_click)
            else:
                return 0.0

        except:
            raise

    @staticmethod
    def get_insights_by_campaign(platform_id, platform_campaign_id,
                                 start_time=None):
        insights_dict = dict()
        insights = None
        session = DBSession.get_db_session()
        if start_time is None:
            insights = session.query(InsightByCampaign).\
                filter(InsightByCampaign.platform_id == platform_id).\
                filter(InsightByCampaign.platform_campaign_id ==
                       platform_campaign_id).all()
        else:
            insights = session.query(InsightByCampaign).\
                filter(InsightByCampaign.platform_id == platform_id).\
                filter(InsightByCampaign.platform_campaign_id ==
                       platform_campaign_id).\
                filter(InsightByCampaign.start_time >= start_time).all()

        for item in insights:
            insight = dict()
            insight['campaign_id'] = item.platform_campaign_id
            insight['reach'] = item.reach
            insight['frequency'] = item.frequency
            insight['impressions'] = item.impressions
            insight['unique_impressions'] = item.unique_impressions
            insight['cpm'] = item.cpm
            insight['spend'] = item.spend
            insight['clicks'] = item.clicks
            insight['unique_clicks'] = item.unique_clicks
            insight['ctr'] = item.ctr
            insight['cpc'] = item.cpc
            insight['start_time'] = item.start_time.strftime(
                "%Y-%m-%d %H:%M:%S")
            insight['end_time'] = item.end_time.strftime("%Y-%m-%d %H:%M:%S")
            insights_dict[item.start_time.strftime("%Y-%m-%d")] = insight

        return insights_dict

    @staticmethod
    def get_insights_by_adgroup(platform_id, platform_adgroup_id,
                                start_time=None):
        insights_dict = dict()
        insights = None
        session = DBSession.get_db_session()
        if start_time is None:
            insights = session.query(InsightByAdgroup).\
                filter(InsightByAdgroup.platform_id == platform_id).\
                filter(
                    InsightByAdgroup.
                    platform_adgroup_id == platform_adgroup_id).all()
        else:
            insights = session.query(InsightByAdgroup).\
                filter(InsightByAdgroup.platform_id == platform_id).\
                filter(InsightByAdgroup.
                       platform_adgroup_id == platform_adgroup_id).\
                filter(InsightByAdgroup.start_time >= start_time).all()
        for item in insights:
            insight = dict()
            insight['adgroup_id'] = item.platform_adgroup_id
            insight['reach'] = item.reach
            insight['frequency'] = item.frequency
            insight['impressions'] = item.impressions
            insight['unique_impressions'] = item.unique_impressions
            insight['cpm'] = item.cpm
            insight['spend'] = item.spend
            insight['clicks'] = item.clicks
            insight['unique_clicks'] = item.unique_clicks
            insight['ctr'] = item.ctr
            insight['cpc'] = item.cpc
            insight['start_time'] = item.start_time.strftime(
                "%Y-%m-%d %H:%M:%S")
            insight['end_time'] = item.end_time.strftime("%Y-%m-%d %H:%M:%S")
            insights_dict[item.start_time.strftime("%Y-%m-%d")] = insight

        return insights_dict

    @staticmethod
    def get_insights_by_ad(platform_id, platform_ad_id, start_time=None):
        insights_dict = dict()
        insights = None
        session = DBSession.get_db_session()
        if start_time is None:
            insights = session.query(InsightByAd).\
                filter(InsightByAd.platform_id == platform_id).\
                filter(InsightByAd.platform_ad_id == platform_ad_id).all()
        else:
            insights = session.query(InsightByAd).\
                filter(InsightByAd.platform_id == platform_id).\
                filter(InsightByAd.platform_ad_id == platform_ad_id).\
                filter(InsightByAd.start_time >= start_time).all()
        for item in insights:
            insight = dict()
            insight['ad_id'] = item.platform_ad_id
            insight['reach'] = item.reach
            insight['frequency'] = item.frequency
            insight['impressions'] = item.impressions
            insight['unique_impressions'] = item.unique_impressions
            insight['cpm'] = item.cpm
            insight['spend'] = item.spend
            insight['clicks'] = item.clicks
            insight['unique_clicks'] = item.unique_clicks
            insight['ctr'] = item.ctr
            insight['cpc'] = item.cpc
            insight['start_time'] = item.start_time.strftime(
                "%Y-%m-%d %H:%M:%S")
            insight['end_time'] = item.end_time.strftime("%Y-%m-%d %H:%M:%S")
            insights_dict[item.start_time.strftime("%Y-%m-%d")] = insight

        return insights_dict

    @staticmethod
    def get_target_company_number(campaign_id):
        try:
            session = DBSession.get_db_session()
            number = session.query(CompanyScoreMap.company_id). \
                filter(CompanyScoreMap.
                       united_campaign_id == int(campaign_id)). \
                distinct().count()
            return number
        except Exception:
            raise

    @staticmethod
    def get_company_found_number(campaign_id):
        try:
            session = DBSession.get_db_session()
            number = session.query(CampaignCompanyMap.company_id). \
                filter(CampaignCompanyMap.
                       united_campaign_id == int(campaign_id)). \
                distinct().count()
            return number
        except:
            raise

    @staticmethod
    def get_insight_data_by_united_campaign_id(campaign_id, start_time,
                                               end_time):
        session = DBSession.get_db_session()
        query = session.query(func.sum(Insight.impressions).label('imp'),
                              func.sum(Insight.clicks).label('clicks'),
                              func.sum(Insight.conversion).label('conv'),
                              func.sum(Insight.spend).label('spend'),
                              UnitedCampaign.budget). \
            select_from(Insight). \
            join(UnitedCampaign,
                 UnitedCampaign.id == Insight.united_campaign_id). \
            filter(Insight.united_campaign_id == campaign_id).\
            filter(Insight.start_time >= start_time). \
            filter(Insight.end_time <= end_time)

        extended_query = session.query(query.subquery(),
                                       literal_column("clicks / imp").
                                       label('ctr'),
                                       literal_column("spend / (imp / 1000)").
                                       label('cpm'),
                                       literal_column("spend / clicks").
                                       label('cpc'))

        insight = extended_query.first()
        result = dict(zip(insight.keys(), insight))
        return result

    @staticmethod
    def get_company_interested_number(campaign_id, start_time, end_time):
        try:
            session = DBSession.get_db_session()
            number = session.query(Insight.company_id). \
                filter(Insight.united_campaign_id == campaign_id). \
                filter(Insight.clicks > 0). \
                filter(Insight.start_time >= start_time).\
                filter(Insight.end_time <= end_time).distinct().count()
            return number
        except:
            raise

    @staticmethod
    def get_people_impression_number(campaign_id, start_time, end_time):
        session = DBSession.get_db_session()
        res = session.query(label('total_impressions',
                                  func.sum(Insight.impressions))). \
            filter(Insight.united_campaign_id == int(campaign_id)). \
            filter(Insight.start_time >= start_time). \
            filter(Insight.end_time <= end_time).first()
        impr_cnt = res.total_impressions
        return int(impr_cnt)

    @staticmethod
    def get_people_clicks_number(campaign_id, start_time, end_time):
        session = DBSession.get_db_session()
        res = session.query(label('total_clicks',
                                  func.sum(Insight.clicks))). \
            filter(Insight.united_campaign_id == int(campaign_id)). \
            filter(Insight.start_time >= start_time). \
            filter(Insight.end_time <= end_time).first()

        return int(res.total_clicks)

    @staticmethod
    def get_company_converted_count(campaign_id, start_time, end_time):
        session = DBSession.get_db_session()
        result = session.\
            query(label('company_count',
                        func.count(distinct(Insight.company_id)))). \
            filter(Insight.united_campaign_id == int(campaign_id)). \
            filter(Insight.start_time >= start_time). \
            filter(Insight.end_time <= end_time). \
            filter(Insight.conversion > 0). \
            first()
        cnt = result.company_count
        return int(cnt)

    @staticmethod
    def get_people_converted_count(campaign_id, start_time, end_time):
        session = DBSession.get_db_session()
        result = session.query(label('conversions',
                                     func.sum(Insight.conversion))). \
            filter(Insight.united_campaign_id == int(campaign_id)). \
            filter(Insight.start_time >= start_time). \
            filter(Insight.end_time <= end_time). \
            first()
        cnt = result.conversions
        return int(cnt)

    @staticmethod
    def get_campaign_budget(campaign_id):
        try:
            session = DBSession.get_db_session()
            result = session.query(UnitedCampaign.budget). \
                filter(UnitedCampaign.id == campaign_id).first()
            return result.budget
        except:
            raise

    @staticmethod
    def get_es_company_score_map(company_id, campaign_id):
        try:
            session = DBSession.get_db_session()
            result = session.query(CompanyScoreMap). \
                filter(CompanyScoreMap.company_id == company_id). \
                filter(
                    CompanyScoreMap.united_campaign_id == campaign_id).first()
            return result
        except:
            raise

    @staticmethod
    def get_company_data_by_name(company_name):
        try:
            session = DBSession.get_db_session()
            result = session.query(Company). \
                filter(Company.name == company_name).first()
            return result
        except:
            raise

    @staticmethod
    def get_company_data_by_id(company_id):
        try:
            session = DBSession.get_db_session()
            result = session.query(Company). \
                filter(Company.id == company_id).first()
            return result
        except:
            raise

    @staticmethod
    def get_united_campaign_name_by_id(campaign_id):
        try:
            session = DBSession.get_db_session()
            result = session.query(UnitedCampaign.name). \
                filter(UnitedCampaign.id == campaign_id).first()
            return result.name
        except:
            raise

    @staticmethod
    def get_united_company_insight(camp_id, cur_page=1, page_size=10,
                                   sort_key="clicks", sort_dir="desc",
                                   search_key="",
                                   start_time="1970-01-01",
                                   end_time="2099-12-31"):
        if sort_key == "":
            raise ValueError("`sortKey` can not be empty")
        if sort_dir.upper() != "DESC" and sort_dir.upper() != "ASC":
            raise ValueError("`sortDir` should be either `DESC` or `ASC`")

        session = DBSession.get_db_session()
        c_id = int(camp_id)

        search_str = "%%%s%%" % search_key
        derived_keys = ['ctr', 'cpm', 'cpc']
        order_by_text = text("%s %s" % (sort_key, sort_dir))

        date_within_range_or_null = or_(and_(Insight.start_time >= start_time,
                                             Insight.end_time <= end_time),
                                        and_(Insight.start_time.is_(None),
                                             Insight.end_time.is_(None)))

        query = session.query(Company,
                              label('clicks', func.sum(Insight.clicks)),
                              label('impressions',
                                    func.sum(Insight.impressions)),
                              label('spend', func.sum(Insight.spend)),
                              label('reaches', func.sum(Insight.reach)),
                              label('conversions',
                                    func.sum(Insight.conversion)),
                              CompanyScoreMap.grade
                              ). \
            join(CampaignCompanyMap,
                 Company.id == CampaignCompanyMap.company_id). \
            join(CompanyScoreMap,
                 and_(CompanyScoreMap.united_campaign_id ==
                      CampaignCompanyMap.united_campaign_id,
                      CompanyScoreMap.company_id ==
                      CampaignCompanyMap.company_id)). \
            outerjoin(Insight, Insight.company_id == Company.id). \
            filter(CompanyScoreMap.united_campaign_id == c_id). \
            filter(Company.name.like(search_str)). \
            filter(date_within_range_or_null). \
            group_by(Insight.company_id)

        if sort_key not in derived_keys:
            query = query.order_by(order_by_text)

        # (1)
        extended_query = session. \
            query(query.subquery(),
                  literal_column("clicks / impressions").label('ctr'),
                  literal_column("spend / impressions * 1000").label('cpm'),
                  literal_column("spend / clicks").label('cpc'))

        # (2)
        if sort_key in derived_keys:
            extended_query = extended_query.order_by(order_by_text)

        # (3)
        extended_query = extended_query.limit(page_size). \
            offset((cur_page - 1) * page_size)

        print(extended_query.statement.
              compile(compile_kwargs={"literal_binds": True}))

        item_cnt = session.query(Company). \
            join(CampaignCompanyMap,
                 Company.id == CampaignCompanyMap.company_id). \
            filter(Company.name.like(search_str)).count()
        results = [dict(zip(row.keys(), row)) for row in extended_query]

        return {
            "list": results,
            "count": item_cnt,
            "currentPage": cur_page
        }

    @staticmethod
    def get_company_campaign_daily_metrics(company_id, campaign_id):
        session = DBSession.get_db_session()
        rows = session.query(Insight).\
            filter(Insight.united_campaign_id == int(campaign_id)).\
            filter(Insight.company_id == int(company_id))

        daily_metrics = []
        for row in rows:
            metrics = {
                "clicks": row.clicks,
                "impressions": row.impressions,
                "start_time": row.start_time
            }
            daily_metrics.append(metrics)

        return daily_metrics

    @staticmethod
    def get_company_campaign_aggregated_metrics(company_id, campaign_id):
        session = DBSession.get_db_session()
        row = session.query(
            label('clicks', func.sum(Insight.clicks)),
            label('impressions', func.sum(Insight.impressions)),
            label('spend', func.sum(Insight.spend)),
            label('days', func.count(Insight.id))
        ).\
            filter(Insight.united_campaign_id == int(campaign_id)).\
            filter(Insight.company_id == int(company_id)).first()

        metrics = {
            "clicks": int(row.clicks),
            "impressions": int(row.impressions),
            "spend": float(row.spend),
            "days": int(row.days)
        }

        return metrics

    # start_time: '2015-04-01'
    # end_time: '2015-04-02'
    @staticmethod
    def get_facebook_insights(ucampaign_id, start_time, end_time):
        platform_id = AdsPrepareInterface().get_platform_id('facebook')
        day_start = DateUtil.validate(start_time)
        day_end = DateUtil.validate(end_time)
        session = DBSession.get_db_session()
        insight_lst = session.query(InsightByAdgroup, CampaignCompanyMap).\
            join(CampaignCompanyMap,
                 InsightByAdgroup.
                 adgroup_id == CampaignCompanyMap.adgroup_id).\
            filter(InsightByAdgroup.start_time >= day_start,
                   InsightByAdgroup.end_time <= day_end,
                   InsightByAdgroup.platform_id == platform_id,
                   CampaignCompanyMap.united_campaign_id == ucampaign_id).\
            all()
        insights = dict()
        for insight_by_adgroup, campaign_company_map in insight_lst:
            insight = Insight()
            insight.united_campaign_id = campaign_company_map.\
                united_campaign_id
            insight.company_id = campaign_company_map.company_id
            insight.reach = insight_by_adgroup.reach
            insight.frequency = insight_by_adgroup.frequency
            insight.impressions = insight_by_adgroup.impressions
            insight.unique_impressions = insight_by_adgroup.unique_impressions
            insight.cpm = insight_by_adgroup.cpm
            insight.spend = insight_by_adgroup.spend
            insight.clicks = insight_by_adgroup.clicks
            insight.unique_clicks = insight_by_adgroup.unique_clicks
            insight.ctr = insight_by_adgroup.ctr
            insight.cpc = insight_by_adgroup.cpc
            insight.conversion = insight_by_adgroup.conversion
            insight.start_time = insight_by_adgroup.start_time
            insight.end_time = insight_by_adgroup.end_time
            insights[insight.company_id] = insight
        session.close()
        return insights

    # start_time: '2015-04-01'
    # end_time: '2015-04-02'
    @staticmethod
    def get_google_insights(ucampaign_id, start_time, end_time):
        platform_id = AdsPrepareInterface().get_platform_id('google')
        day_start = DateUtil.validate(start_time)
        day_end = DateUtil.validate(end_time)
        session = DBSession.get_db_session()
        insight_lst = session.query(InsightByCampaign, CampaignCompanyMap).\
            join(CampaignCompanyMap,
                 InsightByCampaign.
                 campaign_id == CampaignCompanyMap.campaign_id).\
            filter(InsightByCampaign.start_time >= day_start,
                   InsightByCampaign.end_time <= day_end,
                   InsightByCampaign.platform_id == platform_id,
                   CampaignCompanyMap.united_campaign_id == ucampaign_id).\
            all()
        insights = dict()
        for insight_by_campaign, campaign_company_map in insight_lst:
            insight = Insight()
            insight.\
                united_campaign_id = campaign_company_map.united_campaign_id
            insight.company_id = campaign_company_map.company_id
            insight.reach = insight_by_campaign.reach
            insight.frequency = insight_by_campaign.frequency
            insight.impressions = insight_by_campaign.impressions
            insight.unique_impressions = insight_by_campaign.unique_impressions
            insight.cpm = insight_by_campaign.cpm
            insight.spend = insight_by_campaign.spend
            insight.clicks = insight_by_campaign.clicks
            insight.unique_clicks = insight_by_campaign.unique_clicks
            insight.ctr = insight_by_campaign.ctr
            insight.cpc = insight_by_campaign.cpc
            insight.conversion = insight_by_campaign.conversion
            insight.start_time = insight_by_campaign.start_time
            insight.end_time = insight_by_campaign.end_time
            insights[insight.company_id] = insight
        session.close()
        return insights

    @staticmethod
    def get_campaigns_spend(campaign_ids):
        session = DBSession.get_db_session()
        spent_lst = session.query(InsightByCampaign.spend).\
            filter(InsightByCampaign.campaign_id.in_(campaign_ids)).all()
        cost = 0.0
        for spent, in spent_lst:
            cost = cost + spent
        session.close()
        return cost
