# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)


from base import Base

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from session import DBSession


class Ip(Base):
    __tablename__ = "ip"
    id = Column(BigInteger, primary_key=True)
    ip_start = Column(String(256))
    ip_end = Column(String(256))

    company_id = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(company_id, ip_start, ip_end):
        if not company_id:
            return None

        session = DBSession.get_db_session()
        ip_obj = session.query(Ip).\
            filter_by(company_id=company_id,
                      ip_start=ip_start,
                      ip_end=ip_end).first()

        if ip_obj is None:
            ip_obj = Ip()
            ip_obj.company_id = company_id
            ip_obj.ip_start = ip_start
            ip_obj.ip_end = ip_end
            current_time = datetime.datetime.utcnow()
            ip_obj.create_time = current_time
            ip_obj.update_time = current_time
            session.add(ip_obj)
            session.commit()

        return ip_obj

    @staticmethod
    def get_ip(company_id):
        ip_list = list()
        if not company_id:
            return ip_list
        session = DBSession.get_db_session()
        ip_objs = session.query(Ip).filter_by(company_id=company_id).all()
        for ip_obj in ip_objs:
            ip = dict()
            ip['ip_start'] = ip_obj.ip_start
            ip['ip_end'] = ip_obj.ip_end
            ip_list.append(ip)
        return ip_list
