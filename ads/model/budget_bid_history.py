# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP

from base import Base


class BudgetBidHistory(Base):
    __tablename__ = "budget_bid_history"
    id = Column(BigInteger, primary_key=True)
    platform_id = Column(BigInteger)
    budget_type = Column(String(256))
    budget_amount = Column(BigInteger)
    bid_type = Column(String(256))
    bid_amount = Column(BigInteger)
    company_score_map_id = Column(BigInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    def set(self, platform_id, company_score_map_id,
            budget_type, budget_amount, bid_type, bid_amount):
        self.platform_id = platform_id
        self.company_score_map_id = company_score_map_id
        self.budget_type = budget_type
        self.budget_amount = budget_amount
        self.bid_type = bid_type
        self.bid_amount = bid_amount
