import datetime
from sqlalchemy import Column, String, BigInteger, DateTime, TIMESTAMP
from base import Base
from session import DBSession

__author__ = 'boxia'


class Platform(Base):
    __tablename__ = 'platform'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(256))
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def get_platform_id(name):
        session = DBSession.get_db_session()
        try:
            return session.query(Platform.id).filter(
                Platform.name == name).one().id
        except:
            raise

if __name__ == '__main__':
    print Platform.get_platform_id('google')
    print Platform.get_platform_id('facebook')
