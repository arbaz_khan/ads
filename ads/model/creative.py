
import datetime

from sqlalchemy import Column, String, BigInteger, DateTime, SmallInteger
from sqlalchemy import TIMESTAMP

from base import Base
from session import DBSession
__author__ = 'boxia'


class Creative(Base):
    __tablename__ = "creative"
    id = Column(BigInteger, primary_key=True)
    united_campaign_id = Column(BigInteger)
    platform_id = Column(BigInteger)
    platform_creative_id = Column(BigInteger)
    landing_page_url = Column(String(1024))
    headline = Column(String(256))
    description_1 = Column(String(256))
    description_2 = Column(String(256))
    image_url = Column(String(1024))
    status = Column(SmallInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(united_campaign_id, platform_id, landing_page_url, headline,
               description_1, description_2, image_url):
        try:
            session = DBSession.get_db_session()
            creative = session.query(Creative). \
                filter_by(united_campaign_id=united_campaign_id,
                          platform_id=platform_id,
                          image_url=image_url,
                          status=1).\
                first()

            if creative is not None:
                # disable current creative
                creative.status = 0

            # create new creative anyway
            new_creative = Creative()
            new_creative.set(united_campaign_id, platform_id, 0,
                             landing_page_url, headline,
                             description_1, description_2, image_url, 1)
            session.add(new_creative)
            session.commit()

            return new_creative
        except:
            raise

    def set(self, united_campaign_id, platform_id, platform_creative_id,
            landing_page_url, headline, description_1, description_2,
            image_url, status):
        self.united_campaign_id = united_campaign_id
        self.platform_id = platform_id
        self.platform_creative_id = platform_creative_id
        self.landing_page_url = landing_page_url
        self.headline = headline
        self.description_1 = description_1
        self.description_2 = description_2
        self.image_url = image_url
        self.status = status
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def update(creative_id, platform_creative_id):
        if not creative_id or not platform_creative_id:
            return -1

        session = DBSession.get_db_session()
        creative = session.query(Creative).get(creative_id)

        if creative is not None:
            creative.platform_creative_id = platform_creative_id
            session.commit()

    @staticmethod
    def get_creatives(united_campaign_id, platform_id):
        try:
            session = DBSession.get_db_session()
            creatives = session.query(Creative). \
                filter_by(united_campaign_id=united_campaign_id,
                          platform_id=platform_id, status=1).all()

            return creatives
        except:
            raise
