# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, SmallInteger, DateTime
from sqlalchemy import TIMESTAMP

from base import Base
from session import DBSession


class Adgroup(Base):
    __tablename__ = "adgroup"
    id = Column(BigInteger, primary_key=True)
    name = Column(String(256))
    status = Column(SmallInteger)

    campaign_id = Column(BigInteger)
    adgroup_id = Column(String(256))
    platform_id = Column(BigInteger)

    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def get_adgroups_by_campaign(campaign_ids):
        if not campaign_ids:
            return None

        session = DBSession.get_db_session()
        return session.query(Adgroup). \
            filter(Adgroup.campaign_id.in_(campaign_ids)).all()

    @staticmethod
    def create(campaign_id, adgroup_id, name, status, platform_id):
        if not campaign_id or not adgroup_id:
            return None

        session = DBSession.get_db_session()
        ag_obj = session.query(Adgroup).\
            filter_by(campaign_id=campaign_id,
                      adgroup_id=adgroup_id).\
            first()
        if ag_obj is None:
            ag_obj = Adgroup()
            ag_obj.adgroup_id = adgroup_id
            ag_obj.status = status
            ag_obj.campaign_id = campaign_id
            ag_obj.name = name
            ag_obj.platform_id = platform_id
            current_time = datetime.datetime.utcnow()
            ag_obj.create_time = current_time
            ag_obj.update_time = current_time
            session = DBSession.get_db_session()
            session.add(ag_obj)
            session.commit()

        return ag_obj

    # adgroup_id is our system adgroup id
    @staticmethod
    def update(adgroup_id, name, status):
        try:
            session = DBSession.get_db_session()
            ag_obj = session.query(Adgroup).get(adgroup_id)

            ag_obj.name = name
            ag_obj.status = status
            session.commit()
            return 0
        except:
            raise

    @staticmethod
    def get_adgroup_by_platform_adgroup(platform_id, platform_adgroup_id):
        try:
            session = DBSession.get_db_session()
            adgroup = session.query(Adgroup).\
                filter_by(platform_id=platform_id).\
                filter_by(adgroup_id=platform_adgroup_id).\
                first()
            return adgroup
        except:
            raise
