# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by boxia

import datetime

from sqlalchemy import Column, String, Integer, BigInteger, DateTime, Text
from sqlalchemy import TIMESTAMP

from base import Base
from session import DBSession


class Company(Base):
    __tablename__ = "company"
    id = Column(BigInteger, primary_key=True)
    name = Column(String(256))
    domain = Column(String(256))
    industry = Column(String(256))
    revenue = Column(String(256))

    # if there are multiple addresses for a company, populate the HQ
    # here. These columns
    # are for display purpose only. To do geo-targeting, refer to the
    # relation between
    # company and address tables.
    address_street = Column(String(1024))
    address_city = Column(String(256))
    address_state = Column(String(256))
    address_country = Column(String(256))

    description = Column(Text)
    employee_size = Column(Integer)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(company_name, domain, industry, employee_size, revenue="",
               address_street="", address_city="", address_state="",
               address_country=""):
        # Select from 'company' using name,
        # if can get the company,
        # return this company id;
        # if not, should insert
        # and get a new company id.
        session = DBSession.get_db_session()
        company = session.query(Company).\
            filter_by(name=company_name, domain=domain, industry=industry,
                      employee_size=employee_size).first()

        if company is None:
            company = Company()

            company.name = company_name
            company.domain = domain
            company.industry = industry
            company.employee_size = employee_size
            company.revenue = revenue

            current_time = datetime.datetime.utcnow()
            company.create_time = current_time
            company.update_time = current_time
            company.address_street = address_street
            company.address_city = address_city
            company.address_state = address_state
            company.address_country = address_country

            session.add(company)
            session.commit()

        return company

    # DEPRECATED
    def set(self, company_name, domain, industry, employee_size, revenue):
        self.name = company_name
        self.domain = domain
        self.industry = industry
        self.employee_size = employee_size
        self.revenue = revenue
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time
