__author__ = 'boxia'

from sqlalchemy import Column, String, Integer, BigInteger, Float, DateTime
from session import Base

class InsightByAdgroup(Base):
    __tablename__ = 'insight_by_adgroup'

    id = Column(BigInteger, primary_key=True)
    platform_id           = Column(BigInteger)
    account_id            = Column(BigInteger)
    adgroup_id            = Column(BigInteger)
    adgroup_name          = Column(String(256))
    platform_adgroup_id   = Column(BigInteger)
    reach                 = Column(BigInteger)
    frequency             = Column(Float)
    impressions           = Column(BigInteger)
    unique_impressions    = Column(BigInteger)
    cpm                   = Column(Float)
    spend                 = Column(Float)
    clicks                = Column(BigInteger)
    unique_clicks         = Column(BigInteger)
    ctr                   = Column(Float)
    cpc                   = Column(Float)
    conversion            = Column(BigInteger)
    start_time            = Column(DateTime)
    end_time              = Column(DateTime)
    retrieve_time         = Column(DateTime)

