# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified: Bo Xia(boxia@everstring.com)

from ads.utils.ads_exception import AdsExceptionConst
from ads.utils.ads_exception import AdsException
from ads.utils.log import Logger
from campaign import Campaign
from adgroup import Adgroup
from account import Account
from ad import Ad
from bid import Bid
from budget import Budget
from advertiser import Advertiser
from product import Product
from campaign_company_map import CampaignCompanyMap
from session import DBSession
from budget_bid_history import BudgetBidHistory
from campaign_config import CampaignConfig
from united_campaign import UnitedCampaign
import traceback


class AdsInterface():

    # campaign_id is campaign id on other platform.
    def set_campaign(self, account_id, campaign_id,
                     start_time, end_time, platform_id, name='', status=0):

        if not account_id or not campaign_id:
            Logger.get_logger().warning(
                " input account id or campaign_id is empty ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if not name.strip():
            Logger.get_logger().warning(" input name is empty ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        campaign = Campaign.create(account_id, campaign_id, start_time,
                                   end_time, name, status, platform_id)
        return campaign.id

    def get_campaign_by_account(self, account_id):
        campaign_idnames = list()
        if not account_id:
            return campaign_idnames

        campaigns = Campaign.get_campaigns_by_account(account_id)
        for campaign in campaigns:
            campaign_idnames.append(
                (campaign.id, campaign.campaign_id, campaign.name))

        return campaign_idnames

    @staticmethod
    def get_campaign_by_platform_campaign(platform_id, platform_campaign_id):
        if not platform_id or not platform_campaign_id:
            Logger.get_logger().warning(" input param is empty or error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        try:
            return Campaign.get_campaign_by_platform_campaign(
                platform_id, platform_campaign_id)
        except:
            raise

    def get_campaigns_by_account(self, account_id):
        campaign_ids = list()
        if not account_id:
            return campaign_ids

        campaigns = Campaign.get_campaigns_by_account(account_id)
        return campaigns

    def get_campaigns_by_ucampaign(self, ucampaign_id, platform_id):
        session = DBSession.get_db_session()
        campaigns = session.query(Campaign).join(
            CampaignCompanyMap,
            CampaignCompanyMap.campaign_id == Campaign.id).\
            filter(CampaignCompanyMap.united_campaign_id == ucampaign_id).\
            filter(Campaign.platform_id == platform_id).all()
        return campaigns

    def update_campaign(self, campaign_id, name, status):
        return Campaign.update(campaign_id, name, status)

    # adgroup_id is that on 3rd platform id
    def set_adgroup(self, ucampaign_id, campaign_id, adgroup_id, name,
                    company_id, platform_id, status=0):
        if not campaign_id or not adgroup_id or \
                not ucampaign_id or not company_id:
            Logger.get_logger().warning(" input param is empty or error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if isinstance(name, str) is False:
            Logger.get_logger().warning(" input name is empty ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        adgroup = Adgroup.create(campaign_id, adgroup_id, name,
                                 status, platform_id)
        CampaignCompanyMap.create(campaign_id, adgroup.id,
                                  ucampaign_id, company_id)
        return adgroup.id

    @staticmethod
    def get_adgroup_by_platform_adgroup(platform_id, platform_adgroup_id):
        if not platform_id or not platform_adgroup_id:
            Logger.get_logger().warning(" input param is empty or error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        try:
            return Adgroup.get_adgroup_by_platform_adgroup(platform_id,
                                                           platform_adgroup_id)
        except:
            raise

    def get_adgroup_ids_by_campaign(self, campaign_ids):
        adgroup_ids = list()
        if not campaign_ids:
            return adgroup_ids

        adgroups = Adgroup.get_adgroups_by_campaign(campaign_ids)
        for adgroup in adgroups:
            adgroup_ids.append((adgroup.id, adgroup.adgroup_id))
        return adgroup_ids

    # adgroup_id is our system adgroup id
    def update_adgroup(self, adgroup_id, name, status):
        return Adgroup.update(adgroup_id, name, status)

    # ad_id is our those platform ad id
    def set_ad(self, adgroup_id, ad_id, platform_id, name='',
               status=0, creative_id=-1):
        if not adgroup_id or not ad_id or not name.strip():
            Logger.get_logger().warning(" input param is empty or error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        ad = Ad.create(ad_id, adgroup_id, name, status,
                       creative_id, platform_id)
        return ad.id

    @staticmethod
    def get_ad_by_platform_ad(platform_id, platform_ad_id):
        if not platform_id or not platform_ad_id:
            Logger.get_logger().warning(" input param is empty or error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        try:
            return Ad.get_ad_by_platform_ad(platform_id, platform_ad_id)
        except:
            raise

    # ad_id is our system ad id
    def update_ad(self, ad_id, name, status):
        if not ad_id:
            Logger.get_logger().warning(" input param ad_id empty")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return Ad.update(ad_id, name, status)

    def set_budget_bid_batch(self, platform_id, lead_ids, budget_bid_info):
        if not platform_id or isinstance(lead_ids, list) is False or \
                isinstance(budget_bid_info, dict) is False:
            Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if 'budget_type' not in budget_bid_info or \
           'budget_amount' not in budget_bid_info or \
           'bid_type' not in budget_bid_info or \
           'bid_amount' not in budget_bid_info:

            Logger.get_logger.warning(" input param missing")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        budget_lst = list()
        bid_lst = list()
        for lead_id in lead_ids:
            budget = Budget()
            budget.set(platform_id, budget_bid_info.get('budget_type'),
                       budget_bid_info.get('budget_amount'), lead_id)
            budget_lst.append(budget)

            bid = Bid()
            bid.set(platform_id, budget_bid_info.get('bid_type'),
                    budget_bid_info.get('bid_amount'), lead_id)
            bid_lst.append(bid)

        session = DBSession.get_db_session()
        if len(budget_lst) > 0:
            if Budget.get(platform_id,
                          budget_lst[0].company_score_map_id) is None:
                session.add_all(budget_lst)
        if len(bid_lst) > 0:
            if Bid.get(platform_id, bid_lst[0].company_score_map_id) is None:
                session.add_all(bid_lst)

        session.commit()

    def set_budget_bid(self, platform_id, company_score_map_id,
                       budget_bid_info):
        if not platform_id or not company_score_map_id or \
                isinstance(budget_bid_info, dict) is False:
            Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if 'budget_type' not in budget_bid_info or \
           'budget_amount' not in budget_bid_info or \
           'bid_type' not in budget_bid_info or \
           'bid_amount' not in budget_bid_info:

            Logger.get_logger.warning(" input param missing")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        budget_type = budget_bid_info.get('budget_type')
        budget_amount = budget_bid_info.get('budget_amount')
        bid_type = budget_bid_info.get('bid_type')
        bid_amount = budget_bid_info.get('bid_amount')

        try:
            Budget.create(platform_id, budget_type, budget_amount,
                          company_score_map_id)
            Bid.create(platform_id, bid_type, bid_amount, company_score_map_id)
        except Exception as e:
            Logger.get_logger.warning(e.message)
            traceback.print_exc()
            raise AdsException(AdsExceptionConst.MYSQL_ERR)

    def update_budget_bid(self, platform_id, company_score_map_id,
                          budget_bid_info):
        if not platform_id or not company_score_map_id or \
                isinstance(budget_bid_info, dict) is False:
            Logger.get_logger.warning(" input param error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        session = DBSession.get_db_session()
        budget = session.query(Budget).filter_by(
            platform_id=platform_id,
            company_score_map_id=company_score_map_id).first()
        bid = session.query(Bid).filter_by(
            platform_id=platform_id,
            company_score_map_id=company_score_map_id).first()
        if budget is None or bid is None:
            Logger.get_logger.warning("No item found for update")
            return

        need_history = False
        history_budget_type = budget.budget_type
        history_budget_amount = budget.budget_amount
        history_bid_type = bid.bid_type
        history_bid_amount = bid.bid_amount

        if 'budget_type' in budget_bid_info:
            budget.budget_type = budget_bid_info.get('budget_type')
        if 'budget_amount' in budget_bid_info:
            budget.budget_amount = budget_bid_info.get('budget_amount')
        if 'bid_type' in budget_bid_info:
            bid.bid_type = budget_bid_info.get('bid_type')
        if 'bid_amount' in budget_bid_info:
            bid.bid_amount = budget_bid_info.get('bid_amount')

        for obj in session.dirty:
            need_history = need_history or session.is_modified(obj)

        try:
            if need_history:
                budget_bid_history = BudgetBidHistory()
                budget_bid_history.set(platform_id,
                                       company_score_map_id,
                                       history_budget_type,
                                       history_budget_amount,
                                       history_bid_type, history_bid_amount)
                session.add(budget_bid_history)

            session.commit()

        except Exception as e:
            session.rollback()
            Logger.get_logger().warning(e.message)
            traceback.print_exc()
            raise AdsException(AdsExceptionConst.MYSQL_ERR)

    def get_budget(self, platform_id, company_score_map_id):
        return Budget.get(platform_id, company_score_map_id)

    def get_bid(self, platform_id, company_score_map_id):
        return Bid(platform_id, company_score_map_id)

    def set_campaign_config(self, ucampaign_id, platform_id, key, value):
        if not ucampaign_id or not key or not value:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        CampaignConfig.create(ucampaign_id, platform_id, key, value)

    def get_campaign_configs(self, ucampaign_id, platform_id):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        return CampaignConfig.get_configs(ucampaign_id, platform_id)

    def set_united_campaign(self, product_id, ucampaign_name,
                            budget, status=0,
                            start_time=None, end_time=None):
        if not ucampaign_name or not product_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return UnitedCampaign.create(product_id, ucampaign_name,
                                     budget, status,
                                     start_time, end_time)

    def update_ucampaign_status(self, ucampaign_id, status):
        session = DBSession.get_db_session()
        ucampaign = session.query(UnitedCampaign).get(ucampaign_id)
        ucampaign.status = status
        session.commit()

    def get_united_campaign_by_id(self, ucampaign_id):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return UnitedCampaign.get(ucampaign_id)

    def get_platform_campaign_company_(self, united_campaign_id,
                                       company_id, platform_id):
        session = DBSession.get_db_session()
        return session.query(CampaignCompanyMap).\
            join(Campaign, Campaign.id == CampaignCompanyMap.campaign_id).\
            filter(Campaign.platform_id == platform_id). \
            filter(CampaignCompanyMap.company_id == company_id). \
            filter(
                CampaignCompanyMap.united_campaign_id == united_campaign_id
            ).all()

    def get_all_united_campaign(self):
        session = DBSession.get_db_session()
        return session.query(UnitedCampaign).all()

    def get_all_united_campaign_ids(self):
        session = DBSession.get_db_session()
        ucampaigns = session.query(UnitedCampaign.id).all()
        ucampaign_ids = list()
        for ucampaign in ucampaigns:
            ucampaign_ids.append(ucampaign.id)
        return ucampaign_ids

    @staticmethod
    def get_advertiser_info():
        try:
            return Advertiser.get_advertiser_info()
        except:
            # raise AdsException(AdsExceptionConst.PARAM)
            raise

    @staticmethod
    def get_united_campaign_list(search_key=None):
        try:
            return UnitedCampaign.get_all_united_campaign_core_data(search_key)
        except:
            raise

    @staticmethod
    def get_product_list_by_advertiser_id(a_id):
        try:
            return Product.get_product_by_advertiser_id(a_id)
        except:
            raise

    @staticmethod
    def get_campaign_ids_by_product_id(p_id):
        try:
            return CampaignCompanyMap.get_campaign_ids_by_product_id(p_id)
        except:
            raise

    @staticmethod
    def get_campaign_info_by_campaign_id(c_id):
        try:
            return Campaign.get_campaign_info_by_campaign_id(c_id)
        except:
            raise

    @staticmethod
    def get_platform_id_by_account_id(a_id):
        try:
            return Account.get_platform_id_by_account_id(a_id)
        except:
            raise

    def update_budget_id(self, id, budget_id):
        try:
            Budget.update_budget_id(id, budget_id)
        except:
            raise
