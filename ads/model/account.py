# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

import datetime

from sqlalchemy import Column, String, BigInteger, SmallInteger, DateTime
from sqlalchemy import TIMESTAMP

from base import Base
from session import DBSession


# modified by boxia
class Account(Base):
    __tablename__ = "account"

    id = Column(BigInteger, primary_key=True)
    # other platform account id not our system account id
    account_id = Column(BigInteger)
    platform_id = Column(BigInteger)
    status = Column(SmallInteger)
    name = Column(String(256))
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    # param account_id is platform account id
    @staticmethod
    def create(platform_id, account_id, account_name):
        if not platform_id or not account_id:
            return None
        account_id = Account.strip_accont_prefix(account_id)
        account = Account.get_account(platform_id, account_id)

        if account is None:
            account = Account()
            account.set_account(platform_id, account_id, account_name)
            session = DBSession.get_db_session()
            session.add(account)
            session.commit()
        return account

    def set_account(self, platform_id, account_id, account_name):
        self.account_id = account_id
        self.platform_id = platform_id
        self.status = 0
        self.name = account_name
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time

    @staticmethod
    def get_account(platform_id, account_id):
        account_id = Account.strip_account_prefix(account_id)
        session = DBSession.get_db_session()
        account = session.query(Account).\
            filter(Account.platform_id == platform_id).\
            filter(Account.account_id == account_id).\
            first()
        return account

    @staticmethod
    def strip_account_prefix(account_id):
        account_id = str(account_id)
        if account_id.startswith('act_'):
            account_id = account_id[4:]
        return int(account_id)

    @staticmethod
    def get_platform_id_by_account_id(a_id):
        try:
            session = DBSession.get_db_session()
            account = session.query(Account).get(a_id)
            if account is None:
                return None
            return account.platform_id
        except:
            raise


if __name__ == '__main__':
    account = Account.create(2, 'act_708796745899561',
                             'Everstring Internal Ad account')
    print account.id, account.account_id, account.name
