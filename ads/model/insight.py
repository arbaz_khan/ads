__author__ = 'boxia'

from sqlalchemy import Column, String, BigInteger, DateTime, Float, SmallInteger

from base import Base

class Insight(Base):
    __tablename__ = "insight"

    id = Column(BigInteger, primary_key=True)
    united_campaign_id  = Column(BigInteger)
    company_id          = Column(BigInteger)
    reach               = Column(BigInteger)
    frequency           = Column(Float)
    impressions         = Column(BigInteger)
    unique_impressions  = Column(BigInteger)
    cpm                 = Column(Float)
    spend               = Column(Float)
    clicks              = Column(BigInteger)
    unique_clicks       = Column(BigInteger)
    ctr                 = Column(Float)
    cpc                 = Column(Float)
    conversion          = Column(BigInteger)
    start_time          = Column(DateTime)
    end_time            = Column(DateTime)
    retrieve_time       = Column(DateTime)
