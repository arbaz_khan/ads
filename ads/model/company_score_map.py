# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by boxia

import datetime

from sqlalchemy import Column, String, Integer, BigInteger, DateTime, TIMESTAMP
from sqlalchemy import SmallInteger

from base import Base
from session import DBSession


class CompanyScoreMap(Base):
    __tablename__ = "company_score_map"
    id = Column(BigInteger, primary_key=True)
    united_campaign_id = Column(Integer)
    company_id = Column(BigInteger)
    # TODO: change this field to 'fit_score' for clarity.
    score = Column(SmallInteger)
    engagement_score = Column(SmallInteger)
    intent_score = Column(SmallInteger)
    grade = Column(String(4))
    # control group is 0 when lead not in control group,
    # is 1 when lead is in control group but need to be targeted
    # is 2 when lead is in control group but not need to be targeted
    control_group = Column(SmallInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    # modified by boxia
    @staticmethod
    def create(united_campaign_id, target_company_map):
        session = DBSession.get_db_session()
        company_score = session.query(CompanyScoreMap).filter_by(
            united_campaign_id=united_campaign_id,
            company_id=target_company_map['company_id']).first()

        if company_score is None:
            company_score = CompanyScoreMap()
            company_score.set(united_campaign_id, target_company_map)
            session.add(company_score)
        else:
            company_score.grade = target_company_map.get('grade')
            company_score.score = target_company_map.get('score', -1)
            company_score.engagement_score = target_company_map.get(
                'engagement_score', -1)
            company_score.intent_score = target_company_map.get(
                'intent_score', -1)
            company_score.control_group = target_company_map.get(
                'control_group')
            company_score.update_time = datetime.datetime.utcnow()
        session.commit()
        return company_score.id

    def set(self, united_campaign_id, target_company_map):
        self.united_campaign_id = united_campaign_id
        self.grade = target_company_map.get('grade')
        self.score = target_company_map.get('score', -1)
        self.engagement_score = target_company_map.get('engagement_score', -1)
        self.intent_score = target_company_map.get('intent_score', -1)
        self.company_id = target_company_map.get('company_id')
        self.control_group = target_company_map.get('control_group')
        current_time = datetime.datetime.utcnow()
        self.create_time = current_time
        self.update_time = current_time
