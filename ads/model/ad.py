# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modified by Bo Xia

import datetime

from sqlalchemy import Column, String, BigInteger, SmallInteger, DateTime
from sqlalchemy import TIMESTAMP

from base import Base
from session import DBSession


class Ad(Base):
    __tablename__ = "ad"
    id = Column(BigInteger, primary_key=True)
    ad_id = Column(BigInteger)
    adgroup_id = Column(BigInteger)
    name = Column(String(256))
    status = Column(SmallInteger)
    creative_id = Column(BigInteger)
    platform_id = Column(BigInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    # ad_id is the ad id on those platforms
    @staticmethod
    def create(ad_id, adgroup_id, name, status, creative_id, platform_id):
        if not ad_id or not adgroup_id:
            return None

        session = DBSession.get_db_session()
        ad_obj = session.query(Ad).filter_by(
            ad_id=ad_id,
            adgroup_id=adgroup_id).first()
        if ad_obj is None:
            ad_obj = Ad()
            ad_obj.ad_id = ad_id
            ad_obj.adgroup_id = adgroup_id
            ad_obj.name = name
            ad_obj.status = status
            ad_obj.creative_id = creative_id
            ad_obj.platform_id = platform_id

            current_time = datetime.datetime.utcnow()
            ad_obj.create_time = current_time
            ad_obj.update_time = current_time
            session.add(ad_obj)
            session.commit()

        return ad_obj

    # ad_id is our system ad id
    @staticmethod
    def update(ad_id, name, status):
        try:
            session = DBSession.get_db_session()
            ad_obj = session.query(Ad).get(ad_id)

            ad_obj.name = name
            ad_obj.status = status
            session.commit()
            return 0
        except:
            raise

    @staticmethod
    def get_ad_by_platform_ad(platform_id, platform_ad_id):
        try:
            session = DBSession.get_db_session()
            ad = session.query(Ad).filter_by(platform_id=platform_id).\
                filter_by(ad_id=platform_ad_id).first()
            return ad
        except:
            raise
