__author__ = 'boxia'

from sqlalchemy import Column, String, Integer, BigInteger, Float, DateTime
from session import Base

class InsightByAd(Base):
    __tablename__ = 'insight_by_ad'

    id = Column(BigInteger, primary_key=True)
    platform_id         = Column(BigInteger)
    account_id          = Column(BigInteger)
    ad_id               = Column(BigInteger)
    ad_name             = Column(String(256))
    platform_ad_id      = Column(BigInteger)
    reach               = Column(BigInteger)
    frequency           = Column(Float)
    impressions         = Column(BigInteger)
    unique_impressions  = Column(BigInteger)
    cpm                 = Column(Float)
    spend               = Column(Float)
    clicks              = Column(BigInteger)
    unique_clicks       = Column(BigInteger)
    ctr                 = Column(Float)
    cpc                 = Column(Float)
    conversion          = Column(BigInteger)
    start_time          = Column(DateTime)
    end_time            = Column(DateTime)
    retrieve_time       = Column(DateTime)

