# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)
# modify by Bo Xia

from ads.utils.ads_exception import AdsExceptionConst
from ads.utils.ads_exception import AdsException
from ads.utils.log import Logger
from company import Company
from fb_company import FBCompany
from address import Address
from ip import Ip
from product import Product
from platform import Platform
from keywords import AdsKeyword
from company_score_map import CompanyScoreMap
from campaign_company_map import CampaignCompanyMap
from advertiser import Advertiser
from account import Account
from account_product_map import AccountProductMap
from creative import Creative
from united_campaign import UnitedCampaign
from lead import Lead
from session import DBSession


class AdsPrepareInterface():

    '''
    def test(self):
        raise AdsException(AdsExceptionConst.PARAM_ERR)
    '''

    def get_platform_id(self, platform_name=''):
        if not platform_name.strip():
            Logger.get_logger().warning(" input platform_name is empty ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        try:
            return Platform.get_platform_id(platform_name)
        except:
            Logger.get_logger().warning(" mysql get platform id error ")
            # raise AdsException(AdsExceptionConst.MYSQL_ERR)
            raise

    def set_advertiser(self, advertiser_name=''):
        if not advertiser_name.strip():
            Logger.get_logger().warning(" input advertiser_name is empty ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        try:
            advertiser = Advertiser.create(advertiser_name)
            return advertiser
        except:
            Logger.get_logger().warning(" mysql set advertiser error ")
            # raise AdsException(AdsExceptionConst.MYSQL_ERR)
            raise

    def get_advertiser_by_id(self, advertiser_id):
        if not advertiser_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return Advertiser.get(advertiser_id)

    def set_product(self, advertiser_id, product_name=''):
        if not product_name.strip() or not advertiser_id:
            Logger.get_logger().warning("input advertiser_id or product_name"
                                        "is empty")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        try:
            product = Product.create(product_name, advertiser_id)
            return product
        except:
            Logger.get_logger().warning(" mysql create product error ")
            # raise AdsException(AdsExceptionConst.MYSQL_ERR)
            raise

    def get_product_by_id(self, product_id):
        if not product_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return Product.get(product_id)

    def get_product_by_ucampaign(self, ucampaign_id):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        session = DBSession.get_db_session()
        product = session.query(Product).\
            filter(UnitedCampaign.id == ucampaign_id).\
            filter(UnitedCampaign.product_id == Product.id).first()
        return product

    def get_product_id_by_ucampaign(self, ucampaign_id):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        session = DBSession.get_db_session()
        product = session.query(UnitedCampaign.product_id).\
            filter(UnitedCampaign.id == ucampaign_id).first()
        return product.product_id

    def set_account(self, platform_id, platform_account_id,
                    platform_account_name, product_id):
        if not platform_id or not platform_account_id or not product_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        account = Account.get_account(platform_id, platform_account_id)

        session = DBSession.get_db_session()
        if account is None:
            account = Account()
            account.set_account(platform_id, platform_account_id,
                                platform_account_name)

            session.add(account)
            session.flush()

        account_id = account.id
        account_product_map = session.query(AccountProductMap).\
            filter(AccountProductMap.product_id == product_id).\
            filter(AccountProductMap.account_id == account_id).\
            first()
        if account_product_map is None:
            account_product_map = AccountProductMap()
            account_product_map.set(account_id, product_id)
            session.add(account_product_map)
            session.flush()

        session.commit()

        return account

    def get_account(self, platform_id, platform_account_id):
        if not platform_id or not platform_account_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        print platform_id, platform_account_id

        account = Account.get_account(platform_id, platform_account_id)
        return account

    def get_accounts(self, platform_id, product_id):
        if not platform_id or not product_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        session = DBSession.get_db_session()
        return session.query(Account).\
            join(AccountProductMap, Account.id ==
                 AccountProductMap.account_id). \
            filter(Account.platform_id == platform_id). \
            filter(AccountProductMap.product_id == product_id).all()

    def get_accounts_by_ucampaign(self, platform_id, ucampaign_id):
        product_id = self.get_product_id_by_ucampaign(ucampaign_id)
        return self.get_accounts(platform_id, product_id)

    # google implementation using this.
    def get_account_id(self, platform_name, product_id):
        session = DBSession.get_db_session()
        return session.query(Account.id).\
            join(AccountProductMap, Account.id ==
                 AccountProductMap.account_id). \
            join(Platform, Platform.id == Account.platform_id). \
            filter(Platform.name == platform_name). \
            filter(AccountProductMap.product_id == product_id).\
            first().\
            id

    def set_company(self, company_name='', domain='',
                    industry=None, employee_size=None):
        if not company_name.strip():
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        company = Company.create(company_name, domain, industry,
                                 employee_size)
        return company

    def company_exists(self, company_name, domain, industry, employee_size):
        session = DBSession.get_db_session()
        company = session.query(Company). \
            filter_by(name=company_name, domain=domain, industry=industry,
                      employee_size=employee_size).first()
        return company is not None

    def set_fb_company(self, company_id, fb_company_id, fb_company_name):
        if not company_id or not fb_company_id:
            Logger.get_logger.error(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        fb_company = FBCompany.create(company_id, fb_company_id,
                                      fb_company_name)
        return fb_company

    def has_fb_company(self, company_id_list):
        session = DBSession.get_db_session()
        count = session.query(FBCompany.id).\
            filter(FBCompany.company_id.in_(company_id_list)).count()
        return count > 0

    def set_company_score_map(self, ucampaign_id, target_company_map):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param ucampaign_id error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if not target_company_map or \
                isinstance(target_company_map, dict) is False:
            Logger.get_logger().warning("input param target_company_map error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if 'company_id' not in target_company_map:
            Logger.get_logger().warning(" input param company_id error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        company_score_map_id = CompanyScoreMap.create(ucampaign_id,
                                                      target_company_map)
        return company_score_map_id

    def set_company_ip(self, company_id, ip_start, ip_end):
        if not company_id:
            Logger.get_logger().warning(" input param company_id error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if not ip_start.strip() or not ip_end.strip():
            Logger.get_logger().warning("input param ip_start or ip_end error")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        ip = Ip.create(company_id, ip_start, ip_end)
        return ip

    def get_company_ip(self, company_id):
        return Ip.get_ip(company_id)

    def set_company_address(self, company_id, address, longitude, latitude):
        if not company_id or not address or not longitude or not latitude:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        address = Address.create(company_id, address, longitude, latitude)
        return address

    def get_company_address(self, company_id):
        return Address.get_address_list(company_id)

    def set_keywords(self, ucampaign_id, keywords_list):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param ucampaign_id error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        if isinstance(keywords_list, list) is False or \
           len(keywords_list) <= 0:
            Logger.get_logger().warning(" input param keywords_list error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)

        for keyword in keywords_list:
            AdsKeyword.create(ucampaign_id, keyword)

    def get_keywords(self, ucampaign_id):
        if not ucampaign_id:
            Logger.get_logger().warning(" input param ucampaign_id error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return AdsKeyword.get_keywords(ucampaign_id)

    def get_all_leads_info(self, ucampaign_id):
        session = DBSession.get_db_session()
        leads_info = session.query(CompanyScoreMap, Company).\
            filter(CompanyScoreMap.company_id == Company.id).\
            filter(CompanyScoreMap.united_campaign_id == ucampaign_id).all()
        leads = list()
        for company_score_map, company in leads_info:
            lead = Lead(company_score_map.id, company.name, company.domain,
                        company_score_map.score, company_score_map.grade,
                        company_score_map.control_group, company.id)
            leads.append(lead)
        return leads

    # added by boxia
    # if is_new is True, will only return those leads having no campaigns
    def get_leads_info(self, ucampaign_id=None, grade='', control_group=0,
                       is_new=True):
        session = DBSession.get_db_session()
        leads_info = None
        if grade == '':
            leads_info = session.query(CompanyScoreMap, Company).\
                filter(CompanyScoreMap.company_id == Company.id).\
                filter(CompanyScoreMap.united_campaign_id == ucampaign_id).\
                filter(CompanyScoreMap.control_group == control_group).all()
        else:
            leads_info = session.query(CompanyScoreMap, Company).\
                filter(CompanyScoreMap.company_id == Company.id).\
                filter(CompanyScoreMap.united_campaign_id == ucampaign_id).\
                filter(CompanyScoreMap.grade == grade).\
                filter(CompanyScoreMap.control_group == control_group).all()
        leads = list()
        for company_score_map, company in leads_info:
            lead = Lead(company_score_map.id, company.name, company.domain,
                        company_score_map.score, company_score_map.grade,
                        company_score_map.control_group, company.id)
            leads.append(lead)

        if is_new is True:
            company_ids = session.query(CampaignCompanyMap.company_id).\
                filter(CampaignCompanyMap.united_campaign_id == ucampaign_id).\
                all()
            new_leads = list()
            for lead in leads:
                if lead.company_id not in company_ids:
                    new_leads.append(lead)
            return new_leads

        return leads

    def set_creative(self, ucampaign_id, platform_id, landing_page_url,
                     headline, description_1, description_2, image_url):

        if not ucampaign_id or not landing_page_url or not image_url:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        creative = Creative.create(ucampaign_id, platform_id, landing_page_url,
                                   headline, description_1, description_2,
                                   image_url)
        return creative

    def update_creative(self, creative_id, platform_creative_id):

        if not creative_id or not platform_creative_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        Creative.update(creative_id, platform_creative_id)

    def get_creatives(self, ucampaign_id, platform_id):
        if not ucampaign_id or not platform_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return Creative.get_creatives(ucampaign_id, platform_id)

    def get_facebook_company(self, company_id):
        if not company_id:
            Logger.get_logger().warning(" input param error ")
            raise AdsException(AdsExceptionConst.PARAM_ERR)
        return FBCompany.get_facebook_company(company_id)


if __name__ == "__main__":
    ads_pre = AdsPrepareInterface()
    ads_pre.test()
    # print ads_pre.get_platform_id('facebook')

    # print ads_pre.set_advertiser('test')

    # print ads_pre.set_account(2, 708796745899561,
    #       'Everstring Internal Ad account', 2)

    # ads_pre.get_leads_info(2, 'A', 0)

    # ads_pre.set_ads_keywords(2, ['AAA','BBB','CCC'])

    # ads_pre.set_company_address(10, 'ABCDEFGHIJKLMN', -180000000, -50000000)
    # print ads_pre.get_company_address(10)
    # print ads_pre.get_account_id('google',1)

    # ads_pre.set_company_ip(10, '192.168.0.100', '192.168.0.101')
    # print ads_pre.get_company_ip(10)
