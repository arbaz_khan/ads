import datetime
from sqlalchemy import Column, BigInteger, DateTime, distinct, TIMESTAMP
from base import Base
from session import DBSession
__author__ = 'boxia'


class CampaignCompanyMap(Base):
    __tablename__ = "campaign_company_map"
    id = Column(BigInteger, primary_key=True)
    campaign_id = Column(BigInteger)
    adgroup_id = Column(BigInteger)
    united_campaign_id = Column(BigInteger)
    company_id = Column(BigInteger)
    create_time = Column(DateTime)
    update_time = Column(TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @staticmethod
    def create(campaign_id, adgroup_id, united_campaign_id, company_id):
        try:
            session = DBSession.get_db_session()
            campaign_company_map = session.query(CampaignCompanyMap).filter_by(
                adgroup_id=adgroup_id).first()

            if campaign_company_map is None:
                campaign_company_map = CampaignCompanyMap()
                campaign_company_map.campaign_id = campaign_id
                campaign_company_map.adgroup_id = adgroup_id
                campaign_company_map.united_campaign_id = united_campaign_id
                campaign_company_map.company_id = company_id
                current_time = datetime.datetime.utcnow()
                campaign_company_map.create_time = current_time
                campaign_company_map.update_time = current_time
                session.add(campaign_company_map)
            else:
                campaign_company_map.united_campaign_id = united_campaign_id
                campaign_company_map.company_id = company_id
                current_time = datetime.datetime.utcnow()
                campaign_company_map.update_time = current_time
            session.commit()
            return 0

        except:
            raise

    @staticmethod
    def get_company_by_adgroup(adgroup_id):
        try:
            session = DBSession.get_db_session()
            campaign_company_map = session.query(CampaignCompanyMap).filter_by(
                adgroup_id=adgroup_id).first()
            return campaign_company_map.company_id
        except:
            raise

    @staticmethod
    def get_campaign_adgroup_by_product_company(united_campaign_id,
                                                company_id):
        session = DBSession.get_db_session()
        campaign_company_map = session.query(CampaignCompanyMap).\
            filter_by(united_campaign_id=united_campaign_id,
                      company_id=company_id).\
            first()
        return campaign_company_map.campaign_id, \
            campaign_company_map.company_id

    @staticmethod
    def get_campaign_ids_by_united_campaign_id(united_campaign_id):
        try:
            session = DBSession.get_db_session()

            campaign_company_map = session.query(CampaignCompanyMap).\
                filter_by(united_campaign_id=(int)(united_campaign_id)).\
                distinct().all()

            c_list = []
            for one in campaign_company_map:
                c_list.append(one.campaign_id)

            return c_list

        except:
            raise
