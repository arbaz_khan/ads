import logging
import logging.config
import os
from os import path


class Logger(object):
    CONF_LOG = os.path.dirname(
        path.dirname(path.realpath(__file__))) + '/conf/logging.conf'
    logging.config.fileConfig(CONF_LOG)
    logger = logging.getLogger()

    @staticmethod
    def get_logger():
        return Logger.logger

if __name__ == '__main__':
    logger = Logger.get_logger()
    logger.info("info test")
