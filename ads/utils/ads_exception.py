# -*- coding: utf-8 -*-
# author: Cheng Lei(leicheng@everstring.com)

class AdsExceptionConst():
    PARAM_ERR = "param_error"
    MYSQL_ERR = "mysql_error"
    LOGIC_ERR = "logic_error"

class AdsException(Exception):
    def __init__(self, err_type):
        self.err_type = err_type
