#! /usr/bin/python
#-*- coding:utf-8 -*-

"""
Author(s): Yi Jin
Company: EverString Technology Ltd.
File: YahooAPI.py
Description: this program runs yahoo to retreive information
Creation: Feb 25, 2015
Revision: Feb 25, 2015
Copyright (c) All Right Reserved, EverString Technology Ltd., http://www.everstring.com
"""

#===============================================================================
# import references
#===============================================================================
import httplib2
import json
import oauth2
import sys
import time
import urllib
from datetime import datetime
from pprint import pprint

#resolve encoding error
reload(sys)
sys.setdefaultencoding("utf8")

#===============================================================================
# global class/functions
#===============================================================================
#---this class returns YahooAPI results---
class YahooAPI(object):
    
    #---initialize the YahooAPI class---
    def __init__(self):
        self.consumer_key = "dj0yJmk9OTlOOXhGbzFZalJIJmQ9WVdrOVpHRm5TMXBYTm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0zMA--"
        self.consumer_secret = "fe35739c5083f386b0c3abbf24e59934195736ca"
    
    #---this function encodes the query phrases---
    def getYahooEncodedQuery(self, combo):
        formatted_query = ['"' + x + '"' for x in combo]
        query = " ".join(formatted_query)
        query = query.replace("\u00a3", "�").replace("\u20ac","�")
        query = urllib.urlencode({"q" : query})
        query = query.replace("+", "%20").replace('"', "%22")
        return query
    
    #---this function gets the query results---
    def getResults(self, query, start, count, service='web'):
        # Hua edit: added service parameter, to search specific types of pages like 'news', 'images', 'blogs', 'ads'
        
        #---encode the query---
        query = urllib.urlencode({"q" :query}).replace("+", "%20")
        url = "http://yboss.yahooapis.com/ysearch/"+service+"?" + query + "&abstract=long&count="+str(count)+"&start="+str(start)
        #---initiate the keys
        consumer = oauth2.Consumer(key=self.consumer_key,secret=self.consumer_secret)
        params = {
            'oauth_version': '1.0',
            'oauth_nonce': oauth2.generate_nonce(),
            'oauth_timestamp': int(time.time()),
        }
        oauth_request = oauth2.Request(method='GET', url=url, parameters=params)
        oauth_request.sign_request(oauth2.SignatureMethod_HMAC_SHA1(), consumer, None)
        oauth_header=oauth_request.to_header(realm='yahooapis.com')
     
        #---retrieve the search results---
        http = httplib2.Http()
        resp, content = http.request(url, 'GET', headers=oauth_header)
        try:
            results = json.loads(content)
        except:
            results = {}
        #---retrieve results dictionary---
        results_dict_list = self.getOutputResults(results,service=service)
        return results_dict_list
    
    #----this function returns the output results---    
    def getOutputResults(self, results, service='web'):
        results_dict_list = []
        if results:
            if service in results["bossresponse"]:
                if "results" in results["bossresponse"][service]:
                    for i, res in enumerate(results["bossresponse"][service]["results"]):
                        news_dict = {}
                        news_dict["url"] = res["url"]
                        news_dict["abstract"] = res["abstract"].replace("<b>", "").replace("</b>", "")
                        news_dict["title"] = res["title"].replace("<b>", "").replace("</b>", "")
                        news_dict['date'] = res['date']
                        results_dict_list.append(news_dict)
        #---return results_dict_list---
        return results_dict_list   
    
if __name__ == "__main__":
    YahooAPI = YahooAPI()
    #results_list = YahooAPI.getResults('site:linkedin.com/groups "big data analytics"', 0, 50, service='web')
    #print len(results_list)
    #for each_result in results_list:
        #pprint (each_result)
        #raw_input()
    
    pprint (YahooAPI.getResults('site:linkedin.com/company "01com.com"', 0, 3, service='web'))