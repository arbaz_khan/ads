#! /usr/bin/python
#-*- coding:utf-8 -*-

"""
Author(s): Yi Jin
Company: EverString Technology Ltd.
File: Comp2Domain.py
Description: this program runs the company 2 domains
Creation: Feb 25, 2015
Revision: Feb 25, 2015
Copyright (c) All Right Reserved, EverString Technology Ltd., http://www.everstring.com
"""

#===============================================================================
# import references
#===============================================================================
import copy, csv, re, sys, tldextract
from ConfigParser import SafeConfigParser
from datetime import datetime
from operator import itemgetter
from pprint import pprint as pp

from similarity_measurement import SimilarityMeasurement
from YahooAPI import YahooAPI

#resolve encoding issues
reload(sys)
sys.setdefaultencoding("utf-8")

#===============================================================================
# global class/functions
#===============================================================================
class Comp2Domain(object):
    def __init__(self, score_threshold = 0.50):
        self.sim_measurement = SimilarityMeasurement()
        self.yahoo_api = YahooAPI()
        
        # set the min similarity score to accept the domain
        self.score_threshold = score_threshold
        self.blacklist_comp_names = ["test", "self", "student", "none", "testing", "tester"]
        
    def process(self, comp_name):
        """gets a domain and confidence score for company to domain
        
        Args:
            comp_name (str): company name

        Returns:
            (dict): contains two keys, "domain" (the domain related to the company) 
                and "score" (confidence score).  "domain" is only returned if the 
                confidence score is higher than the score_threshold (default 0.5).
                Else an empty string is returned for a domain and a score of 0
        """
        
        domain = ""
        score = 0
        if comp_name.strip().lower() in self.blacklist_comp_names:
            return {"domain": "", "score": 0}
        # try:
        results = self.yahoo_api.getResults(comp_name, 0, 10, service='web')
        abbr, first_comp_word = self._get_comp_name_abbreviation_and_first_word(comp_name)
        
        
        for each_result in results:
            url = each_result.get("url")
            domain_handle, raw_domain = self._get_domain_handle(url)
            
            if first_comp_word == domain_handle:
                domain = raw_domain
                score = 1.0
            
            # check for comp_name to domain similarity 
            similar_score = self._get_comp_to_domain_similarity_score(comp_name, domain_handle, raw_domain)
            if similar_score >= self.score_threshold:
                domain = raw_domain
                score = similar_score
                break
            else:
                # try checking abbreviations
                abbr_similar_score = self._get_comp_to_domain_similarity_score(abbr, domain_handle, raw_domain)
                
                # set a high similarity score for abbreviations
                if abbr_similar_score >= 0.7:
                    domain = raw_domain
                    score = abbr_similar_score
            
            if score == 1.0:
                break

        return {"domain": domain, "score": score}
        
    def _get_domain_handle(self,d):
        tld_extract = tldextract.extract(d)
        try:
            domain = ".".join([tld_extract.domain, tld_extract.tld])
        except:
            domain = ".".join([tld_extract.domain, tld_extract.suffix])
        return [tld_extract.domain, domain]
        
    def _get_comp_to_domain_similarity_score(self, comp_name, domain_handle, raw_domain):
        """checks if a company's name and its domain handle are similar
        
        Args:
            comp_name (str): company name
            domain_handle (str): the "name" portion of a domain.  
                E.g. ibm is the domain handle for ibm.com, com is the tld (top level domain)
    
        Returns:
            (float): True if the calculated similarity score is higher than the score
                threshold
        """

        #need to try various methods for verification
        if comp_name.lower() == domain_handle or comp_name.lower() == raw_domain:
            return 1.0
        
        #normalize the comp name
        normalized_comp_name = self._normalize_comp_name(comp_name)
        sim_score = self.sim_measurement.get_sequence_similarity(normalized_comp_name, domain_handle)
        if sim_score >= self.score_threshold:
            return sim_score       
       
        return 0
    
    def _clean_comp_name(self, comp_name):
        """cleans a company name by removing various special characters from it
        
        Args:
            comp_name (str): company name
            
        Returns:
            clean_comp_name (str): a cleaned company name
        """
        
        # remove brackets and parentheses
        # replace dashes with space for better name matching
        clean_comp_name = re.sub(r'\([^)]*\)', '', comp_name.replace("*", "").strip()).decode("utf-8", "ignore")
        clean_comp_name.replace("-", " ")
        return clean_comp_name
    
    def _normalize_comp_name(self, comp_name):
        """cleans a company name by removing various special characters from it
        
        Args:
            comp_name (str): company name
            
        Returns:
            clean_comp_name (str): a cleaned company name
        """
        
        clean_comp_name = self._clean_comp_name(comp_name)
        normalized_comp_name = self._normalize_text(clean_comp_name)
        return normalized_comp_name
    
    def _get_comp_name_abbreviation_and_first_word(self, comp_name):
        clean_comp_name = self._clean_comp_name(comp_name)
        words = clean_comp_name.split(" ")
               
        first_comp_word = words[0].lower()
        abbr = "".join([w[0] for w in words if w]).lower()
        return abbr, first_comp_word
                        
    def _normalize_text(self, text):
        #keep only letter and numbers in company and domain names
        normalized_text = "".join([c for c in text if c.isalnum()]).lower()
        return normalized_text    
                                             
if __name__ == "__main__":
    comp2domain = Comp2Domain(score_threshold = 0.40)
    while(True):
        comp_name = raw_input("please enter company name: ")
        print "cleaned comp name:", comp2domain._clean_comp_name(comp_name)
        print "comp abbr:", comp2domain._get_comp_name_abbreviation_and_first_word(comp_name)
        print "normalized comp name", comp2domain._normalize_comp_name(comp_name)
        domain_dict = comp2domain.process(comp_name)
        pp(domain_dict)
        print "\n"
