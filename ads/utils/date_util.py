__author__ = 'boxia'

import datetime


class DateUtil(object):

    @staticmethod
    def validate(date_str):
        try:
            return datetime.datetime.strptime(date_str, '%Y-%m-%d')
        except ValueError:
            raise ValueError("Incorrect data format, should be YYYY-MM-DD")

