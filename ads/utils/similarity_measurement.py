import re, math

from collections import Counter
from difflib import SequenceMatcher

WORD = re.compile(r'\w+')

class SimilarityMeasurement(object):
    def __init__ (self, score_threshold = 0.5):
        self.score_threshold = score_threshold
        
    def confirm_text_similarity(self, text1, text2, algorithm="sequence"):
        """checks if two strings are similar
        
        Primary function is to facilitate comparison of a company name and a domain
         
        Args:
            text1 (str): first string of text to compare
            text2 (str): second string of text to compare
            score_threshold (float): the threhold value to accept or deny similarity.
                range is 0-1
            algorithm (string): the type of algorithm to use for calculating similarity.  
                Sequence similarity is used by default.  

        Returns:
            (bool): True if the calculated similarity score is higher than the score
                threshold
        """
        
        if algorithm == "sequence":
            sim_score = self.get_sequence_similarity(text1.lower(), text2.lower())
        
        if algorithm == "cosine":
            sim_score = self.get_cosine_similarity(text1.lower(), text2.lower())
        
        print sim_score
        if sim_score >= self.score_threshold:
            return True
        return False 
    
    def get_sequence_similarity(self, text1, text2):
        """checks if two strings are similar according to cosin similarity
        
        Primary function is to facilitate comparison of a company name and a domain
         
        Args:
            text1 (str): first string of text to compare
            text2 (str): second string of text to compare

        Returns:
            sim_score (float): a "score" of the similarity measurement from 0 (worst) to 1 (best)
        """
        
        sim_score = 0
        try:
            sim_score = SequenceMatcher(None, text1, text2).ratio()
        except Exception, e:
            print Exception, e
        return sim_score
    
    def get_cosine_similarity(self, text1, text2):
        """checks if two strings are similar according to cosine similarity
        
        Primary function is to facilitate comparison of a company name and a domain
         
        Args:
            text1 (str): first string of text to compare
            text2 (str): second string of text to compare

        Returns:
            sim_score (float): a "score" of the similarity measurement from 0 (worst) to 1 (best)
        """
        
        vec1 = self._text_to_vector(text1)
        vec2 = self._text_to_vector(text2)
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])
        
        sum1 = sum([vec1[x]**2 for x in vec1.keys()])
        sum2 = sum([vec2[x]**2 for x in vec2.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)
        
        if not denominator:
            return 0.0
        sim_score = float(numerator) / denominator
        return sim_score
    
    def _text_to_vector(self, text):
        words = WORD.findall(text)
        return Counter(words)

def main():
    text1 = raw_input("Enter text1 for comparison:")
    text2 = raw_input("Enter text2 for comparison:")
    sim_mea = SimilarityMeasurement()
    print sim_mea.confirm_text_similarity(text1, text2, algorithm="sequence")

if __name__ == "__main__":
    main()