from Comp2Domain import Comp2Domain
import urllib
import urllib2
import json


class ApiCall(object):
    @staticmethod
    def company2domain(company_name):
        comp2domain = Comp2Domain(score_threshold=0.40)
        domain = comp2domain.process(company_name).get('domain')
        return domain

    @staticmethod
    def get_fb_company_list(access_token, company_name):
        fb_search_url = 'https://graph.facebook.com/search?' \
                + 'access_token=%s&q=%s&type=adworkemployer'
        url = fb_search_url % (access_token, urllib.quote(company_name))
        connection = urllib2.urlopen(url)
        response_json = connection.read()
        connection.close()
        json_dict = json.loads(response_json, encoding='utf-8')
        return json_dict['data']


if __name__ == "__main__":
    print ApiCall.company2domain('EverString')
