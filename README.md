# Everstring Ads Platform
## Prepare runtime enviroment

    apt-get update
    # install python 2.7
    apt-get install mysql-server
    apt-get install python-pip
    apt-get install python-mysqldb

## Clone the code
    git clone https://dianqiang@bitbucket.org/everstring_tech/ads.git

## Install python lib
if anything wrong, run `easy_install -U pip` first

  easy_install -U pip
  [sudo] pip install virutalenv

  # create virtualenv named ENV
  virtualenv ENV

  # activate ENV
  source ENV/bin/activate

  # install requirements in new environments
  pip install -r requirements.txt

## Update config file
1. Copy `mysql.example.conf` to `mysql.conf`, then update `mysql.conf` according your mysql enviroment
2. update google_api_client.conf, read [here](https://everstring.atlassian.net/wiki/display/AD/Guide+for+Advertising+on+Google+AdWords) for more instructions.
3. update facebook_api_client.conf, read [here](https://everstring.atlassian.net/wiki/display/AD/Setting+up+development+environment) for more instructions.

## init mysql

    mysql>source sql/schema.sql
    mysql>source sql/platform_init.sql

if got error about `BLOB/TEXT column can't have default value`, comment out this line `sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES` in the mysql config file.

## comment the following lines from the installed package pytz if necessary

    try:
        from pkg_resources import resource_stream
    except ImportError:
        resource_stream = None