# -*- coding: utf-8 -*-
from ads.campaign_management.advertising import Advertising
import datetime
from subprocess import call
from nose.tools import *
from nose_parameterized import parameterized
from nose_parameterized import param
from dbutil import get_db_conn
from ads.service.google import campaign_service
from ads.service.google import adwords_client
from ads.service.facebook import fb_campaign_service
from ads.service.facebook import fb_ads_client
from facebookads.objects import AdCampaign
from ads.utils import log

import os
import dbutil

advertising = None
google_campaign_service = None
facebook_campaign_service = None
logger = log.Logger().get_logger()


def init_db():
    call(['sh', '/home/kunchen/backup/prepare.sh'])
    sql_file = open('/home/kunchen/backup/schema.sql', 'rb')
    conf = dbutil.get_db_config()
    call(['mysql', '-u' + conf['user'], '-p' + conf['password'], '-h' + conf['host'], conf['dbname']], stdin=sql_file)


def setup_module():
    # init_db()
    global advertising

    # google init
    advertising = Advertising(['google', 'facebook'])
    # advertising = Advertising(['facebook'])
    # advertising = Advertising(['google'])
    adwords_clt = adwords_client.GoogleAdwordClient(4512002271).get_client()
    global google_campaign_service
    google_campaign_service = campaign_service.\
        GoogleCampaignService(adwords_clt, 4512002271).\
        adwords_client.GetService('CampaignService', version='v201409')

    # facebook init
    facebook_client = fb_ads_client.FBAdsClient().get_facebook_ads_client()
    global facebook_campaign_service
    facebook_campaign_service = fb_campaign_service.CampaignService(facebook_client)


def teardown_module():
    pass


def verify_advertiser_creation(advertiser_id, advertiser_name):
    con = get_db_conn()
    query_sql = 'select id, name from advertiser where id=%s and name=%s'
    v = advertiser_id, advertiser_name
    cur = con.cursor()
    cur.execute(query_sql, v)
    row = cur.fetchone()
    con.close()

    assert_is_not_none(row, 'advertiser creation failed')


def verify_product_advertiser(product_id, product_name, advertiser_id):
    with get_db_conn() as cur:
        query_sql = 'select id, name, advertiser_id from product ' \
                    'where id=%s and name=%s and advertiser_id=%s'
        v = product_id, product_name, advertiser_id
        cur.execute(query_sql, v)
        row = cur.fetchone()

    assert_is_not_none(row, 'product creation failure')


def verify_account_product(product_id):
    with get_db_conn() as cur:
        query_sql = 'select acct.id ' \
                    'from account as acct join account_product_map as map on acct.id = map.account_id ' \
                    'where map.product_id = %s'
        v = product_id
        cur.execute(query_sql, v)
        rows = cur.fetchall()
    assert_equals(len(rows), 2)


def verify_ucampaign(product_id, ucampaign_name, budget_amount):
    with get_db_conn() as cur:
        query_sql = 'select product_id, name, budget, status from united_campaign ' \
                    'where product_id = %s and name = %s and budget = %s'
        v = product_id, ucampaign_name, budget_amount
        cur.execute(query_sql, v)
        rows = cur.fetchall()
    assert_equals(len(rows), 1)


def verify_creative(ucampaign_id):
    with get_db_conn() as cur:
        query_sql = 'select * from creative where united_campaign_id = %s'
        v = ucampaign_id
        cur.execute(query_sql, v)
        rows = cur.fetchall()
    assert_greater_equal(len(rows), 1)


def verify_leads(ucampaign_id, leads_file):
    with get_db_conn() as cur:
        with open(leads_file) as f:
            lines = f.readlines()
            query_sql = 'select count(*) ' \
                        'from company_score_map ' \
                        'where united_campaign_id = %s'
            v = ucampaign_id
            cur.execute(query_sql, v)
            rows = cur.fetchall()
            print rows, lines, ucampaign_id

    assert_equal(rows[0][0], len(lines) - 1)


def verify_budget(lead_ids, ucampaign_id):
    with get_db_conn() as cur:
        for lead_id in lead_ids:
            query_sql = 'select count(*) ' \
                        'from budget join company_score_map map ' \
                        'on budget.company_score_map_id = map.id ' \
                        'where map.id = %s and map.united_campaign_id = %s'
            v = lead_id, ucampaign_id
            cur.execute(query_sql, v)
            row = cur.fetchone()
            assert_equal(row[0], 2, 'lead id ' + str(lead_id) + ' not equal')


def verify_bid(lead_ids, ucampaign_id):
    with get_db_conn() as cur:
        for lead_id in lead_ids:
            query_sql = 'select count(*) ' \
                        'from bid join company_score_map map ' \
                        'on bid.company_score_map_id = map.id ' \
                        'where map.id = %s and map.united_campaign_id = %s'
            v = lead_id, ucampaign_id
            cur.execute(query_sql, v)
            row = cur.fetchone()
            assert_equal(row[0], 2)


def verify_campaign_creation(ucampgaign_id, platforms):
    with get_db_conn() as cur:
        for platform in platforms:
            if platform == 'google':  # google
                platform_id = 1
                exp_query_sql = 'select count(distinct map.company_id) ' \
                            'from company_score_map map  join address addr on ' \
                            'map.company_id = addr.company_id ' \
                            'where map.united_campaign_id = %s'
                v = ucampgaign_id
                cur.execute(exp_query_sql, ucampgaign_id)
                exp_cnt = cur.fetchone()[0]

                act_query_sql = 'select count(*) ' \
                                'from campaign_company_map map ' \
                                'join campaign ' \
                                'on map.campaign_id = campaign.id ' \
                                'where map.united_campaign_id = %s and campaign.platform_id = %s'
                v = ucampgaign_id, platform_id
                cur.execute(act_query_sql, v)
                act_cnt = cur.fetchone()[0]

                assert_equal(exp_cnt, act_cnt, 'campaign not all created on ' + platform)

            elif platform == 'facebook':  # facebook:
                platform_id = 2
                exp_query_sql = 'select count(*) ' \
                                'from company_score_map map join fb_company on ' \
                                'map.company_id = fb_company.company_id ' \
                                'where map.united_campaign_id = %s'
                v = ucampgaign_id
                cur.execute(exp_query_sql, v)
                exp_cnt = cur.fetchone()[0]

                act_query_sql = 'select count(*) ' \
                                'from campaign_company_map map ' \
                                'join adgroup on map.adgroup_id = adgroup.id ' \
                                'join campaign on campaign.id = adgroup.campaign_id ' \
                                'where map.united_campaign_id = %s and campaign.platform_id = %s'
                v = ucampgaign_id, platform_id
                cur.execute(act_query_sql, v)
                act_cnt = cur.fetchone()[0]

                assert_equal(exp_cnt, act_cnt, 'campaign not all created on ' + platform)


def remove_campaigns(ucampaign_id, platforms):
    if 'google' in platforms:
        logger.info('9. cleaning up google campaigns of ucampaign : %s' % ucampaign_id)
        remove_google_campaigns(ucampaign_id)
        logger.info('9. cleaned up google campaigns of ucampaign : %s' % ucampaign_id)
    if 'facebook' in platforms:
        remove_facebook_campaigns(ucampaign_id)
        logger.info('9. cleaned up facebook campaigns')


def remove_google_campaigns(ucampaign_id):
    with get_db_conn() as cur:
        query_sql = 'select campaign.campaign_id ' \
                    'from campaign join campaign_company_map map on campaign.id = map.campaign_id ' \
                    'where map.united_campaign_id = %s and campaign.platform_id = %s'
        v = ucampaign_id, 1
        cur.execute(query_sql, v)
        rows = cur.fetchall()
        campaign_ids = set()
        for row in rows:
            campaign_ids.add(row[0])
        operations = list()
        for campaign_id in campaign_ids:
            operation = {
                         'operator': 'SET',
                         'operand': {
                             'id': campaign_id,
                             'status': 'REMOVED'
                         }
                     }
            operations.append(operation)
            logger.info('removing campaign with %s ' % operation)
        google_campaign_service.mutate(operations)


def remove_facebook_campaigns(ucampaign_id):
    with get_db_conn() as cur:
        query_sql = 'select campaign.campaign_id ' \
                    'from campaign join campaign_company_map map on campaign.id = map.campaign_id ' \
                    'where map.united_campaign_id = %s and campaign.platform_id = %s'
        v = ucampaign_id, 2
        cur.execute(query_sql, v)
        rows = cur.fetchall()
        campaign_ids = set()
        for row in rows:
            campaign_ids.add(row[0])
        for campaign_id in campaign_ids:
            remove_one_facebook_campaign(campaign_id)


def remove_one_facebook_campaign(campaign_id):
    campaign = AdCampaign(campaign_id)
    campaign[AdCampaign.Field.status] = AdCampaign.Status.deleted
    campaign.remote_update()


# @parameterized([
#     param(advertiser_name='Autodesk',
#           product_name='Fusion 365',
#           facebook_acct='727144177398151',
#           facebook_acct_name='Google Yuge Test',
#           google_acct='4512002271',
#           google_acct_name='4512002271',
#           united_campaign_name='Autodesk Fusion 365 Campaign 1',
#           united_campaign_budget_amount=365,
#           creative_file='./ads/conf/ads3.ini',
#           leads_file='./test/10.csv',
#           budget_type='STANDARD',
#           budget_amount=32200,
#           bid_type='CPC',
#           bid_amount=1200,
#           is_share=False,
#           budget_period='DAILY',
#           clean_up=True
#           ),
#     param(advertiser_name='Autodesk',
#           product_name='Fusion 365',
#           facebook_acct='727144177398151',
#           facebook_acct_name='Google Yuge Test',
#           google_acct='4512002271',
#           google_acct_name='4512002271',
#           united_campaign_name='Autodesk Fusion 365 Campaign 2',
#           united_campaign_budget_amount=365,
#           creative_file='./ads/conf/ads3.ini',
#           leads_file='./test/10a.csv',
#           budget_type='STANDARD',
#           budget_amount=88800,
#           bid_type='CPC',
#           bid_amount=1500,
#           is_share=False,
#           budget_period='DAILY',
#           clean_up=True
#            ),
#     param(advertiser_name='Autodesk',
#           product_name='AutoCAD',
#           facebook_acct='727144177398151',
#           facebook_acct_name='Google Yuge Test',
#           google_acct='4512002271',
#           google_acct_name='4512002271',
#           united_campaign_name='Autodesk AutoCAD Campaign 1',
#           united_campaign_budget_amount=365,
#           creative_file='./ads/conf/ads3.ini',
#           leads_file='./test/10b.csv',
#           budget_type='STANDARD',
#           budget_amount=88800,
#           bid_type='CPC',
#           bid_amount=1500,
#           is_share=False,
#           budget_period='DAILY',
#           clean_up=True
#           ),
#     param(advertiser_name='Microsoft',
#           product_name='Office 7',
#           facebook_acct='727144177398151',
#           facebook_acct_name='Google Yuge Test',
#           google_acct='4512002271',
#           google_acct_name='4512002271',
#           united_campaign_name='Microsoft Office 7 Campaign 1',
#           united_campaign_budget_amount=365,
#           creative_file='./ads/conf/ads2.ini',
#           leads_file='./test/10c.csv',
#           budget_type='STANDARD',
#           budget_amount=88800,
#           bid_type='CPC',
#           bid_amount=1500,
#           is_share=False,
#           budget_period='DAILY',
#           clean_up=True
#           ),
#     param(advertiser_name='Microsoft',
#           product_name='Office 7',
#           facebook_acct='727144177398151',
#           facebook_acct_name='Google Yuge Test',
#           google_acct='4512002271',
#           google_acct_name='4512002271',
#           united_campaign_name='Microsoft Office 7 Campaign 2',
#           united_campaign_budget_amount=365,
#           creative_file='./ads/conf/ads2.ini',
#           leads_file='./test/10d.csv',
#           budget_type='STANDARD',
#           budget_amount=88800,
#           bid_type='CPC',
#           bid_amount=1500,
#           is_share=False,
#           budget_period='DAILY',
#           clean_up=True
#           )
# ])
def test_create_campaign(advertiser_name, product_name, facebook_acct, facebook_acct_name,
                         google_acct, google_acct_name, united_campaign_name, united_campaign_budget_amount,
                         creative_file, leads_file, budget_type, budget_amount, bid_type, bid_amount,
                         is_share, budget_period, clean_up
                         ):
    advertiser = advertising.set_advertiser(advertiser_name)
    verify_advertiser_creation(advertiser.id, advertiser_name)
    print '1. create advertiser and verified'
    logger.info('1. create advertiser and verified')

    product = advertising.set_product(advertiser.id, product_name)
    verify_product_advertiser(product.id, product.name, advertiser.id)
    print '2. create product and verified'
    logger.info('2. create product and verified')

    advertising.set_account('facebook', facebook_acct, facebook_acct_name, product.id)
    advertising.set_account('google', google_acct, google_acct_name, product.id)
    verify_account_product(product.id)
    print '3. create account and verified'
    logger.info('3. create account and verified')

    ucampaign = advertising.set_united_campaign(product.id, united_campaign_name,
                                                united_campaign_budget_amount, datetime.datetime.utcnow())
    verify_ucampaign(product.id, united_campaign_name, united_campaign_budget_amount)
    print '4. create united campaign and verified'
    logger.info('4. create united campaign and verified')

    advertising.set_creative_config(ucampaign.id, creative_file)
    verify_creative(ucampaign.id)
    print '5. create creative and verified'
    logger.info('5. create creative and verified')

    lead_ids = advertising.import_leads(ucampaign.id, leads_file)
    verify_leads(ucampaign.id, leads_file)
    print '6. import leads and verified'
    logger.info('6. import leads and verified')

    # set budget bid
    budget_bid_info = dict()
    budget_bid_info['budget_type'] = budget_type
    budget_bid_info['budget_amount'] = budget_amount
    budget_bid_info['bid_type'] = bid_type
    budget_bid_info['bid_amount'] = bid_amount
    budget_bid_info['is_share'] = is_share
    budget_bid_info['budget_period'] = budget_period

    advertising.set_budget_bid('facebook', lead_ids, budget_bid_info)
    advertising.set_budget_bid('google', lead_ids, budget_bid_info)
    # set google budget bid
    verify_budget(lead_ids, ucampaign.id)
    verify_bid(lead_ids, ucampaign.id)
    print '7. set budget and bidding and verified'
    logger.info('7. set budget and bidding and verified')

    advertising.create_campaigns(advertiser.id, ucampaign.id, budget_bid_info)
    verify_campaign_creation(ucampaign.id, advertising.platforms)
    print '8. create campaigns and verified'
    logger.info('8. create campaigns and verified')

    if clean_up:
        remove_campaigns(ucampaign.id, advertising.platforms)


@nottest
@parameterized([
    param(ucampaign_id=1,
          budget_type='STANDARD',
          budget_amount='88800',
          is_share=False,
          budget_period='DAILY'),
    param(ucampaign_id=2,
          budget_type='STANDARD',
          budget_amount='88800',
          is_share=False,
          budget_period='DAILY'),
    param(ucampaign_id=3,
          budget_type='STANDARD',
          budget_amount='88800',
          is_share=False,
          budget_period='DAILY'),
    param(ucampaign_id=4,
          budget_type='STANDARD',
          budget_amount='88800',
          is_share=False,
          budget_period='DAILY'),
    param(ucampaign_id=5,
          budget_type='STANDARD',
          budget_amount='88800',
          is_share=False,
          budget_period='DAILY'),
])
def test_update_budget(ucampaign_id, budget_type, budget_amount, is_share, budget_period):
    budget_bid_info = dict()
    budget_bid_info['budget_type'] = budget_type
    budget_bid_info['budget_amount'] = budget_amount
    budget_bid_info['is_share'] = is_share
    budget_bid_info['budget_period'] = budget_period
    advertising.update_budget(ucampaign_id, budget_bid_info)
    verify_update_budget(ucampaign_id, budget_type, budget_amount, is_share, budget_period, advertising.platforms)


def verify_update_budget(ucampaign_id, budget_type, budget_amount, is_share, budget_period, platforms):
    for platform in platforms:
        if platform == 'google':
            platform_id = 1
        else:
            platform_id = 2
        with get_db_conn() as cur:
            query_sql = 'select budget_type, budget_amount ' \
                        'from budget join company_score_map as map ' \
                        'on budget.company_score_map_id = map.id ' \
                        'where budget.platform_id = %s and map.united_campaign_id = %s'
            v = platform_id, ucampaign_id
            cur.execute(query_sql, v)
            for row in cur.fetchall():
                assert_equal(budget_type, row[0])
                assert_equal(budget_amount, row[1])


@nottest
@parameterized([
    param(ucampaign_id=1,
          bid_tyype='CPC',
          bid_amount=1200),
    param(ucampaign_id=2,
          bid_tyype='CPC',
          bid_amount=1200),
    param(ucampaign_id=3,
          bid_tyype='CPC',
          bid_amount=1200),
    param(ucampaign_id=4,
          bid_tyype='CPC',
          bid_amount=1200),
    param(ucampaign_id=5,
          bid_tyype='CPC',
          bid_amount=1200)
])
def test_update_bidding(ucampaign_id, bid_type, bid_amount):
    budget_bid_info = dict()
    budget_bid_info['bid_type'] = bid_type
    budget_bid_info['bid_amount'] = bid_amount
    advertising.update_bidding(ucampaign_id, budget_bid_info)
    verify_udpate_bidding(ucampaign_id, bid_type, bid_amount, advertising.platforms)


def verify_udpate_bidding(ucampaign_id, bid_type, bid_amount, platforms):
    for platform in platforms:
        if platform == 'google':
            platform_id = 1
        else:
            platform_id = 2
        with get_db_conn() as cur:
            query_sql = 'select bid_type, bid_amount from bid ' \
                        'join company_score_map as map on bid.company_score_map_id = map.id ' \
                        'where bid.platform_id = %s and map.united_campaign_id = %s'
            v = platform_id, ucampaign_id
            cur.execute(query_sql, v)
            for row in cur.fetchall():
                assert_equal(bid_type, row[0])
                assert_equal(bid_amount, row[1])


# @nottest
# @parameterized([
#     param(ucampaign_id=1,
#           status=1),
#     param(ucampaign_id=1,
#           status=0),
#     param(ucampaign_id=2,
#           status=1),
#     param(ucampaign_id=2,
#           status=0),
#     param(ucampaign_id=3,
#           status=1),
#     param(ucampaign_id=3,
#           status=0),
#     param(ucampaign_id=4,
#           status=1),
#     param(ucampaign_id=4,
#           status=0),
#     param(ucampaign_id=5,
#           status=1),
#     param(ucampaign_id=5,
#           status=0)
# ])
def test_update_campaign_status(ucampaign_id, status):
    advertising.update_campaign_status(ucampaign_id, status)
    verify_update_campaign_status(ucampaign_id, status)


def verify_update_campaign_status(ucampaign_id, status):
    with get_db_conn() as cur:
        query_sql = 'select status from united_campaign where id = %s'
        v = ucampaign_id
        cur.execute(query_sql, v)
        row = cur.fetchone()
        assert_equal(row[0], status)

if __name__ == '__main__':
    project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    os.chdir(project_root)
    setup_module()
    #
    # test_create_campaign(advertiser_name='Autodesk',
    #       product_name='Fusion 365',
    #       facebook_acct='727144177398151',
    #       facebook_acct_name='Google Yuge Test',
    #       google_acct='4512002271',
    #       google_acct_name='4512002271',
    #       united_campaign_name='Autodesk Fusion 365 Campaign 1',
    #       united_campaign_budget_amount=365,
    #       creative_file='./ads/conf/ads3.ini',
    #       leads_file='./test/10.csv',
    #       budget_type='STANDARD',
    #       budget_amount=32200,
    #       bid_type='CPC',
    #       bid_amount=1200,
    #       is_share=False,
    #       budget_period='DAILY',
    #       clean_up=False
    #       )
    # test_create_campaign(advertiser_name='Autodesk',
    #       product_name='Fusion 365',
    #       facebook_acct='727144177398151',
    #       facebook_acct_name='Google Yuge Test',
    #       google_acct='4512002271',
    #       google_acct_name='4512002271',
    #       united_campaign_name='Autodesk Fusion 365 Campaign 2',
    #       united_campaign_budget_amount=365,
    #       creative_file='./ads/conf/ads3.ini',
    #       leads_file='./test/10a.csv',
    #       budget_type='STANDARD',
    #       budget_amount=88800,
    #       bid_type='CPC',
    #       bid_amount=1500,
    #       is_share=False,
    #       budget_period='DAILY',
    #       clean_up=False
    #        )

    test_update_campaign_status(1, 1)
    test_update_campaign_status(1, 0)
    test_update_campaign_status(2, 1)
    test_update_campaign_status(2, 0)
