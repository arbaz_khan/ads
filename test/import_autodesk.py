__author__ = 'kunchen'

import csv
import datetime
import pymysql.connections
import pymysql.cursors
import ConfigParser
import os, sys
import random

class Insight():
    def __init__(self, domain='www.com'):
        self.domain = domain
        self.impressions = 0
        self.clicks = 0
        self.reach = 0
        self.conversion = 0
        self.spend = 0
        self.intent_score = 40 #random.randint(1,100)
        self.engagement_score = 90 #random.randint(1,100)

    def calc_other(self):
        if self.impressions > 0:
            self.cpm = float(self.spend) / self.impressions * 1000
            self.ctr = float(self.clicks) / self.impressions
        else:
            self.cpm = 0.0
            self.ctr = 0

        if self.clicks > 0:
            self.cpc = float(self.spend) / self.clicks
        else:
            self.cpc = 0

        if self.reach > 0:
            self.frequency = float(self.impressions) / self.reach
        else:
            self.frequency = 0

    def get_fit_score(self):
        g = self.grade
        if g == 'A':
            return random.randint(76,100)
        elif g == 'B':
            return random.randint(51,75)
        elif g == 'C':
            return random.randint(26,50)
        else:
            return random.randint(1,25)


def compare_insight(ins1, ins2):
    if ins1.impressions > ins2.impressions:
        return 1
    elif ins1.impressions < ins2.impressions:
        return -1
    else:
        if ins1.clicks > ins2.clicks:
            return 1
        elif ins1.clicks < ins2.clicks:
            return -1
        else:
            if ins1.conversion > ins2.conversion:
                return 1
            elif ins1.conversion < ins2.conversion:
                return -1
            else:
                return 0

def parse_csv_to_dict(input_csv, temp_dict):
    f = open(input_csv, 'rb')
    reader = csv.DictReader(f)
    for row in reader:
        domain = row['Domain']
        insight = temp_dict.get(domain, Insight(domain))
        insight.impressions += int(row.get('Impressions', 0))
        insight.clicks += int(row.get('Clicks', 0))
        # insight.reach += int(row.get('Reach', 0))
        insight.reach = insight.impressions / random.randint(7,10)
        if row.has_key('Spend'):
            spend_str = row['Spend']
            if '$' in spend_str:
                insight.spend += float(spend_str.split('$')[1])
            else:
                insight.spend += float(spend_str)

        # print row.get('Downloads', 0)
        insight.conversion += int(row.get('Downloads', 0))

        if len(row.get('EverString Ratings')) > 0 :
            insight.grade = row.get('EverString Ratings')
        temp_dict[domain] = insight

def top_insight_list(temp_dict, top):
    insight_list = temp_dict.values()
    top_list = sorted(insight_list, cmp=compare_insight, reverse=True)[0:top]
    return top_list
    pass

def get_db_conn():
    real_path = os.path.dirname(os.path.realpath(__file__))
    config_file = real_path + '/../ads/conf/mysql.conf'
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    host = config.get('database', 'host')
    dbname = config.get('database', 'dbname')
    user = config.get('database', 'user')
    password = config.get('database', 'password')
    port = config.get('database', 'port')

    con = pymysql.connect(host=host, port=int(port), user=user, passwd=password, db=dbname)
    return con

def gen_lead_csv_from_top_insight_list(ins_list):
    outfile = open('../tasks/todo/200.csv', 'wb')
    headers = ('Company Website','Company Name','Company Rating','Score','Type', 'Industry', 'Employee Size',
               'Intent Score', 'Engagement Score')
    writer = csv.DictWriter(outfile, headers)
    writer.writeheader()
    conn = get_db_conn()
    cur = conn.cursor()

    i = 1
    for ins in ins_list:
        domain = ins.domain
        if not hasattr(ins, 'grade'):
            print ins.domain + " doesn't has grade info, writing grade A as default"
            sys.exit(1)

        sql = 'select name, industry, employee_size from company where domain = %s'
        cur.execute(sql, (domain))
        if cur.rowcount <= 0:
            print ins.domain + " company info not in table, skipping"
        else:
            row = cur.fetchone()
            row_dict={}
            row_dict['Company Name'] = row[0]
            row_dict['Industry'] = row[1]
            row_dict['Employee Size'] = row[2]
            row_dict['Company Website'] = domain
            row_dict['Company Rating'] = ins.grade
            row_dict['Intent Score'] = ins.intent_score
            row_dict['Engagement Score'] = ins.engagement_score
            row_dict['Score'] = ins.get_fit_score()
            writer.writerow(row_dict)
            i += 1

    conn.close()
    outfile.close()
    print 'wrote ' + str(i) + ' companies'



def import_insight(idate, csv_list, top):
    temp_dict = {}
    for input_csv in csv_list:
        parse_csv_to_dict(input_csv, temp_dict)

    result_list = top_insight_list(temp_dict, top)
    gen_lead_csv_from_top_insight_list(result_list)

    # import main_qa

    conn = get_db_conn()
    cur = conn.cursor()
    for ins in result_list:
        ins.ucampaign_id = 1
        cur.execute('select id from company where domain = %s', (ins.domain))
        if cur.rowcount <= 0:
            continue
        else:
            ins.company_id = cur.fetchone()[0]
            ins.calc_other()
            insert_sql = 'insert into insight (united_campaign_id, company_id, ' \
                         'reach, frequency, impressions, unique_impressions, cpm, spend, clicks, ' \
                         'unique_clicks, ctr, cpc, conversion, start_time, end_time, retrieve_time)' \
                         ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            v = (ins.ucampaign_id, ins.company_id, ins.reach, ins.frequency, ins.impressions, ins.reach, ins.cpm,
                 ins.spend, ins.clicks, 0, ins.ctr, ins.cpc, ins.conversion, idate, idate, idate)
            cur.execute(insert_sql, v)
            conn.commit()
    conn.close()

def load_company_csv(comp_csv):
    f = open(comp_csv, 'rb')
    reader = csv.DictReader(f)
    conn = get_db_conn()
    cur = conn.cursor()
    insert_sql = 'insert into company (name, domain, industry, employee_size, revenue, ' \
                 'address_street, address_city, address_state, address_country, description, create_time, update_time)' \
                    'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
    # domain,name,industry,revenue,employeeSize,address,description
    i = 1
    for row in reader:
        print "imported company " + str(i)
        i += 1
        v = (row['name'], row['domain'], row['industry'], row['employeeSize'], row['revenue'], row['addressStreet'],
             row['addressCity'], row['addressState'], row['addressCountry'],
             row['description'], datetime.datetime.utcnow(), datetime.datetime.utcnow())
        cur.execute(insert_sql, v)
        conn.commit()
    conn.close()

def load_eng_int_score():
    f = open('../tasks/todo/200.csv', 'rb')
    reader = csv.DictReader(f)
    conn = get_db_conn()
    cur = conn.cursor()

    query_sql = 'select id from company where domain = %s'
    update_sql = 'update company_score_map set engagement_score = %s, intent_score = %s'
    for csv_row in reader:
        domain = csv_row['Company Website']
        cur.execute(query_sql, (domain))
        company_id = cur.fetchone()[0]

        eng_score = csv_row['Engagement Score']
        int_score = csv_row['Intent Score']
        cur.execute(update_sql, (eng_score, int_score))
        conn.commit()





if __name__ == '__main__':
    load_company_csv('../tasks/load_firmographic_json_into_database/autodesk_ad_campaign_comp_profile_enrichment.csv')
    csv_list = ['../tasks/todo/accounts_download.csv',
                '../tasks/todo/cookies_targeting.csv',
                '../tasks/todo/geoip_targeting.csv',
                '../tasks/todo/social_targeting.csv']

    idate = datetime.datetime.strptime('2015-06-01', '%Y-%m-%d')
    import_insight(idate, csv_list, 200)

    # load_eng_int_score()