# -*- coding: utf-8 -*-
from ads.service.google import adwords_client
from ads.service.google.insight_service import GoogleInsightService
from ads.service.facebook import fb_ads_client
from ads.service.facebook.fb_insight_service import InsightService
from mock import patch
from nose.tools import *
import ConfigParser
import unittest


class PlatformInsightTestCase(unittest.TestCase):

    def testGoogleInsight(self):
        client_obj = adwords_client.GoogleAdwordClient(6089274671)
        # client_obj = adwords_client.GoogleAdwordClient(4512002271)
        client = client_obj.get_client()
        account_id = client_obj.get_account()
        print client, account_id
        insightSvc = GoogleInsightService(client, account_id)

        start_date = '20150522'
        end_date = '20150522'

        with patch.object(insightSvc.report_downloader,
                          "DownloadReportAsStringWithAwql") as mock:
            insightSvc.get_ad_sights_by_ad(start_date, end_date)
            insightSvc.get_ad_sights_by_ad_group(start_date, end_date)
            insightSvc.get_ad_sights_by_campaign(start_date, end_date)
            assert_equal(mock.call_count, 3)

    @unittest.skip('')
    def testFBInsight(self):
        cf = ConfigParser.ConfigParser()
        cf.read('../ads/conf/facebook_api_client.ini')
        access_token = cf.get("token", "access_token")
        app_id = cf.get("app", "app_id")
        app_secret = cf.get("app", "app_secret")
        client = fb_ads_client.FacebookAdsClient(app_id, app_secret, access_token)
        client.connect()

        insightSvc = InsightService(client)
        # insightSvc.get_insight_by_ad()
        query_field = {}
        query_field['day_start'] = '2015-05-22'
        query_field['day_stop'] = '2015-05-23'
        insightSvc.get_insight_cron('act_715934635185772', query_field)

if __name__ == '__main__':
    PlatformInsightTestCase.main()
