__author__ = 'kunchen'

import ConfigParser
import pymysql
import csv
import os.path
import random

def main():
    real_path = os.path.dirname(os.path.realpath(__file__))
    config_file = real_path + '/../ads/conf/mysql_old.conf'
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    host = config.get('database', 'host')
    dbname = config.get('database', 'dbname')
    user = config.get('database', 'user')
    password = config.get('database', 'password')
    port = config.get('database', 'port')

    conn = pymysql.connect(host=host, port=int(port), user=user, passwd=password, db=dbname)
    cur = conn.cursor()
    cur.execute("select company_name, domain from company_scoring_autodesk limit 10000")
    print(cur.description)
    print()

    csv_file = open('leads_all.csv', 'wb')
    csv_writer = csv.writer(csv_file, delimiter=',')
    for row in cur:
        my_score = random.randint(1,100)
        my_rating = rating(my_score)
        my_type = type()
        c = row[0]
        d = row[1]
        csv_writer.writerow([d, c, my_rating, my_score, my_type])

    csv_file.close()

def rating(score):
    if score > 75:
        return 'A'
    if score <= 75 and score > 50:
        return 'B'
    if score <=50 and score > 25:
        return 'C'
    if score <= 25 and score > 0:
        return 'D'

def type():
    result = random.randint(0,1)
    if result == 1:
        return 'control group'
    else:
        return ''

if __name__ == '__main__':
    main()