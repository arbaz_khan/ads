insert into insight(united_campaign_id,company_id,reach,frequency,impressions,unique_impressions,cpm,spend,clicks,unique_clicks,ctr,cpc,start_time,end_time,retrieve_time)
select
                                united_campaign_id,
                                company_id,
                                if(reach is null,0,reach) reach,
                                if(reach=0 or reach is null,0,impressions/reach) frequency,
                                if(impressions is null,0,impressions) impressions,
                                if(unique_impressions is null,0,unique_impressions) unique_impressions,
                                if(impressions=0 or impressions is null,0,spend*1000/impressions) cpm,
                                if(spend is null,0,spend) spend,
                                if(clicks is null,0,clicks) clicks,
                                if(unique_clicks is null,0,unique_clicks) unique_clicks,
                                if(impressions=0 or impressions is null,0,clicks/impressions) ctr,
                                if(clicks=0 or clicks is null,0,spend/clicks) cpc,
                                if(start_time is null,'_DATE_',start_time) start_time,
                                if(end_time is null,'_DATE_',end_time) end_time,
                                if(retrieve_time is null,'_DATE_',retrieve_time) retrieve_time
FROM
(
                                select
                                                        united_campaign_id,
                                                        company_id,
                                                        sum(reach) reach,
                                                        sum(impressions) impressions,
                                                        sum(unique_impressions) unique_impressions,
                                                        sum(spend) spend,
                                                        sum(clicks) clicks,
                                                        sum(unique_clicks) unique_clicks,
              min(start_time) start_time,
              max(end_time) end_time,
              max(retrieve_time) retrieve_time
                                from
                                (
                                                        select  united_campaign_id,company_id,reach,frequency,impressions,unique_impressions,cpm,spend,clicks,unique_clicks,ctr,cpc,start_time,end_time,retrieve_time from
                                                        (select campaign_id,united_campaign_id,company_id from campaign_company_map) a
                                                        LEFT OUTER JOIN
                                                        (select * from insight_by_campaign where platform_id=1 and start_time>='_DATE_' and end_time<='_DATE_') b
                                                        on a.campaign_id=b.campaign_id

                                union

                                                        select  united_campaign_id,company_id,reach,frequency,impressions,unique_impressions,cpm,spend,clicks,unique_clicks,ctr,cpc,start_time,end_time,retrieve_time from
                                                        (select adgroup_id,united_campaign_id,company_id from campaign_company_map) a
                                                        JOIN
                                                        (select * from insight_by_adgroup where platform_id=2 and start_time>='_DATE_' and end_time<='_DATE_') b
                                                        on a.adgroup_id=b.adgroup_id
                                ) c
                                group by united_campaign_id,company_id
) d
