__author__ = 'kunchen'
import random
import csv

industry_list = \
    ['Software',
     'Realestate',
     'SAAS',
     'Military',
     'Chemistry',
     'Energy',
     'Consulting']

addr_list = \
    ['RM 1510, SOHO T2, Chaoyang, Beijing, China',
     'BD 10, Borel Ave, San Mateo, CA, US']

desc_list = \
    ["it's a great company",
     "not so good a company",
     "very lousy company"]

field_names = \
    ['Company Website',
     'Company Name',
     'Company Rating',
     'Score',
     'Type',
     'Industry']

def main():
    input_csv = '200a.csv'
    output_csv = 'new_'+input_csv

    infile = open(input_csv, 'rb')
    outfile = open(output_csv, 'wb')


    writer = csv.DictWriter(outfile, field_names)
    writer.writeheader()

    for row in csv.DictReader(infile):
        row['Industry'] = random.choice(industry_list)
        writer.writerow(row)

if __name__ == '__main__':
    main()