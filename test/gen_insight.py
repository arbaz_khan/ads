__author__ = 'kunchen'

import ConfigParser
import pymysql
import os.path
import random
import datetime

class Insight():
    def __init__(self, cpc):
        self.reach = random.randint(0,2000)
        self.impressions = random.randint(5000, 20000)
        self.unique_impressions = self.reach
        if self.reach == 0:
            self.frequency = 0
        else:
            self.frequency = float(self.impressions) / self.reach
        self.cpc = cpc
        self.clicks = random.randint(0, 100)
        self.unique_clicks = int(self.clicks / random.uniform(1,2))
        self.spend = self.cpc * self.clicks
        self.cpm = self.spend / self.impressions * 1000
        self.ctr = float(self.clicks) / self.impressions * 100
        self.conversion = random.randint(0,2)

def gen_campaign_insight(conn, cpc, numdays):
    # get campaign list first
    sql = 'select account_id, id, campaign_id, name from campaign'
    cur = conn.cursor()
    cur.execute(sql)

    base = datetime.date.today()
    date_list = [base - datetime.timedelta(days=x) for x in range(1, numdays+1)]
    for row in cur.fetchall():
        for d in date_list:
            acct_id = row[0]
            if acct_id == 1:
                p_id = 2
            else:
                p_id = 1
            id = row[1]
            cmp_id = row[2]
            name = row[3]
            insight = Insight(cpc)
            print insight.frequency
            v = (p_id, acct_id, id, cmp_id, name,
                 insight.reach, insight.frequency, insight.impressions, insight.unique_impressions,
                 insight.cpm, insight.spend, insight.clicks, insight.unique_clicks, insight.conversion, insight.ctr, insight.cpc, d, d,
                 datetime.datetime.today())
            insert_sql = 'insert into insight_by_campaign (platform_id, account_id, campaign_id, platform_campaign_id, ' \
                         ' campaign_name, reach, frequency, impressions, unique_impressions, cpm, spend, clicks, ' \
                         'unique_clicks, conversion, ctr, cpc, start_time, end_time, retrieve_time)' \
                         ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

            cur.execute(insert_sql, v)
            conn.commit()

    # conn.commit()

def gen_adgroup_insight(conn, cpc, numdays):
    sql = 'select c.account_id, ag.id, ag.adgroup_id, ag.name from campaign as c join adgroup as ag on c.id = ag.campaign_id;'
    cur = conn.cursor()
    cur.execute(sql)

    base = datetime.date.today()
    date_list = [base - datetime.timedelta(days=x) for x in range(1, numdays+1)]
    for row in cur.fetchall():
        for d in date_list:
            acct_id = row[0]
            if acct_id == 1:
                p_id = 2
            else:
                p_id = 1
            id = row[1]
            grp_id = row[2]
            name = row[3]
            insight = Insight(cpc)
            v = (p_id, acct_id, id, grp_id, name,
                 insight.reach, insight.frequency, insight.impressions, insight.unique_impressions,
                 insight.cpm, insight.spend, insight.clicks, insight.unique_clicks, insight.conversion, insight.ctr, insight.cpc, d, d,
                 datetime.datetime.today())
            insert_sql = 'insert into insight_by_adgroup (platform_id, account_id, adgroup_id, platform_adgroup_id, ' \
                         ' adgroup_name, reach, frequency, impressions, unique_impressions, cpm, spend, clicks, ' \
                         'unique_clicks, conversion, ctr, cpc, start_time, end_time, retrieve_time)' \
                         ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            cur.execute(insert_sql, v)
            conn.commit()

def gen_ad_insight(conn, cpc, numdays):
    sql = 'select c.account_id, a.id, a.ad_id, a.name from campaign as c join adgroup as ag join ad as a on c.id = ag.campaign_id and ag.id = a.adgroup_id;'
    cur = conn.cursor()
    cur.execute(sql)

    base = datetime.date.today()
    date_list = [base - datetime.timedelta(days=x) for x in range(1, numdays+1)]
    for row in cur.fetchall():
        for d in date_list:
            acct_id = row[0]
            if acct_id == 1:
                p_id = 2
            else:
                p_id = 1
            id = row[1]
            ad_id = row[2]
            name = row[3]
            insight = Insight(cpc)
            v = (p_id, acct_id, id, ad_id, name,
                 insight.reach, insight.frequency, insight.impressions, insight.unique_impressions,
                 insight.cpm, insight.spend, insight.clicks, insight.unique_clicks, insight.conversion, insight.ctr, insight.cpc, d, d,
                 datetime.datetime.today())
            insert_sql = 'insert into insight_by_ad (platform_id, account_id, ad_id, platform_ad_id, ' \
                         ' ad_name, reach, frequency, impressions, unique_impressions, cpm, spend, clicks, ' \
                         'unique_clicks, conversion, ctr, cpc, start_time, end_time, retrieve_time)' \
                         ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            cur.execute(insert_sql, v)
            conn.commit()


def main():
    real_path = os.path.dirname(os.path.realpath(__file__))
    config_file = real_path + '/../ads/conf/mysql.conf'
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    host = config.get('database', 'host')
    dbname = config.get('database', 'dbname')
    user = config.get('database', 'user')
    password = config.get('database', 'password')
    port = config.get('database', 'port')

    conn = pymysql.connect(host=host, port=int(port), user=user, passwd=password, db=dbname)

    cpc = 1.23
    numdays = 2
    gen_campaign_insight(conn, cpc, numdays)
    gen_adgroup_insight(conn, cpc, numdays)
    gen_ad_insight(conn, cpc, numdays)

    conn.close()

if __name__ == '__main__':
    main()