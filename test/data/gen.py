__author__ = 'kunchen'

import random
def score():
    grades = ['A','B','C','D']
    control_groups = ['control group', '']
    grade = random.choice(grades)
    cg = random.choice(control_groups)
    if grade=='A':
        return ('A', random.randrange(76,100), cg)
    elif grade=='B':
        return ('B', random.randrange(51,75), cg)
    elif grade=='C':
        return ('C', random.randrange(26,50), cg)
    elif grade=='D':
        return ('D', random.randrange(1,25), cg)
    else:
        return None

def main():
    f = open('xx.csv', 'r')
    fo = open('lead.csv', 'w')
    fo.write('Company Website,Company Name,Company Rating,Score,Type\n')
    for line in f:
        comp_name = line.split('.')[0]
        domain = line[:-2]
        g, s, cg = score()
        fo.write(domain +','+ comp_name +','+ g +','+ str(s)+','+cg+'\n')

    f.close()
    fo.close()





if __name__ == '__main__':
    main()