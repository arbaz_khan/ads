import unittest

from ads.model.ads_insight_interface import AdsInsightInterface


class AdsInsightInterfaceTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_company_insight(self):
        # On my local dev box, this query runs for 3.210s
        result = AdsInsightInterface.get_united_company_insight(
            1, 1, 10, 'clicks', 'desc', '', '1970-01-01', '2015-12-12')
        # pprint(result)
        insights = result.get('list')
        # pprint(insights)
        assert len(insights) >= 0

        keys = [
            "address_street",
            "address_city",
            # "address_state",
            # "address_country",

            "clicks",
            "conversions",
            "cpc",
            "cpm",
            "create_time",
            "ctr",
            "description",
            "domain",
            "employee_size",
            "id",
            "impressions",
            "industry",
            "name",
            "reaches",
            "revenue",
            "spend",
            "update_time",
        ]

        for insight in insights:
            # pprint(insight)
            for key in keys:
                assert key in insight
        # assert False

    def test_converted_count(self):
        company_cnt = AdsInsightInterface.get_company_converted_count(1, '1970-01-01', '2015-12-12')
        self.assertGreaterEqual(company_cnt, 0)

        people_cnt = AdsInsightInterface.get_people_converted_count(1, '1970-01-01', '2015-12-12')
        self.assertGreaterEqual(people_cnt, 0)

        # if two people might belongs to one company, that counts two `peole_cnt` and one `company_cnt`.
        # so people_cnt >= company_cnt
        self.assertGreaterEqual(people_cnt, company_cnt)

    def test_campaign_insight(self):
        keys = [
            'imp',
            'clicks',
            'conv',
            'ctr',
            'cpm',
            'cpc',
            'spend',
            'budget',
        ]
        campaign_insight = AdsInsightInterface.get_insight_data_by_united_campaign_id(1, '1970-01-01', '2015-12-12')

        for key in keys:
            self.assertIn(key, campaign_insight)
