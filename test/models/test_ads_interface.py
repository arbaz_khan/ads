import unittest
import datetime

from mock import patch

from ads.model.ads_interface import AdsInterface
from ads.model.session import DBSession
from ads.model.budget import Budget
from ads.model.bid import Bid
from ads.model.campaign import Campaign
from ads.model.budget_bid_history import BudgetBidHistory


SPECIAL_DATE = datetime.datetime(1990, 6, 25).date()


class BudgetBidTest(unittest.TestCase):
    SPECIAL_DATE = datetime.datetime(1990, 6, 25).date()

    def setUp(self):
        self.session = DBSession.get_db_session()

        self.bid = Bid(platform_id=-1, company_score_map_id=-1,
                       bid_type=0, bid_amount=-1,
                       update_time=SPECIAL_DATE)
        self.budget = Budget(platform_id=-1, company_score_map_id=-1,
                             budget_type="TEST", budget_amount=-1,
                             update_time=SPECIAL_DATE)
        self.session.add_all([self.bid, self.budget])
        self.session.commit()

    def tearDown(self):
        self.session.delete(self.bid)
        self.session.delete(self.budget)

        self.session.commit()
        self.session.close()

    def test_set_update_time_on_updating_budget_bid(self):
        ads = AdsInterface()
        budget_bid_info = {"budget_amount": -2, "bid_amount": -1}
        ads.update_budget_bid(-1, -1, budget_bid_info)

        self.assertEqual(-2, self.budget.budget_amount)

        self.assertEqual(datetime.date.today(), self.budget.update_time.date())
        self.assertEqual(SPECIAL_DATE,
                         self.bid.update_time.date())

    def test_set_update_time_of_bid(self):
        ads = AdsInterface()
        budget_bid_info = {"budget_amount": -1, "bid_amount": -2}
        ads.update_budget_bid(-1, -1, budget_bid_info)

        self.assertEqual(-2, self.bid.bid_amount)

        self.assertEqual(datetime.date.today(),
                         self.bid.update_time.date())

    @patch.object(BudgetBidHistory, 'set')
    def test_not_add_budget_bid_history_on_unchange(self, func):
        ads = AdsInterface()
        budget_bid_info = {"budget_amount": -1, "bid_amount": -1}
        ads.update_budget_bid(-1, -1, budget_bid_info)
        self.assertFalse(func.called)

    @patch.object(BudgetBidHistory, 'set')
    def test_add_budget_bid_history_on_change(self, setfunc):
        a = AdsInterface()
        budget_bid_info = {"budget_amount": -1, "bid_amount": -2}
        try:
            a.update_budget_bid(-1, -1, budget_bid_info)
        except:
            pass
        self.assertTrue(setfunc.called)


class CampaignTest(unittest.TestCase):

    def setUp(self):
        self.session = DBSession.get_db_session()
        self.campaign = Campaign(id=-1, campaign_id=-1,
                                 name="dummy campaign 1",
                                 account_id=-1, status=0, platform_id=-1,
                                 update_time=SPECIAL_DATE)
        self.session.add(self.campaign)
        self.session.commit()

    def tearDown(self):
        self.session.delete(self.campaign)
        self.session.commit()
        self.session.close()

    def test_set_update_time_on_updating_campaign(self):
        ads = AdsInterface()
        es_campaign_id = self.campaign.id
        ads.update_campaign(es_campaign_id, "dummy campaign 2", None)

        self.assertEqual("dummy campaign 2", self.campaign.name)
        self.assertEqual(datetime.date.today(),
                         self.campaign.update_time.date())
