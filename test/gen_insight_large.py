__author__ = 'kunchen'
#coding=utf-8
import ConfigParser
import pymysql
import os.path
import random
import csv
import datetime
import import_autodesk


from ads.model.ads_prepare_interface import AdsPrepareInterface
from ads.campaign_management.advertising import Advertising


class Insight():
    def __init__(self, cpc):
        self.reach = random.randint(0, 2000)
        self.impressions = random.randint(5000, 20000)
        self.unique_impressions = self.reach
        if self.reach == 0:
            self.frequency = 0
        else:
            self.frequency = float(self.impressions) / self.reach
        self.cpc = cpc
        self.clicks = random.randint(0, 100)
        self.unique_clicks = int(self.clicks / random.uniform(1, 2))
        self.spend = self.cpc * self.clicks
        self.cpm = self.spend / self.impressions * 1000
        self.ctr = float(self.clicks) / self.impressions * 100
        self.conversion = random.randint(0, 2)


def import_leads(ucampaign_id, csv_file):
    ads_prepare = AdsPrepareInterface()
    company_ids = list()
    with open(csv_file, mode='rb') as f:
        reader = csv.DictReader(f)
        for row in reader:
            domain = row['Company Website']
            company_name = row['Company Name']
            grade = row.get('Company Rating', '')
            industry = row.get('Industry', '')
            employee_size = row.get('Employee Size', '')
            score = row.get('Score', -1)
            type = row.get('Type', '')
            if type == 'control group':
                control_group = 1
            else:
                control_group = 0

            # create company
            company = ads_prepare.set_company(company_name, domain, industry,
                                                   employee_size)

            target_company = {
                'company_id': company.id,
                'score': score,
                'grade': grade,
                'control_group': control_group,
            }
            lead_id = ads_prepare.set_company_score_map(ucampaign_id, target_company)
            company_ids.append(company.id)

    return company_ids


def get_db_con():
    real_path = os.path.dirname(os.path.realpath(__file__))
    config_file = real_path + '/../ads/conf/mysql.conf'
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    host = config.get('database', 'host')
    dbname = config.get('database', 'dbname')
    user = config.get('database', 'user')
    password = config.get('database', 'password')
    port = config.get('database', 'port')

    con = pymysql.connect(host=host, port=int(port), user=user, passwd=password, db=dbname)
    return con

def gen_insight_large(con, ucmp_id, company_ids, cpc, numdays):
    base = datetime.date.today()
    date_list = [base - datetime.timedelta(days=x) for x in range(1, numdays+1)]

    cur = con.cursor()
    i=1
    for d in date_list:
        for cmp_id in company_ids:
            print i
            i += 1
            insight = Insight(cpc)
            v=(ucmp_id, cmp_id, insight.reach, insight.frequency, insight.impressions, insight.unique_impressions,
               insight.cpm, insight.spend, insight.clicks, insight.unique_clicks, insight.ctr, insight.cpc,
               insight.conversion, d,d,datetime.datetime.today())
            insert_sql = 'insert into insight (united_campaign_id, company_id, ' \
                         'reach, frequency, impressions, unique_impressions, cpm, spend, clicks, ' \
                         'unique_clicks, ctr, cpc, conversion, start_time, end_time, retrieve_time)' \
                         ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            cur.execute(insert_sql, v)
            con.commit()
def fill_campaign_company_map(ucampaign_id, company_ids):
    con = get_db_con()
    cur = con.cursor()
    insert_sql = 'insert into campaign_company_map (campaign_id, adgroup_id, united_campaign_id, company_id, create_time, update_time) ' \
                 'value (%s, %s, %s, %s, %s, %s)'

    for company_id in company_ids:
        print 'filling in campaign company map for company ' + str(company_id)
        v = 100, 10000 + company_id, ucampaign_id, company_id, datetime.datetime.utcnow(), datetime.datetime.utcnow()
        cur.execute(insert_sql, v)
        con.commit()


def main():
    real_path = os.path.dirname(os.path.realpath(__file__))

    lead_csv = real_path + '/5000right.csv'
    lead_csv_score = real_path + '/5000lead.csv'
    advertiser_name = 'AutoDesk'
    product_name = 'Fusion360'
    ucmp_name = advertiser_name + '_' + product_name + ' Campaign 1'
    ucmp_budget = 25000

    cpc = 2.5
    numdays = 60

    advertising = Advertising(['google'])
    advertiser = advertising.set_advertiser(advertiser_name)
    product = advertising.set_product(advertiser.id, product_name)
    advertising.set_account('facebook', '727144177398151', 'Test', product.id)
    advertising.set_account('google', '4512002271', 'Google Test', product.id)

    ucampaign = advertising.set_united_campaign(product.id, ucmp_name, ucmp_budget, datetime.datetime.utcnow())
    import_autodesk.load_company_csv(lead_csv)
    company_ids = advertising.import_leads(ucampaign.id, lead_csv_score)
    # company_ids = [id for id in range(1,5001)]

    fill_campaign_company_map(ucampaign.id, set(company_ids))
    gen_insight_large(get_db_con(), ucampaign.id, set(company_ids), cpc, numdays)
    # fill_campaign_company_map(1, company_ids)
    # gen_insight_large(get_db_con(), 1, company_ids, cpc, numdays)


if __name__ == '__main__':

    main()