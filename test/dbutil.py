from ads.service.common.insight_service import InsightService
import os
import ConfigParser
import pymysql
__author__ = 'kunchen'
# -*- coding: utf-8 -*-


def get_db_config():
    real_path = os.path.dirname(os.path.realpath(__file__))
    config_file = real_path + '/../ads/conf/mysql.conf'
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    conf_dict = {}
    conf_dict['host'] = config.get('database', 'host')
    conf_dict['dbname'] = config.get('database', 'dbname')
    conf_dict['user'] = config.get('database', 'user')
    conf_dict['password'] = config.get('database', 'password')
    conf_dict['port'] = config.get('database', 'port')
    return conf_dict


def get_db_conn():
    conf_dict = get_db_config()

    con = pymysql.connect(host=conf_dict['host'], port=int(conf_dict['port']), user=conf_dict['user'],
                          passwd=conf_dict['password'], db=conf_dict['dbname'])
    return con


def merge():
    InsightService.merge_insight_cron('2015-06-16', '2015-06-17')

if __name__ == '__main__':
    merge()
