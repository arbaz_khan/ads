__author__ = 'dianqiang'
import sys
import os.path
import random
import datetime
import csv
class Insight():

    def __init__(self, row):
        self.united_campaign_id = row['united_campaign_id']
        self.company_id = row['company_id']
        self.reach = row['reach']
        self.frequency = row['frequency']
        self.impressions = int(row['impressions'])
        self.unique_impressions = row['unique_impressions']
        self.cpm = row['cpm']
        self.spend = row['spend']
        self.clicks = int(row['clicks'])
        self.unique_clicks = row['unique_clicks']
        self.ctr = row['ctr']
        self.cpc = row['cpc']
        self.conversion = row['conversion']
        self.start_time = row['start_time']
        self.end_time = row['end_time']
        self.retrieve_time = row['retrieve_time']
    def set_start_time(self,start_time):
        self.start_time = start_time
    def set_end_time(self,end_time):
        self.end_time = end_time
    def set_retrieve_time(self,retrieve_time):
        self.retrieve_time = retrieve_time
    def to_string(self):
        return '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' % (self.united_campaign_id,self.company_id,self.reach,self.frequency,self.impressions,self.unique_impressions,self.cpm,self.spend,self.clicks,self.unique_clicks,self.ctr,self.cpc,self.conversion,self.start_time,self.end_time,self.retrieve_time)

def deal_record(row,date_list):
    day_num = len(date_list)
    all_insight=Insight(row)
    ctr = float(all_insight.clicks)/float(all_insight.impressions)
    insight_list=[]
    for i in range(day_num):
        day_insight = Insight(row)
        day_insight.clicks = 0
        day_insight.impressions = 0
        day_insight.set_start_time(date_list[i])
        day_insight.set_end_time(date_list[i])
        day_insight.set_retrieve_time(date_list[i])
        if i!=0:
           day_insight.conversion='0'
           day_insight.cpc = '0'
           day_insight.ctr='0'
           day_insight.unique_clicks='0'
           day_insight.spend='0'
           day_insight.unique_impressions='0'
           day_insight.reach = '0'
        insight_list.append(day_insight)
    for i in range(all_insight.clicks):
        random_index=random.randint(0,day_num-1)
        insight_list[random_index].clicks = insight_list[random_index].clicks + 1
        insight_list[random_index].impressions = insight_list[random_index].impressions + 1
    for i in range(all_insight.impressions - all_insight.clicks):
        random_index=random.randint(0,day_num-1)
        insight_list[random_index].impressions = insight_list[random_index].impressions + 1
    for i in range(day_num):
        print insight_list[i].to_string()
        
        
    

def main(all_insight_file):
    start_date = datetime.datetime.strptime('20150429', '%Y%m%d')
    end_date = datetime.datetime.strptime('20150506', '%Y%m%d')
    date_list = []
    for i in range(int((end_date - start_date).days)):
        date_itr = start_date + datetime.timedelta(i)
        start_date_str = date_itr.strftime('%Y%m%d')
        date_list.append(start_date_str)
    print "united_campaign_id,company_id,reach,frequency,impressions,unique_impressions,cpm,spend,clicks,unique_clicks,ctr,cpc,conversion,start_time,end_time,retrieve_time"
    with open(all_insight_file, mode='rb') as f:
        reader = csv.DictReader(f)
        for row in reader:
            day_insight=deal_record(row,date_list)


if __name__ == '__main__':
    all_insight_file = sys.argv[1]
    main(all_insight_file)
