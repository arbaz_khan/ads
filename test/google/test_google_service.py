#!/usr/bin/python2.7
import uuid
from multiprocessing import Queue
from ads.service.google import advertising_service
from ads.service.common import advertiser_service
from ads.service.common import product_service
from ads.model import ads_prepare_interface
from ads.campaign_management import campaign
from ads.campaign_management import ad
from ads.campaign_management import budget_strategy
from ads.campaign_management import bidding_strategy
results = Queue()
target_service = advertising_service.AdvertisingService()
product_id = 0
adver_id = 0
company_name = ''
product_name = ''


def setUp():
    global results
    global target_service
    global product_id
    global adver_id
    global company_name
    global product_name
    google_account_id = '4430529228'
    google_platform_id = '1'
    google_platform_account_name = 'autodesk'
    adver_service = advertiser_service.AdvertiserService()
    produ_service = product_service.ProductService()
    prepare_interface = ads_prepare_interface.AdsPrepareInterface()

    company_name = str(uuid.uuid4())
    product_name = str(uuid.uuid4())

    adver_id = adver_service.set_advertiser(company_name)
    product_id = product_service.ProductService().add_product(
        adver_id, product_name)
    prepare_interface.set_account(
        google_platform_id, google_account_id, google_platform_account_name, product_id)
    print 'setup'


def tearDown():
    print 'tearDown'


def test_create():
    leads_grade = None
    leads_info = []
    lead_info = {'domain': 'google.com',
                 'name': 'Google', 'company_id': str(adver_id)}
    leads_info.append(lead_info)
    campaign_parameters = campaign.CampaignParameters(
        'MANUAL_CPC', '20180710', '20180720', 'PAUSED', 'DISPLAY')
    ad_list_info = []
    for i in range(4):
        image_ad = ad.ImageAd('fd', 'fdf', 'dd', 'test', '1', 'http://www.everstring.com', 'www.everstring.com', '1',
                              '/work/job/google_adword/data/1.png', 'image_name')
        ad_list_info.append(image_ad)
    budget = budget_strategy.BudgetStrategy('ACCELERATED', 10000)
    bidding = bidding_strategy.BiddingStrategy(
        'CpcBid', 8000, 'bidding_scheme')
    ad_type = None
    keywords = ['keyword1', 'keyword2']
    target_service.create_campaigns(results, company_name, product_id, product_name, leads_grade,
                                    leads_info, campaign_parameters, ad_list_info, budget, bidding, None, keywords)


def test_update_budget():
    budget = budget_strategy.BudgetStrategy('ACCELERATED', 20000)
    target_service.update_budget(results, budget, product_id)


def test_update_bid():
    bidding = bidding_strategy.BiddingStrategy(
        'CpcBid', 16000, 'bidding_scheme')
    target_service.update_bidding(results, bidding, product_id)
