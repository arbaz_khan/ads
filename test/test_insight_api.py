# -*- coding: utf-8 -*-
""" Test cases for insight API called by front-end ajax. """
from ads.controller import insight
import simplejson as json
from datetime import date
from datetime import timedelta
import dbutil
from nose.tools import assert_equals, assert_raises
from nose.plugins.attrib import attr
from subprocess import call
from decimal import *
__author__ = 'kunchen'

app = insight.application.test_client()
# getcontext().prec = 4


def init_db():
    sql_file = open('/home/kunchen/tmp/autodesk4.sql', 'rb')
    conf = dbutil.get_db_config()
    call(['mysql', '-u' + conf['user'], '-p' + conf['password'], '-h' + conf['host'], conf['dbname']], stdin=sql_file)


def setup_module():
    init_db()


def teardown_module():
    pass


def query_db(sql, v):
    con = dbutil.get_db_conn()
    cur = con.cursor()
    cur.execute(sql, v)
    con.commit()
    con.close()
    return cur


def test_status():
    rv = json.loads(app.get('/status').data)
    print rv['status']
    assert_equals(rv['status'], 200)
    assert_equals(rv['data']['status'], 'ok')


def test_campaign_list():
    rv = json.loads(app.get('/ads/insight/campaigns?currentPage=1&pageSize=10&'
                            'sortKey=&sortDir=desc&searchKey=&'
                            'startTime=1970-1-1&endTime=2015-6-20').data)
    assert_equals(rv['status'], 200)

    # there can be more than one item in the test db, which leads to
    # different result in index 0.

    # query_sql = 'select budget from united_campaign'
    # cur = query_db(query_sql, ())
    # result = cur.fetchone()
    # assert_equals(rv['data']['list'][0]['budget'], result[0])


def test_funnel():
    rv = json.loads(app.get('/ads/insight/funnel?currentPage=1&pageSize=10'
                            '&campId=1&sortDir=desc&startTime=1970-1-1&'
                            'endTime=2015-6-20').data)
    v = (1, '1970-01-01', '2015-6-20')
    cur = query_db('select count(*) from company_score_map where '
                   'united_campaign_id = %s', (v[0]))
    target_comp = cur.fetchone()[0]
    cur = query_db('select count(*) from campaign_company_map where '
                   'united_campaign_id = %s', (v[0]))
    found_comp = cur.fetchone()[0]

    cur = query_db('select count(*) from '
                   '( select company_id '
                   'from insight where united_campaign_id = %s and start_time'
                   ' >= %s and end_time <= %s '
                   'group by company_id having sum(clicks)>0) as tmp', v)
    interest_comp = cur.fetchone()[0]

    cur = query_db('select count(*) from '
                   '( select company_id'
                   ' from insight where united_campaign_id = %s and start_time'
                   ' >= %s and end_time <= %s '
                   'group by company_id having sum(conversion) > 0 ) as tmp',
                   v)
    converted_comp = cur.fetchone()[0]

    cur = query_db('select sum(impressions), sum(clicks), sum(conversion) '
                   'from insight where united_campaign_id = %s and start_time'
                   ' >= %s and end_time <= %s', v)
    imp, clicks, conversions = cur.fetchone()

    assert_equals(rv['status'], 200)
    assert_equals(rv['data']['targetComp'], target_comp)
    assert_equals(rv['data']['compFound'], found_comp)
    assert_equals(rv['data']['compInterested'], interest_comp)
    assert_equals(rv['data']['compSold'], converted_comp)
    assert_equals(rv['data']['peopleImp'], imp)
    assert_equals(rv['data']['peopleClicks'], clicks)
    assert_equals(rv['data']['peopleConv'], conversions)


def test_spend_total():
    rv = json.loads(app.get('/ads/insight/spendTotals?currentPage=1&'
                            'pageSize=10'
                            '&campId=1&sortDir=desc&startTime=1970-1-1&'
                            'endTime=2015-6-20').data)
    v = (1, '1970-01-01', '2015-6-20')
    cur = query_db('select sum(spend), sum(spend)/sum(clicks),'
                   ' sum(conversion)/sum(clicks) from insight '
                   'where united_campaign_id = %s and start_time >=%s and'
                   ' end_time <= %s;', v)
    spend, cpc, conv_rate = cur.fetchone()

    assert_equals(rv['status'], 200)
    assert_equals(rv['data']['spend'], spend)
    assert_equals(rv['data']['cpc'], cpc)
    assert_equals('%.4f' % rv['data']['convRate'], '%.4f' % conv_rate)  # do this because of precision


def test_company_list():
    for page in 1, 1:  # 5, 20:
        # , 'yesterday', 'last_7_days', :
        for date_range in 'all_time', 'last_30_days':
            begin, end = get_dates_by_range(date_range)
            rv = json.loads(app.get('/ads/insight/companies?currentPage={0}&'
                                    'pageSize=1000&campId=1&sortDir=desc'
                                    '&startTime={1}&endTime={2}'.
                                    format(page, begin, end)).data)

            rv_list = rv['data']['list']
            assert all(rv_list[i]['clicks'] >= rv_list[i + 1]['clicks'] for i in xrange(len(rv_list) - 1))
            # v = (1, begin.strftime('%Y-%m-%d'), end.strftime('%Y-%m-%d'),
            #      (page - 1) * 10)
            # query_sql = 'select c.name, sum(clicks), sum(reach), ' \
            #             'sum(impressions), sum(conversion) ' \
            #             'from insight i ' \
            #             'right join campaign_company_map cm ' \
            #             'on i.company_id = cm.company_id ' \
            #             'join company c on cm.company_id = c.id ' \
            #             'where i.united_campaign_id = %s and start_time >=%s '\
            #             'and end_time <=%s ' \
            #             'group by i.company_id order by sum(clicks) desc ' \
            #             'limit %s, 10'
            # cur = query_db(query_sql, v)
            # result = cur.fetchall()
            # i = 0
            # while i < len(result):
            #     assert_equals(rv['status'], 200)
            #     assert_equals(rv['data']['currentPage'], page)
            #     item = rv['data']['list'][i]
            #     assert_equals(item['name'], result[i][0])
            #     assert_equals(item['clicks'], result[i][1])
            #     assert_equals(item['reaches'], result[i][2])
            #     assert_equals(item['impressions'], result[i][3])
            #     assert_equals(item['conversions'], result[i][4])
            #     i += 1


def test_search_order_by_company_list():
    begin, end = get_dates_by_range('all_time')
    ucampaign_id = 1

    # TODO : 'ctr', 'cpm', 'cpc',:
    for sortKey in 'reaches', 'impressions', 'clicks', 'spend':
        for order in 'asc', 'desc':
            for searchKey in '', 'dell', 'abe':
                path = '/ads/insight/companies?currentPage=1&'\
                                        'pageSize=200&'\
                                        'campId=1&sortKey={0}&sortDir={1}&'\
                                        'searchKey={2}&'\
                                        'startTime={3}&endTime={4}'\
                                        .format(sortKey, order, searchKey,
                                                begin, end)
                rv = json.loads(app.get(path).data)
                rv_list = rv['data']['list']
                if order == 'asc':
                    assert all(rv_list[i][sortKey] <= rv_list[i + 1][sortKey] for i in xrange(len(rv_list) - 1))
                else:
                    assert all(rv_list[i][sortKey] >= rv_list[i + 1][sortKey] for i in xrange(len(rv_list) - 1))
                # newsortKey = sortKey
                # if sortKey == 'reaches':
                #     newsortKey = 'reach'
                # if sortKey == 'conversions':
                #     newsortKey = 'conversion'
                #
                # query_sql = 'select c.name, sum(clicks), sum(reach), ' \
                #             'sum(impressions), sum(conversion) ' \
                #             'from insight i ' \
                #             'right join campaign_company_map cm on ' \
                #             'i.company_id = cm.company_id ' \
                #             'join company c on cm.company_id = c.id ' \
                #             'where i.united_campaign_id = %s and ' \
                #             'start_time >=%s and end_time <=%s ' \
                #             'and upper(c.name) like upper(%s) ' \
                #             'group by i.company_id order by sum(' + newsortKey + ') ' + order + ', name desc'
                #
                # v = ucampaign_id, begin, end, '%' + searchKey + '%'
                # cur = query_db(query_sql, v)
                # result = cur.fetchall()
                # i = 0
                # while i < len(result):
                #     assert_equals(rv['status'], 200)
                #     assert_equals(rv['data']['count'], len(result))
                #     assert_equals(rv['data']['list'][i]['name'], result[i][0])
                #     assert_equals(rv['data']['list'][i]['clicks'], result[i][1])
                #     assert_equals(rv['data']['list'][i]['reaches'], result[i][2])
                #     assert_equals(rv['data']['list'][i]['impressions'], result[i][3])
                #     assert_equals(rv['data']['list'][i]['conversions'], result[i][4])
                #     i += 1


@attr(only='yes')
def test_status_code_500():
    '''
    Response status code should be 500 on server error.

    The request is missing required params: `startTime` and `endTime`
    '''
    url = '/ads/insight/companies'
    response = app.get(url)
    assert_equals(response.status_code, 500)


@attr(only='yes')
def test_status_code_404():
    '''
    Response status code should be 404 when not found.
    '''
    url = '/ads/insight/not-such-resource'
    response = app.get(url)
    assert_equals(response.status_code, 404)


def get_dates_by_range(date_range):
    end = date.today() + timedelta(days=-1)

    if date_range == 'all_time':
        begin = date.fromtimestamp(0)
    elif date_range == 'yesterday':
        begin = end
    elif date_range == 'last_7_days':
        begin = date.today() + timedelta(days=-8)
    elif date_range == 'last_30_days':
        begin = date.today() + timedelta(days=-31)
    else:
        begin = None

    return begin, end


if __name__ == '__main__':
    setup_module()
    test_company_list()
    pass
