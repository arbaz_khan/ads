#!/bin/sh

ps -ef | grep "python insight.py" | awk '{printf("kill -9 %s\n", $2)}' | sh
