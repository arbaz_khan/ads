try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My Project',
    'author': 'My Name',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'abc@123.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['ads'],
    'scripts': [],
    'name': 'ads'
}

setup(**config)
